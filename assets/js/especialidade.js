//Caminho do sistema, var endereco.
document.write('<script type="text/javascript" src="../assets/js/clinigon.js"></script>');
/*
 *  Controle da tela do especialidade
 */
$(document).ready(function() {
    $("#frm_cad").submit(function(event) {
        event.preventDefault();
    });
    $('#frm_cad').validate(
            {
                onkeyup: false,
                onclick: false,
                submitHandler: function(form) {
                    var cadEspecialidade = $.ajax({
                        url: $(form).attr('action'),
                        type: $(form).attr('method'),
                        async: false,
                        data: $(form).serialize(),
                        dataType: "json",
                        beforeSend: function() {
                            $("#div-cad-especialidade").hide("slow");
                        }
                    });
                    cadEspecialidade.done(function(msg) {
                        if (msg) { //boolean
                            $("html, body").animate({scrollTop: 0}, "slow");
                            $("#cad-ok").show("slow");
                            oTable.fnReloadAjax(); //Atualiza o LISTAR
                        } else {
                            $("html, body").animate({scrollTop: 0}, "slow");
                            $("#cad-erro").show("slow");
                        }
                    });
                    cadEspecialidade.fail(function(jqXHR, textStatus) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $("#cad-erro").show("slow");
                    });
                }
            });
});
$("#btn-tentar").click(function(event) {
    $("#cad-erro").hide("slow");
    $("#div-cad-especialidade").show("slow");
});
$("#btn-cad-novo").click(function(event) {
    $('#frm_cad').get(0).reset()
    $("#cad-ok").hide("slow");
    $("#div-cad-especialidade").show("slow");
});
//////////////////////////
//Modificando #outro formulário
//////////////////////////


//////////////////////////////////
/// AUTO COMPLETE com TYPEAHEAD
/////////////////////////////////
$(document).ready(function() {
    $("#modifica-especialidade").hide();
});
$('#busca-editar').typeahead({
    source: function(query, process) {
        var $url = endereco + 'especialidade/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof(this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#idespecialidade').val(item.id);

        $.get(endereco + "especialidade/pega_especialidade/" + item.id)
                .done(function(data) {
            $("#frm_edit #especialidade").val(data.especialidade);
            //perfurmaria
            $("#modifica-especialidade").fadeTo("slow", 0);
            $("#modifica-especialidade").fadeTo("slow", 100);
        });

        $("#modifica-especialidade").show("slow");


        return item.name;
    }
});

//////// FRM EDIT , modificando especialidade existente

$(document).ready(function() {
    $("#frm_edit").submit(function(event) {
        event.preventDefault();
    });
    $('#frm_edit').validate(
            {
                onkeyup: false,
                onclick: false,
                submitHandler: function(form) {
                    var cadEspecialidade = $.ajax({
                        url: $(form).attr('action'),
                        type: $(form).attr('method'),
                        async: false,
                        data: $(form).serialize(),
                        dataType: "json",
                        beforeSend: function() {
                            $("#modifica-especialidade").toggle("slow");
                        }
                    });
                    cadEspecialidade.done(function(msg) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        if (msg) { //boolean
                            $("#ok_edit").show("slow");
                             oTable.fnReloadAjax(); //Atualiza o LISTAR
                        } else {
                            $("#erro_edit").show("slow");
                        }

                        //Oculta a mensagem de alerta depois de 3 segundos
                        $("#ok_edit").alert();
                        window.setTimeout(function() {
                            $("#ok_edit").alert('close');
                        }, 3000);
                    });
                    cadEspecialidade.fail(function(jqXHR, textStatus) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $("#cad-erro").show("slow");
                    });
                }
            });

});
$("#btn_tentar_edit").click(function(event) {
    $("#erro_edit").hide("slow");
});
$("#btn-edit").click(function(event) {
    $("#ok_edit").hide("slow");
});

////////////////////////////
/// EXCLUINDO
///////////////////////////
$(document).ready(function() {
    $("#exclui_especialidade").hide();
});
$('#busca_excluir').typeahead({
    source: function(query, process) {
        var $url = endereco + 'especialidade/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof(this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#id_especialidade').val(item.id);

        $.get(endereco + "especialidade/pega_especialidade/" + item.id)
                .done(function(data) {
            $("#frm_delete #especialidade").val(data.especialidade);
            //perfurmaria
            $("#exclui_especialidade").fadeTo("slow", 0);
            $("#exclui_especialidade").fadeTo("slow", 100);
        });

        $("#exclui_especialidade").show("slow");
        

        return item.name;
    }
});

///BTN-DELETE
$('#btn_excluir').click(function() {

    var id = $("#frm_delete #id_especialidade").val();
    $.ajax({
        url: endereco + "especialidade/apaga/" + id,
        success: function() {
            $("html, body").animate({scrollTop: 0}, "slow");
            $('#confirma').modal('toggle');
            $("#exclui_especialidade").hide("slow");
            $("#busca_excluir").val("");
             oTable.fnReloadAjax(); //Atualiza o LISTAR
        },
        error: function() {
            console.log("AJAX request was a failure");
        }
    });

});