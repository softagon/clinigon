//Caminho do sistema, var endereco.
document.write('<script type="text/javascript" src="../assets/js/clinigon.js"></script>');
/*
 *  Controle da tela do paciente
 */

////FRM CAD 
$(document).ready(function() {
    $("#frm_cad").submit(function(event) {
        event.preventDefault();
    });
    $('#frm_cad').validate(
            {
                onkeyup: false,
                onclick: false,
                rules: {
                    cpf: {
                        remote: endereco + "paciente/cpf_valido_existe",
                        chave: true
                    },
                    sus: {
                        chave: true,
                        remote: endereco + "paciente/sus_valido_existe"
                    },
                    titulo_eleitor: {
                        chave: true,
                        remote: endereco + "paciente/titulo_eleitor_existe"
                    }
                },
                messages: {
                    cpf: {
                        remote: jQuery.validator.format("{0} já em uso por outro ou inválido.")
                    },
                    sus: {
                        remote: jQuery.validator.format("{0} já em uso por outro ou inválido.")
                    },
                    titulo_eleitor: {
                        remote: jQuery.validator.format("{0} já em uso por outro.")
                    }
                },
                submitHandler: function(form) {
                    var cadPaciente = $.ajax({
                        url: $(form).attr('action'),
                        type: $(form).attr('method'),
                        async: false,
                        data: $(form).serialize(),
                        dataType: "json",
                        beforeSend: function() {
                            $("#div-cad-paciente").hide("slow");
                        }
                    });
                    cadPaciente.done(function(msg) {
                        if (msg) { //boolean
                            $("html, body").animate({scrollTop: 0}, "slow");
                            $("#cad-ok").show("slow");
                            oTable.fnReloadAjax(); //Atualiza o LISTAR
                        } else {
                            $("html, body").animate({scrollTop: 0}, "slow");
                            $("#cad-erro").show("slow");
                        }
                         oTable.fnReloadAjax();
                    });
                    cadPaciente.fail(function(jqXHR, textStatus) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $("#cad-erro").show("slow");
                    });
                }
            });

    $.validator.addMethod("chave", function(value, element) {
        var cpf = $("#frm_cad #cpf").val();
        var sus = $("#frm_cad #sus").val();
        var titulo_eleitor = $("#frm_cad #titulo_eleitor").val();

        if (cpf == "" && sus == "" && titulo_eleitor == "")
            return false;
        else
            return true;

    }, "Um dos documentos é obrigatório");

    $("#frm_cad #cep").change(function(event) {
        var cepdata = $.ajax({
            url: endereco + "util/pegaendereco",
            type: "post",
            data: $("#cep").serialize(),
            dataType: 'json',
            beforeSend: function(x) {
                if (x && x.overrideMimeType) {
                    x.overrideMimeType("application/json;charset=UTF-8");
                }
            },
            error: function(request, error) {
                $('#output').html(error);
            },
            success: function(data) {
                if (data.success == true) {
                    var endereco = data.endereco + ", " + data.bairro;
                    $("#endereco").val(endereco);
                    $("#cidade").val(data.cidade);
                    $("select#uf option")
                            .each(function() {
                        this.selected = (this.text == data.uf);
                    });
                }
                else {
                    $('#output').html(data.success);
                }

            }
        });
    });
});
$("#btn-tentar").click(function(event) {
    $("#cad-erro").hide("slow");
    $("#div-cad-paciente").show("slow");
});
$("#btn-cad-novo").click(function(event) {
    $('#frm_cad').get(0).reset()
    $("#cad-ok").hide("slow");
    $("#div-cad-paciente").show("slow");
});



$("#frm_cad #celular").change(function(event) {
    $.ajax({
        url: endereco + "util/pega_operadora",
        type: "post",
        data: $(this).serialize(),
        dataType: 'json',
        beforeSend: function(x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json;charset=UTF-8");
            }
        },
        error: function(request, error) {
            $('#output').html(error);
        },
        success: function(data) {
            if (data.success == true) {
                $("#frm_cad #operadora").val(data.operadora);
            }
            else {
                $('#output').html(data.success);
            }

        }
    });
});

//////////////////////////
//Modificando #outro formulário
//////////////////////////


//////////////////////////////////
/// AUTO COMPLETE com TYPEAHEAD
/////////////////////////////////
$(document).ready(function() {
    $("#modifica-paciente").hide();
});
$('#busca-editar').typeahead({
    source: function(query, process) {
        var $url = endereco + 'paciente/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof(this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#idpaciente').val(item.id);

        $.get(endereco + "paciente/pega_paciente/" + item.id)
                .done(function(data) {

            //Formatando data de nascimento para exibição
            var fnascimento = data.nascimento;
            var fnasc = fnascimento.split('\/');
            fnascimento = fnasc[0] + "/" + fnasc[1] + "/" + fnasc[2];

            $("#frm_edit #cpf").val(data.cpf);
            $("#frm_edit #sus").val(data.sus);
            $("#frm_edit #titulo_eleitor").val(data.titulo_eleitor);
            $("#frm_edit #nome").val(data.nome);
            $("#frm_edit #nascimento").val(fnascimento);
            $("#frm_edit #sexo").val(data.sexo);
            $("#frm_edit #estado_civil").val(data.estado_civil);
            $("#frm_edit #telefone").val(data.telefone);
            $("#frm_edit #celular").val(data.celular);
            $("#frm_edit #email_paciente").val(data.email_paciente);
            $("#frm_edit #cep").val(data.cep);
            $("#frm_edit #cidade").val(data.cidade);
            $("#frm_edit #uf").val(data.uf);
            $("#frm_edit #endereco").val(data.endereco);
            $("#frm_edit #observacoes").val(data.observacoes);
            //perfurmaria
            $("#modifica-paciente").fadeTo("slow", 0);
            $("#modifica-paciente").fadeTo("slow", 100);
        });

        $("#modifica-paciente").show("slow");


        return item.name;
    }
});


//////// FRM EDIT , modificando paciente existente
$(document).ready(function() {
    $("#frm_edit").submit(function(event) {
        event.preventDefault();
    });
    $('#frm_edit').validate(
            {
                onkeyup: false,
                onclick: false,
                rules: {
                    cpf: {
                        remote: endereco + "paciente/cpf_valido",
                        chavet: true
                    },
                    sus: {
                        chavet: true,
                        remote: endereco + "paciente/sus_valido"
                    }
                },
                messages: {
                    cpf: {
                        remote: jQuery.validator.format("{0} já em uso por outro ou inválido.")
                    },
                    sus: {
                        remote: jQuery.validator.format("{0} já em uso por outro ou inválido.")
                    }
                },
                submitHandler: function(form) {
                    var cadPaciente = $.ajax({
                        url: $(form).attr('action'),
                        type: $(form).attr('method'),
                        async: false,
                        data: $(form).serialize(),
                        dataType: "json",
                        beforeSend: function() {
                            $("#modifica-paciente").toggle("slow");
                        }
                    });
                    cadPaciente.done(function(msg) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        if (msg) { //boolean
                            $("#ok_edit").show("slow");
                            oTable.fnReloadAjax(); //Atualiza o LISTAR
                        } else {
                            $("#erro_edit").show("slow");
                        }

                        //Oculta a mensagem de alerta depois de 3 segundos
                        $("#ok_edit").alert();
                        window.setTimeout(function() {
                            $("#ok_edit").alert('close');
                        }, 3000);
                    });
                    cadPaciente.fail(function(jqXHR, textStatus) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $("#cad-erro").show("slow");
                    });
                }
            });
    $.validator.addMethod("chavet", function(value, element) {
        var cpf = $("#frm_edit #cpf").val();
        var sus = $("#frm_edit #sus").val();
        var titulo_eleitor = $("#frm_edit #titulo_eleitor").val();

        if (cpf == "" && sus == "" && titulo_eleitor == "")
            return false;
        else
            return true;

    }, "Um dos documentos é obrigatório");

    $("#frm_edit #cep").change(function(event) {
        var cepdata = $.ajax({
            url: endereco + "util/pegaendereco",
            type: "post",
            data: $("#frm_edit #cep").serialize(),
            dataType: 'json',
            beforeSend: function(x) {
                if (x && x.overrideMimeType) {
                    x.overrideMimeType("application/json;charset=UTF-8");
                }
            },
            error: function(request, error) {
                $('#output').html(error);
            },
            success: function(data) {
                if (data.success == true) {
                    var endereco = data.endereco + ", " + data.bairro;
                    $("#frm_edit #endereco").val(endereco);
                    $("#frm_edit #cidade").val(data.cidade);
                    $("#frm_edit select#uf option")
                            .each(function() {
                        this.selected = (this.text == data.uf);
                    });
                }
                else {
                    $('#output').html(data.success);
                }

            }
        });
    });

});


$("#btn_tentar_edit").click(function(event) {
    $("#erro_edit").hide("slow");
});
$("#btn-edit").click(function(event) {
    $("#ok_edit").hide("slow");
});

$("#frm_edit #celular").change(function(event) {
    $.ajax({
        url: endereco + "util/pega_operadora",
        type: "post",
        data: $(this).serialize(),
        dataType: 'json',
        beforeSend: function(x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json;charset=UTF-8");
            }
        },
        error: function(request, error) {
            $('#output').html(error);
        },
        success: function(data) {
            if (data.success == true) {
                $("#frm_edit #operadora").val(data.operadora);
            }
            else {
                $('#output').html(data.success);
            }

        }
    });
});

////////////////////////////
/// EXCLUINDO
///////////////////////////
$(document).ready(function() {
    $("#exclui_paciente").hide();
});
$('#busca_excluir').typeahead({
    source: function(query, process) {
        var $url = endereco + 'paciente/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof(this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#id_paciente').val(item.id);

        $.get(endereco + "paciente/pega_paciente/" + item.id)
                .done(function(data) {
            //Formatando data de nascimento para exibição
            var fnascimento = data.nascimento;
            var fnasc = fnascimento.split('\/');
            fnascimento = fnasc[0] + "/" + fnasc[1] + "/" + fnasc[2];

            $("#frm_delete #cpf").val(data.cpf);
            $("#frm_delete #sus").val(data.sus);
            $("#frm_delete #titulo_eleitor").val(data.titulo_eleitor);
            $("#frm_delete #nome").val(data.nome);
            $("#frm_delete #nascimento").val(fnascimento);
            $("#frm_delete #sexo").val(data.sexo);
            $("#frm_delete #estado_civil").val(data.estado_civil);
            $("#frm_delete #telefone").val(data.telefone);
            $("#frm_delete #celular").val(data.celular);
            $("#frm_delete #email_paciente").val(data.email_paciente);
            $("#frm_delete #cep").val(data.cep);
            $("#frm_delete #cidade").val(data.cidade);
            $("#frm_delete #uf").val(data.uf);
            $("#frm_delete #endereco").val(data.endereco);
            $("#frm_delete #observacoes").val(data.observacoes);
            //perfurmaria
            $("#modifica-paciente").fadeTo("slow", 0);
            $("#modifica-paciente").fadeTo("slow", 100);
        });

        $("#exclui_paciente").show("slow");


        return item.name;
    }
});

///BTN-DELETE
$('#btn_excluir').click(function() {

    var id = $("#frm_delete #id_paciente").val();
    $.ajax({
        url: endereco + "paciente/apaga/" + id,
        success: function() {
            $("html, body").animate({scrollTop: 0}, "slow");
            $('#confirma').modal('toggle');
            $("#exclui_paciente").hide("slow");
            $("#busca_excluir").val("");
        },
        error: function() {
            console.log("AJAX request was a failure");
        }
    });

});