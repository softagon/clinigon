/*
 * Neste arquivo iremos realizar os seguintes passos:
 * 1 - Encontrar o Paciente
 * 2 - Checar se é o paciente ou um menor de idade, dependente
 * 3 - Selecionar o associado(médico) que atenderá
 * 4 - A forma de pagamento
 * 5 - Registrar a forma de pagamento
 */
//Caminho do sistema, var endereco.
document.write('<script type="text/javascript" src="../assets/js/clinigon.js"></script>');

//Validação dos campos do formulário


/*
 * 
 * Controle visual ao clicar nos botoes
 */
$(document).ready(function() {
    $("#frm_reg_con").validate();
    $("#btn_convenio_escolha").attr("disabled", false);
    $("#btn_particular_escolha").attr("disabled", false);

    $("#total").maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});
    $("#total_desconto").maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});
    $("#valor_pago_agora").maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});

});
$("#btn_paciente").click(function() {

    //Impede valores em branco
    if ($("#busca-paciente").val().length == 0) {
        $("paciente_alerta").alert();
    } else {
        $("#caixa-busca-paciente").hide("slow");
        $("#confirma-paciente").fadeOut("slow");
        $("#tipo_atendimento").show("slow");
    }
});
//Aqui decide se é convênio ou particular, escolha.
$("#btn_convenio_escolha").click(function() {
    
    $("#tipo_consulta").val("convênio");
    $('#associado_valor_lbl').text("Valor no convênio");
    $("#btn_particular_escolha").hide("slow");
    $("#btn_convenio_escolha").attr("disabled", true);
    $("#convenio").show("slow");
});
$("#btn_particular_escolha").click(function() {
    $("#tipo_consulta").val("particular");
    $('#associado_valor_lbl').text("Valor particular");
    $("#btn_convenio_escolha").hide("slow");
    $("#btn_particular_escolha").attr("disabled", true);
    $("#associado").show("slow");
});
//Escolheu convênio
$("#btn_convenio").click(function() {
    $("#caixa-busca-convenio").hide("slow");
    $("#confirma-convenio").fadeOut("slow");
    $("#associado").show("slow");
});
$("#btn_associado").click(function() {

    $("#caixa-busca-associado").hide("slow");
    $("#confirma-associado").fadeOut("slow");
    //Nesse momento validamos se é convênio, pois o cliente não paga nada
    var tipo_consulta = $("#tipo_consulta").val();
    console.log(tipo_consulta);
    if(tipo_consulta == "convênio") 
        $("#sem_pagamento").show("slow");
    else
        $("#forma_pagamento").show("slow");
});


//Calculo e descontos na forma de pagamento
$( "#total_desconto" ).change(function() {
   var total = $("#total_desconto").maskMoney('unmasked')[0];
   var totalsf = $("#totalsf").val();
   var totalsff = parseFloat(totalsf);
   var total_agora = totalsff - total;
   
   $("#valor_pago_agora").val(total_agora * 100);
});

//Caixa de buscas 
$('#busca-paciente').typeahead({
    source: function(query, process) {
        var $url = endereco + 'paciente/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof (this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#id_paciente_FK').val(item.id);

        $.get(endereco + "paciente/pega_paciente/" + item.id)
                .done(function(data) {
                    $("#paciente_dados").show("slow");

                    if (data.operadora.length != 0)
                        data.operadora = ' - ' + data.operadora;

                    $("#paciente_nome").text(data.nome);
                    $("#documento_paciente").text(data.documento);
                    $("#cidade_uf_paciente").text(data.cidade + '/' + data.uf);
                    $("#celular_paciente").text(data.telefone + data.operadora);
                    $("#idade_paciente").text(data.idade + ' anos');
                    $("#nascimento_paciente").text(data.nascimento);
                });

        return item.name;
    }
});

$('#busca-associado').typeahead({
    source: function(query, process) {
        var $url = endereco + 'associado/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof (this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#id_associado_FK').val(item.id);
        $.get(endereco + "associado/pega_associado/" + item.id)
                .done(function(data) {
                    $("#associado_dados").show("slow");
                    $("#associado_nome").text(data.nome);
                    $("#documento_associado").text(data.documento);
                    $("#especialidade").text(data.especialidade);

                    var tipo_consulta = $("#tipo_consulta").val();
                    if (tipo_consulta == "particular") {
                        $("#valor").text(data.valorf);
                        $("#total").val(data.valorf);
                        $("#totalsf").val(data.valor);

                    } else {
                        if (data.valor_conveniof == "R$ 0,00") {
                            alert("Atenção, esse Associado(Médico) não tem valor(R$) de convênio registrado.");
                            window.setTimeout('location.reload()', 1000); //para tudo. recarrega a página.
                        } else {
                            $("#valor").text(data.valor_conveniof);
                        }
                    }
                });

        return item.name;
    }
});

/*
 * Aqui começamos a parte do convênio, busca e registro
 */
$('#busca-convenio').typeahead({
    source: function(query, process) {
        var url = endereco + 'convenio/auto_complete/' + query;
        var items = new Array;
        items = [""];
        $.ajax({
            url: url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        documento: data.document,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof (this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    items.push(group);
                });

                process(items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);

        var cart = new Object();
        //Essa coleta de dados é necessária para gravar na sessão da venda
        var pegando = $.fn.pegando = $.get(endereco + "convenio/pega_convenio/" + item.id, function() {
            console.log("pegou convenio");
        })
        pegando.done(function(data) {
            $('#remove_espaco').remove(); //organizando
            $("#convenio_dados").show("slow");
            $("#convenio_nome").text(data.nome);
            $("#convenio_telefone").text(data.telefone);
            $("#convenio_responsavel").text(data.responsavel);
            $("#convenio_cidade_uf").text(data.cidade + '/' + data.uf);

            cart.id_convenio = data.id;
            cart.convenio_nome = data.nome;
            cart.convenio_telefone = data.telefone;
            cart.convenio_responsavel = data.convenio_responsavel;
            $("#id_convenio_FK").val(data.id);

        });
        return item.name;
    }
});
