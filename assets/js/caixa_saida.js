//Caminho do sistema, var endereco.
document.write('<script type="text/javascript" src="../assets/js/clinigon.js"></script>');
/*
 *  Controle da tela do usuario
 */

////FRM CAD 
//
//Força da senha
jQuery(document).ready(function() {
    "use strict";
    var options = {};
    options.ui = {
        container: "#div-cad-usuario",
        viewports: {
            progress: ".pwstrength_viewport_progress",
            verdict: ".pwstrength_viewport_verdict"
        }
    };
    options.common = {
        onLoad: function() {
            $('#mensagens_cad').text('Força da senha');
        }
    };
    $('#frm_cad #senha').pwstrength(options);
});
//frm
$(document).ready(function() {
    $("#frm_cad").submit(function(event) {
        event.preventDefault();
    });
    $('#frm_cad').validate(
            {
                onkeyup: false,
                onclick: false,
                rules: {
                    senha: {
                        minlength: 5
                    },
                    csenha: {
                        minlength: 5,
                        equalTo: "#senha"
                    },
                    email_usuario: {
                        required: true,
                        remote: endereco + "usuario/email_existe"
                    }
                },
                messages: {
                    csenha: {
                        remote: jQuery.validator.format("{0} no mínimo 5 digítos e igual.")
                    },
                    email_usuario: {
                        remote: jQuery.validator.format("{0} já em uso por outro.")
                    }
                },
                submitHandler: function(form) {
                    var cadAssociado = $.ajax({
                        url: $(form).attr('action'),
                        type: $(form).attr('method'),
                        async: false,
                        data: $(form).serialize(),
                        dataType: "json",
                        beforeSend: function() {
                            $("#div-cad-usuario").hide("slow");
                        }
                    });
                    cadAssociado.done(function(msg) {
                        if (msg) { //boolean
                            $("html, body").animate({scrollTop: 0}, "slow");
                            $("#cad-ok").show("slow");
                             oTable.fnReloadAjax(); //Atualiza o LISTAR
                        } else {
                            $("html, body").animate({scrollTop: 0}, "slow");
                            $("#cad-erro").show("slow");
                        }
                    });
                    cadAssociado.fail(function(jqXHR, textStatus) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $("#cad-erro").show("slow");
                    });
                }
            });

    $("#email_usuario").change(function(event) {
        var email_usuario = $.ajax({
            url: endereco + "usuario/pega_dados",
            type: "post",
            data: $("#email_usuario").serialize(),
            dataType: 'json',
            beforeSend: function(x) {
                if (x && x.overrideMimeType) {
                    x.overrideMimeType("application/json;charset=UTF-8");
                }
            },
            error: function(request, error) {
                $('#output').html(error);
            },
            success: function(data) {
                if (data.success == true) {
                    $("#frm_cad #nome").val(data.nome);
                }
                else {
                    $('#output').html(data.success);
                }

            }
        });
    });
});
$("#btn-tentar").click(function(event) {
    $("#cad-erro").hide("slow");
    $("#div-cad-usuario").show("slow");
});
$("#btn-cad-novo").click(function(event) {
    $('#frm_cad').get(0).reset()
    $("#cad-ok").hide("slow");
    $("#div-cad-usuario").show("slow");
});
//////////////////////////
//Modificando #outro formulário
//////////////////////////


//////////////////////////////////
/// AUTO COMPLETE com TYPEAHEAD
/////////////////////////////////
$(document).ready(function() {
    $("#modifica-usuario").hide();
});
$('#busca-editar').typeahead({
    source: function(query, process) {
        var $url = endereco + 'usuario/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof(this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#idusuario').val(item.id);

        $.get(endereco + "usuario/pega_usuario/" + item.id)
                .done(function(data) {
            $("#frm_edit #nome").val(data.nome);
            $("#frm_edit #id_usuario_nivel_FK").val(data.id_usuario_nivel_FK);
            $("#frm_edit #email_usuario").val(data.email_usuario);
            //perfurmaria
            $("#modifica-usuario").fadeTo("slow", 0);
            $("#modifica-usuario").fadeTo("slow", 100);
        });

        $("#modifica-usuario").show("slow");


        return item.name;
    }
});

//////// FRM EDIT , modificando usuario existente
//Força da senha
jQuery(document).ready(function() {
    "use strict";
    var options = {};
    options.ui = {
        container: "#div-edit-usuario",
        viewports: {
            progress: ".pwstrength_viewport_progress",
            verdict: ".pwstrength_viewport_verdict"
        }
    };
    options.common = {
        onLoad: function() {
            $('#mensagens_edit').text('Força da senha');
        }
    };
    $('#frm_edit #senha').pwstrength(options);
});
$(document).ready(function() {
    $("#frm_edit").submit(function(event) {
        event.preventDefault();
    });
    $('#frm_edit').validate(
            {
                onkeyup: false,
                onclick: false,
                rules: {
                    senha: {
                        minlength: 5
                    },
                    csenha: {
                        minlength: 5,
                        equalTo: "#frm_edit #senha"
                    },
                    email_usuario: {
                        required: true
                    }
                },
                messages: {
                    csenha: {
                        remote: jQuery.validator.format("{0} no mínimo 5 digítos e igual.")
                    },
                    email_usuario: {
                        remote: jQuery.validator.format("{0} já em uso por outro.")
                    }
                },
                submitHandler: function(form) {
                    var cadAssociado = $.ajax({
                        url: $(form).attr('action'),
                        type: $(form).attr('method'),
                        async: false,
                        data: $(form).serialize(),
                        dataType: "json",
                        beforeSend: function() {
                            $("#modifica-usuario").toggle("slow");
                        }
                    });
                    cadAssociado.done(function(msg) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        if (msg) { //boolean
                            $("#ok_edit").show("slow");
                             oTable.fnReloadAjax(); //Atualiza o LISTAR
                        } else {
                            $("#erro_edit").show("slow");
                        }

                        //Oculta a mensagem de alerta depois de 3 segundos
                        $("#ok_edit").alert();
                        window.setTimeout(function() {
                            $("#ok_edit").alert('close');
                        }, 3000);
                    });
                    cadAssociado.fail(function(jqXHR, textStatus) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $("#cad-erro").show("slow");
                    });
                }
            });

});
$("#btn_tentar_edit").click(function(event) {
    $("#erro_edit").hide("slow");
});
$("#btn-edit").click(function(event) {
    $("#ok_edit").hide("slow");
});

////////////////////////////
/// EXCLUINDO
///////////////////////////
$(document).ready(function() {
    $("#exclui_usuario").hide();
});
$('#busca_excluir').typeahead({
    source: function(query, process) {
        var $url = endereco + 'usuario/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof(this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#id_usuario').val(item.id);

        $.get(endereco + "usuario/pega_usuario/" + item.id)
                .done(function(data) {
            $("#frm_delete #nome").val(data.nome);
            $("#frm_delete #email_usuario").val(data.email_usuario);
            $("#frm_delete #id_usuario_nivel_FK").val(data.id_usuario_nivel_FK);
            //perfurmaria
            $("#exclui_usuario").fadeTo("slow", 0);
            $("#exclui_usuario").fadeTo("slow", 100);
        });

        $("#exclui_usuario").show("slow");


        return item.name;
    }
});

///BTN-DELETE
$('#btn_excluir').click(function() {

    var id = $("#frm_delete #id_usuario").val();
    $.ajax({
        url: endereco + "usuario/apaga/" + id,
        success: function() {
            $("html, body").animate({scrollTop: 0}, "slow");
            $('#confirma').modal('toggle');
            $("#exclui_usuario").hide("slow");
            $("#busca_excluir").val("");
             oTable.fnReloadAjax(); //Atualiza o LISTAR
        },
        error: function() {
            console.log("AJAX request was a failure");
        }
    });

});