//Caminho do sistema, var endereco.
document.write('<script type="text/javascript" src="../assets/js/clinigon.js"></script>');
/*
 *  Controle da tela do saida_caixa
 */

////FRM CAD 
//frm
$(document).ready(function() {
     $('#frm_cad #valor').each(function() {
        $(this).maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});
    });
    $("#frm_cad").submit(function(event) {
        event.preventDefault();
    });
    $('#frm_cad').validate(
            {
                onkeyup: false,
                onclick: false,
                submitHandler: function(form) {
                    var cadAssociado = $.ajax({
                        url: $(form).attr('action'),
                        type: $(form).attr('method'),
                        async: false,
                        data: $(form).serialize(),
                        dataType: "json",
                        beforeSend: function() {
                            $("#div-cad-saida_caixa").hide("slow");
                        }
                    });
                    cadAssociado.done(function(msg) {
                        if (msg) { //boolean
                            $("html, body").animate({scrollTop: 0}, "slow");
                            $("#cad-ok").show("slow");
                            oTable.fnReloadAjax(); //Atualiza o LISTAR
                        } else {
                            $("html, body").animate({scrollTop: 0}, "slow");
                            $("#cad-erro").show("slow");
                        }
                        oTable.fnReloadAjax();
                    });
                    cadAssociado.fail(function(jqXHR, textStatus) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $("#cad-erro").show("slow");
                    });
                }
            });

});
$("#btn-tentar").click(function(event) {
    $("#cad-erro").hide("slow");
    $("#div-cad-saida_caixa").show("slow");
});
$("#btn-cad-novo").click(function(event) {
    $('#frm_cad').get(0).reset()
    $("#cad-ok").hide("slow");
    $("#div-cad-saida_caixa").show("slow");
});
//////////////////////////
//Modificando #outro formulário
//////////////////////////


//////////////////////////////////
/// AUTO COMPLETE com TYPEAHEAD
/////////////////////////////////
$(document).ready(function() {
    $("#modifica-saida_caixa").hide();
});
$('#busca-editar').typeahead({
    source: function(query, process) {
        var $url = endereco + 'saida_caixa/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof (this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#idsaida_caixa').val(item.id);

        $.get(endereco + "saida_caixa/pega_saida_caixa/" + item.id)
                .done(function(data) {
                    $("#frm_edit #nome").val(data.nome);
                    $("#frm_edit #valor").val(data.valor);
                    $("#frm_edit #valor").maskMoney('mask');
                    $("#frm_edit #id_centro_custo_FK").val(data.id_centro_custo_FK);
                   
                    //perfurmaria
                    $("#modifica-saida_caixa").fadeTo("slow", 0);
                    $("#modifica-saida_caixa").fadeTo("slow", 100);
                });

        $("#modifica-saida_caixa").show("slow");


        return item.name;
    }
});

//////// FRM EDIT , modificando saida_caixa existente
$(document).ready(function() {
     $("#frm_edit #valor").maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});
    $("#frm_edit").submit(function(event) {
        event.preventDefault();
    });
    $('#frm_edit').validate(
            {
                onkeyup: false,
                onclick: false,
                submitHandler: function(form) {
                    var cadAssociado = $.ajax({
                        url: $(form).attr('action'),
                        type: $(form).attr('method'),
                        async: false,
                        data: $(form).serialize(),
                        dataType: "json",
                        beforeSend: function() {
                            $("#modifica-saida_caixa").toggle("slow");
                        }
                    });
                    cadAssociado.done(function(msg) {

                        $("html, body").animate({scrollTop: 0}, "slow");
                        if (msg) { //boolean
                            $("#ok_edit").toggle("slow");
                            $("#ok_edit").alert();
                            window.setTimeout(function() {
                                $("#ok_edit").alert('close');
                            }, 3000);
//                            console.log("Retorno X " + msg);
                            oTable.fnReloadAjax(); //Atualiza o LISTAR
                        } else {
                            $("#erro_edit").toggle("slow");
                        }

                        //Oculta a mensagem de alerta depois de 3 segundos


                    });
                    cadAssociado.fail(function(jqXHR, textStatus) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $("#cad-erro").show("slow");
                    });
                }
            });

});
$("#btn_tentar_edit").click(function(event) {
    $("#erro_edit").hide("slow");
});
$("#btn-edit").click(function(event) {
    $("#ok_edit").hide("slow");
});

////////////////////////////
/// EXCLUINDO
///////////////////////////
$(document).ready(function() {
      $("#frm_delete #valor").maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});
    $("#exclui_saida_caixa").hide();
});
$('#busca_excluir').typeahead({
    source: function(query, process) {
        var $url = endereco + 'saida_caixa/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof (this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#id_saida_caixa').val(item.id);

        $.get(endereco + "saida_caixa/pega_saida_caixa/" + item.id)
                .done(function(data) {
                    $("#frm_delete #nome").val(data.nome);
                    $("#frm_delete #valor").val(data.valor);
                    $("#frm_delete #valor").maskMoney("mask");
                    $("#frm_delete #id_centro_custo_FK").val(data.id_centro_custo_FK);
                                        //perfurmaria
                    $("#exclui_saida_caixa").fadeTo("slow", 0);
                    $("#exclui_saida_caixa").fadeTo("slow", 100);
                });

        $("#exclui_saida_caixa").show("slow");


        return item.name;
    }
});

///BTN-DELETE
$('#btn_excluir').click(function() {

    var id = $("#frm_delete #id_saida_caixa").val();
    $.ajax({
        url: endereco + "saida_caixa/apaga/" + id,
        success: function() {
            $("html, body").animate({scrollTop: 0}, "slow");
            $('#confirma').modal('toggle');
            $("#exclui_saida_caixa").hide("slow");
            $("#busca_excluir").val("");
            oTable.fnReloadAjax(); //Atualiza o LISTAR
        },
        error: function() {
            console.log("AJAX request was a failure");
        }
    });

});