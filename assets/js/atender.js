//Caminho do sistema, var endereco.
document.write('<script type="text/javascript" src="../assets/js/clinigon.js"></script>');

$(document).ready(function() {
    $("#paciente_dados").hide();
    $("#dependente").hide();
    $("#subtotal-div").hide();

    $('#passo02').bind('click', false);
    $('#passo03').bind('click', false);


});
$('#busca-paciente').typeahead({
    source: function(query, process) {
        var url = endereco + 'paciente/auto_complete/' + query;
        var items = new Array;
        items = [""];
        $.ajax({
            url: url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        documento: data.document,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof(this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    items.push(group);
                });

                process(items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#id_paciente').val(item.id);

        $.get(endereco + "paciente/pega_paciente/" + item.id)
                .done(function(data) {
            $("#paciente_nome").text(data.nome);
            $("#paciente").text(data.nome);
            $("#documento_paciente").text(data.documento);
            $("#cidade_uf").text(data.cidade + '/' + data.uf);
            $("#celular").text(data.telefone + ' - ' + data.operadora);
            $("#idade").text(data.idade + ' anos');
            $("#nascimento_paciente").text(data.nascimento);

            $("#paciente2_nome").text(data.nome);
            $("#paciente2").text(data.nome);
            $("#documento_paciente2").text(data.documento);
            $("#paciente2_cidade_uf").text(data.cidade + '/' + data.uf);
            $("#paciente2_celular").text(data.celular + ' - ' + data.operadora);
            $("#idade2").text(data.idade + ' anos');
            $("#nascimento2_paciente").text(data.nascimento);

            $("#paciente_dados").show();
            $("#dependente").show("slow");
        });
        return item.name;
    }
});

//Habilita passo 2
$("#este_paciente").click(function(event) {

    $('#collapseTwo').toggle("slow");
    $('#collapseOne').toggle("slow");
    $('#passo02').unbind('click', false);
});

//Selecionando se é convenio ou particular
$(document).ready(function() {
    $("#busca-convenio").hide();
    $("#convenio_dados").hide();
    $("#convenio").click(function(event) {

        $('#busca-convenio').toggle("slow");
        $('#particular-div').toggle("slow");
    })
    $("#particular").click(function(event) {
        
        //Exibe caixa da subtotal, pois só deve ser exposta no particular
        $("#subtotal-div").show();
        
        //O ID CONVENIO = 1, significa que é particular
        var cart = new Object();
        cart.id_convenio = 1;
        var registra = $.ajax({
            type: "POST",
            url: endereco + "atender/registra_convenio/",
            data: cart
        })
        registra.done(function(msg) {
            $("#id_convenio").val(1); //O ID CONVENIO = 1, significa que é particular
            $('#collapseTwo').toggle("slow");
            $('#collapseThree').toggle("slow");
            $('#passo03').unbind('click', false);
        })


    })
});

//Finalizar atendimento
$("#finalizando").click(function(event) {
    $('#id_forma_pagamento_FK option[value=1]').attr('selected','selected');
    if ($("#id_paciente").val().length == 0 || $("#subtotal").val().length == 0) {
        alert("Faltou o paciente.");
        return false;
    } else
        console.log($("#id_paciente").val());
});



