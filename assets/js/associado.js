//Caminho do sistema, var endereco.
document.write('<script type="text/javascript" src="../assets/js/clinigon.js"></script>');
/*
 *  Controle da tela do associado
 */

////FRM CAD 
$(document).ready(function() {
    $('#frm_cad #valor, #valor_convenio').each(function() {
        $(this).maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});
    });
    $("#frm_cad").submit(function(event) {
        event.preventDefault();
    });
    $('#frm_cad').validate(
            {
                onkeyup: false,
                onclick: false,
                rules: {
                    documento: {
                        required: true,
                        remote: endereco + "associado/documento_existe"
                    },
                    email_associado: {
                        required: true,
                        remote: endereco + "associado/email_existe"
                    }
                },
                messages: {
                    documento: {
                        remote: jQuery.validator.format("{0} já em uso por outro.")
                    },
                    email_associado: {
                        remote: jQuery.validator.format("{0} já em uso por outro.")
                    }
                },
                submitHandler: function(form) {
                    var cadAssociado = $.ajax({
                        url: $(form).attr('action'),
                        type: $(form).attr('method'),
                        async: false,
                        data: $(form).serialize(),
                        dataType: "json",
                        beforeSend: function() {
                            $("#div-cad-associado").hide("slow");
                        }
                    });
                    cadAssociado.done(function(msg) {
                        if (msg) { //boolean
                            $("html, body").animate({scrollTop: 0}, "slow");
                            $("#cad-ok").show("slow");
                            oTable.fnReloadAjax(); //Atualiza o LISTAR
                        } else {
                            $("html, body").animate({scrollTop: 0}, "slow");
                            $("#cad-erro").show("slow");
                        }
                    });
                    cadAssociado.fail(function(jqXHR, textStatus) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $("#cad-erro").show("slow");
                    });
                }
            });
});
$("#btn-tentar").click(function(event) {
    $("#cad-erro").hide("slow");
    $("#div-cad-associado").show("slow");
});
$("#btn-cad-novo").click(function(event) {
    $('#frm_cad').get(0).reset()
    $("#cad-ok").hide("slow");
    $("#div-cad-associado").show("slow");
});


$("#frm_cad #celular").change(function(event) {
    $.ajax({
        url: endereco + "util/pega_operadora",
        type: "post",
        data: $(this).serialize(),
        dataType: 'json',
        beforeSend: function(x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json;charset=UTF-8");
            }
        },
        error: function(request, error) {
            $('#output').html(error);
        },
        success: function(data) {
            if (data.success == true) {
                $("#frm_cad #operadora").val(data.operadora);
            }
            else {
                $('#output').html(data.success);
            }

        }
    });
});

//////////////////////////
//Modificando #outro formulário
//////////////////////////
/// AUTO COMPLETE com TYPEAHEAD
/////////////////////////////////
$(document).ready(function() {
    $("#modifica-associado").hide();
});
$('#busca-editar').typeahead({
    source: function(query, process) {
        var $url = endereco + 'associado/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof(this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#idassociado').val(item.id);

        $.get(endereco + "associado/pega_associado/" + item.id)
                .done(function(data) {
            $("#frm_edit #nome").val(data.nome);
            $("#frm_edit #documento").val(data.documento);
            $("#frm_edit #email_associado").val(data.email_associado);
            $("#frm_edit #telefone").val(data.telefone);
            $("#frm_edit #celular").val(data.celular);
            $("#frm_edit #comissao").val(data.comissao);
            $("#frm_edit #id_especialidade_FK").val(data.id_especialidade_FK);
            $("#frm_edit #valor").val(data.valor);
            $("#frm_edit #valor").maskMoney('mask');
            $("#frm_edit #valor_convenio").val(data.valor_convenio);
            $("#frm_edit #valor_convenio").maskMoney('mask');
            //perfurmaria
            $("#modifica-associado").fadeTo("slow", 0);
            $("#modifica-associado").fadeTo("slow", 100);
        });

        $("#modifica-associado").show("slow");


        return item.name;
    }
});

//////// FRM EDIT , modificando associado existente
$(document).ready(function() {
    $("#frm_edit #valor").maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});

    $("#frm_edit").submit(function(event) {
        event.preventDefault();

    });
    $('#frm_edit').validate(
            {
                onkeyup: false,
                onclick: false,
                submitHandler: function(form) {
                    var cadAssociado = $.ajax({
                        url: $(form).attr('action'),
                        type: $(form).attr('method'),
                        async: false,
                        data: $(form).serialize(),
                        dataType: "json",
                        beforeSend: function() {
                            $("#modifica-associado").toggle("slow");
                        }
                    });
                    cadAssociado.done(function(msg) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        if (msg) { //boolean
                            $("#ok_edit").show("slow");
                            oTable.fnReloadAjax(); //Atualiza o LISTAR
                        } else {
                            $("#erro_edit").show("slow");
                        }

                        //Oculta a mensagem de alerta depois de 3 segundos
                        $("#ok_edit").alert();
                        window.setTimeout(function() {
                            $("#ok_edit").alert('close');
                        }, 3000);
                    });
                    cadAssociado.fail(function(jqXHR, textStatus) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $("#cad-erro").show("slow");
                    });
                }
            });

});
$("#btn_tentar_edit").click(function(event) {
    $("#erro_edit").hide("slow");
});
$("#btn-edit").click(function(event) {
    $("#ok_edit").hide("slow");
});

$("#frm_edit #celular").change(function(event) {
    $.ajax({
        url: endereco + "util/pega_operadora",
        type: "post",
        data: $(this).serialize(),
        dataType: 'json',
        beforeSend: function(x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json;charset=UTF-8");
            }
        },
        error: function(request, error) {
            $('#output').html(error);
        },
        success: function(data) {
            if (data.success == true) {
                $("#frm_edit  #operadora").val(data.operadora);
            }
            else {
                $('#output').html(data.success);
            }

        }
    });
});

////////////////////////////
/// EXCLUINDO
///////////////////////////
$(document).ready(function() {
    $("#exclui_associado").hide();
    $("#frm_delete #valor").maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});
});
$('#busca_excluir').typeahead({
    source: function(query, process) {
        var $url = endereco + 'associado/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof(this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#id_associado').val(item.id);

        $.get(endereco + "associado/pega_associado/" + item.id)
                .done(function(data) {
            $("#frm_delete #nome").val(data.nome);
            $("#frm_delete #documento").val(data.documento);
            $("#frm_delete #email_associado").val(data.email_associado);
            $("#frm_delete #telefone").val(data.telefone);
            $("#frm_delete #celular").val(data.celular);
            $("#frm_delete #comissao").val(data.comissao);
            $("#frm_delete #id_especialidade_FK").val(data.id_especialidade_FK);
            $("#frm_delete #valor").val(data.valor);
            $("#frm_delete #valor").maskMoney('mask');
            $("#frm_delete #valor_convenio").val(data.valor_convenio);
            $("#frm_delete #valor_convenio").maskMoney('mask');
            //perfurmaria
            $("#exclui_associado").fadeTo("slow", 0);
            $("#exclui_associado").fadeTo("slow", 100);
        });

        $("#exclui_associado").show("slow");


        return item.name;
    }
});

///BTN-DELETE
$('#btn_excluir').click(function() {

    var id = $("#frm_delete #id_associado").val();
    $.ajax({
        url: endereco + "associado/apaga/" + id,
        success: function() {
            $("html, body").animate({scrollTop: 0}, "slow");
            $('#confirma').modal('toggle');
            $("#exclui_associado").hide("slow");
            $("#busca_excluir").val("");
            oTable.fnReloadAjax(); //Atualiza o LISTAR
        },
        error: function() {
            console.log("AJAX request was a failure");
        }
    });

});