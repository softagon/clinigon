//Caminho do sistema, var endereco.
document.write('<script type="text/javascript" src="../assets/js/clinigon.js"></script>');
/* 
 * Código que gerencia a tabela no momento de listar
 * os dados que vem do banco de dados.
 * 
 * Utiliza o DataTables com Bootstrap
 * @author Hermes Alves
 * @description var ajaxurl é preenchida no momento da chamada "fornecedor/pega_fornecedores"
 */
/* Table initialisation */

var oTable;
jQuery.fn.dataTableExt.aTypes.push(
    function ( sData ) {
        return 'html';
    }
);


$(document).ready(function() {
    oTable = $('#DT-tabela').dataTable({
        "oLanguage": {
            "oPaginate": {
                "sFirst": "Primeira",
                "sLast": "Última",
                "sNext": "Próxima",
                "sPrevious": "Anterior"
            },
            "sInfo": "Temos _TOTAL_ registros para exibir (_START_ de _END_)",
            "sInfoEmpty": "Nada encontrado",
            "sEmptyTable": "Nada cadastrado ainda.",
            "sLoadingRecords": "Buscando...",
            "sProcessing": '<div class="alert alert-info dt-busca">Estamos trabalhando nos dados...</div>',
            "sSearch": "",
            "sZeroRecords": "Nada cadastrado...",
            "sLengthMenu": "Exibindo _MENU_ registros",
            "sInfoFiltered": " - da busca de _MAX_ registros"
        },
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": endereco + ajaxurl,
        "sServerMethod": "POST",
        "aaSorting": [[0, "desc"]],
        "sDom": 'T<"clear">lfrtip',
        "oTableTools": {
            "sSwfPath": local + "assets/swf/copy_csv_xls_pdf.swf",
            "aButtons": [
                {"sExtends": "copy", "sButtonText": "Copiar"},
                {"sExtends": "print", "sButtonText": "Imprimir"},
                {
                    "sExtends": "collection",
                    "sButtonText": 'Salvar <span class="caret" />',
                    "aButtons": ["csv", "xls",
                        {
                            "sExtends": "pdf",
                            "sPdfMessage": "<?= $data['titulo'] ?>"
                        }]
                }
            ]
        }

    });
    $("#DT-tabela_filter > label > input").attr("placeholder", "Busque por aqui, digite.");
    $("#DT-tabela_filter > label > input").attr("class", "form-control input-sm");

});