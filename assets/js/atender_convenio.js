
$('#busca_convenio').typeahead({
    source: function(query, process) {
        var url = endereco + 'convenio/auto_complete/' + query;
        var items = new Array;
        items = [""];
        $.ajax({
            url: url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        documento: data.document,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof(this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    items.push(group);
                });

                process(items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);

        var cart = new Object();
        //Essa coleta de dados é necessária para gravar na sessão da venda
        var pegando = $.fn.pegando = $.get(endereco + "convenio/pega_convenio/" + item.id, function() {
            console.log("pegou convenio");
        })
        pegando.done(function(data) {
            $('#remove_espaco').remove(); //organizando
            $("#convenio_dados").show("slow");
            $("#convenio_nome").text(data.nome);
            $("#convenio_telefone").text(data.telefone);
            $("#convenio_responsavel").text(data.responsavel);
            $("#convenio_cidade_uf").text(data.cidade + '/' + data.uf);

            cart.id_convenio = data.id;
            cart.convenio_nome = data.nome;
            cart.convenio_telefone = data.telefone;
            cart.convenio_responsavel = data.convenio_responsavel;
            $("#id_convenio").val(data.id);



        });
        return item.name;
    }
});

$("#confirmar_convenio").click(function(event) {
//Aqui registra na sessão o convenio utilizado, pode ser 1=particular
    var cart = new Object();
    cart.id_convenio = $("#id_convenio").val();
    var registra = $.ajax({
        type: "POST",
        url: endereco + "atender/registra_convenio/",
        data: cart
    })
    registra.done(function(msg) {
        $('#collapseTwo').toggle("slow");
        $('#collapseThree').toggle("slow");
        $('#passo03').unbind('click', false);
    })
});