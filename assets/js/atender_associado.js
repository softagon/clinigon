
$('#busca_associado').typeahead({
    source: function(query, process) {
        var url = endereco + 'associado/auto_complete/' + query;
        var items = new Array;
        items = [""];
        $.ajax({
            url: url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        documento: data.document,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof(this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    items.push(group);
                });

                process(items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);

        var cart = new Object();
        //Essa coleta de dados é necessária para gravar na sessão da venda
        var pegando = $.fn.pegando = $.get(endereco + "associado/pega_associado/" + item.id, function() {
            console.log("pegou associado");
        })
        pegando.done(function(data) {

            $("#nome").text(data.nome);
            $("#paciente").text(data.nome);
            $("#documento_paciente").text(data.documento);
            $("#cidade_uf").text(data.cidade + '/' + data.uf);
            $("#celular").text(data.celular + ' - ' + data.operadora);

            cart.id_associado = data.id;
            cart.associado_nome = data.nome;
            cart.associado_doc = data.documento;
            cart.especialidade = data.especialidade;
            cart.associado_valor = data.valor;
            cart.associado_valorf = data.valorf;
            cart.id_paciente = $("#id_paciente").val();

            //Aqui registra na sessão da venda
            var registra = $.ajax({
                type: "POST",
                url: endereco + "atender/registra_associado/",
                data: cart
            })
            registra.done(function(msg) {
                //Nest momento preenche a tela com os itens da sessão
                var pega = $.get(endereco + "atender/pega_associados_sessao/", function() {
                    console.log("pegou na sessao associado");
                })

                pega.done(function(data) {

                    //Gera a tela que exibe os associados
                    $("#associados").fadeOut(500);
                    $("#associado_dados > tbody").html("");

                    var itemCount = 0;

                    var vend = new Object();
//                    alert(data.carrinho.total);
                    //O loop abaixo cria as linhas da tabela
                    for (var k in data) {
                        if (data[k].id_associado) {
                            var newLine = '<tr id="a' + itemCount + '" class="a' + data[k].id_associado + '" idbd="' + data[k].id_associado + '" valor="' + data[k].associado_valor + '"><td>' + data[k].associado_nome + '</td><td>' + data[k].associado_doc + '</td><td>' + data[k].especialidade + '</td><td>' + data[k].associado_valorf + '</td><td><span class="glyphicon glyphicon-minus-sign" style="cursor: pointer; cursor: hand;"></span></td></tr>';
                            $('#associado_dados tbody').append(newLine);
                        }

                        $("#a" + itemCount).click(function() {
                            var id_associado = $(this).attr("idbd");
                            var contador = $(this).attr("id");
                            var id = $(this).attr("class");
                            var valor = $(this).attr("valor");

                            //Aqui REMOVE na sessão da venda
                            vend.id_associado = id_associado;
                            vend.itemCount = contador;
                            var removendo = $.ajax({
                                type: "POST",
                                url: endereco + "atender/remove_associado/",
                                data: vend,
                                global: false,
                                success: function(data) {
                                    result = data;
                                    console.log("removido com sucesso");
                                    $("." + id).remove();
                                    $("#subtotal").val(data.total);
                                }
                            }).responseText;

                        });
                        itemCount++;

                    }
                    $("#subtotal").val(data.carrinho.total);
                    $("#subtotal").maskMoney('mask');
                    $("#associados").fadeIn(1000);
                });
            });

        });
        return item.name;
    }
});

$(document).ready(function() {
    $("#subtotal").maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});
});
