$('#busca-editar').typeahead({
    source: function(query, process) {
        var $url = endereco + 'exame/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof (this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#idexame').val(item.id);

        $.get(endereco + "exame/pega_exame/" + item.id)
                .done(function(data) {
                    $("#frm_edit #nome").val(data.nome);
                    $("#frm_edit #comissao").val(data.comissao);
                    $("#frm_edit #valor").val(data.valor);
                    $("#frm_edit #valor").maskMoney('mask');
                    $("#frm_edit #valor_convenio").val(data.valor_convenio);
                    $("#frm_edit #valor_convenio").maskMoney('mask');
                    //perfurmaria
                    //perfurmaria
                    $("#modifica-exame").fadeTo("slow", 0);
                    $("#modifica-exame").fadeTo("slow", 100);
                });

        $("#modifica-exame").show("slow");


        return item.name;
    }
});
