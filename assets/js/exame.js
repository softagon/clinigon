//Caminho do sistema, var endereco.
document.write('<script type="text/javascript" src="../assets/js/clinigon.js"></script>');
/*
 *  Controle da tela do exame
 */

////FRM CAD 
$(document).ready(function() {
    $('#frm_cad #valor').each(function() {
        $(this).maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});
    });
    $('#frm_cad #valor_convenio').each(function() {
        $(this).maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});
    });
    $("#frm_cad").submit(function(event) {
        event.preventDefault();
    });
    $('#frm_cad').validate(
            {
                onkeyup: false,
                onclick: false,
                submitHandler: function(form) {
                    var cadExame = $.ajax({
                        url: $(form).attr('action'),
                        type: $(form).attr('method'),
                        async: false,
                        data: $(form).serialize(),
                        dataType: "json",
                        beforeSend: function() {
                            $("#div-cad-exame").hide("slow");
                        }
                    });
                    cadExame.done(function(msg) {
                        oTable.fnReloadAjax(); //Atualiza o LISTAR
                        if (msg) { //boolean
                            $("html, body").animate({scrollTop: 0}, "slow");
                            $("#cad-ok").show("slow");

                        } else {
                            $("html, body").animate({scrollTop: 0}, "slow");
                            $("#cad-erro").show("slow");
                        }
                    });
                    cadExame.fail(function(jqXHR, textStatus) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $("#cad-erro").show("slow");
                    });
                }
            });
});
$("#btn-tentar").click(function(event) {
    $("#cad-erro").hide("slow");
    $("#div-cad-exame").show("slow");
});
$("#btn-cad-novo").click(function(event) {
    $('#frm_cad').get(0).reset()
    $("#cad-ok").hide("slow");
    $("#div-cad-exame").show("slow");
});


//////////////////////////
//Modificando #outro formulário
//////////////////////////
/// AUTO COMPLETE com TYPEAHEAD
/////////////////////////////////
$(document).ready(function() {
    $("#modifica-exame").hide();
});
$('#busca-editar').typeahead({
    source: function(query, process) {
        var $url = endereco + 'exame/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof (this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#idexame').val(item.id);

        $.get(endereco + "exame/pega_exame/" + item.id)
                .done(function(data) {
                    $("#frm_edit #nome").val(data.nome);
                    $("#frm_edit #comissao").val(data.comissao);
                    $("#frm_edit #valor").val(data.valor);
                    $("#frm_edit #valor").maskMoney('mask');
                    $("#frm_edit #valor_convenio").val(data.valor_convenio);
                    $("#frm_edit #valor_convenio").maskMoney('mask');
                    //perfurmaria
                    //perfurmaria
                    $("#modifica-exame").fadeTo("slow", 0);
                    $("#modifica-exame").fadeTo("slow", 100);
                });

        $("#modifica-exame").show("slow");


        return item.name;
    }
});

//////// FRM EDIT , modificando exame existente
$(document).ready(function() {
    $("#frm_edit #valor").maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});
    $("#frm_edit #valor_convenio").maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});

    $("#frm_edit").submit(function(event) {
        event.preventDefault();

    });
    $('#frm_edit').validate(
            {
                onkeyup: false,
                onclick: false,
                submitHandler: function(form) {
                    var cadExame = $.ajax({
                        url: $(form).attr('action'),
                        type: $(form).attr('method'),
                        async: false,
                        data: $(form).serialize(),
                        dataType: "json",
                        beforeSend: function() {
                            $("#modifica-exame").toggle("slow");
                        }
                    });
                    cadExame.done(function(msg) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        if (msg) { //boolean
                            $("#ok_edit").show("slow");
                            oTable.fnReloadAjax(); //Atualiza o LISTAR
                        } else {
                            $("#erro_edit").show("slow");
                        }

                        //Oculta a mensagem de alerta depois de 3 segundos
                        $("#ok_edit").alert();
                        window.setTimeout(function() {
                            $("#ok_edit").alert('close');
                        }, 3000);
                    });
                    cadExame.fail(function(jqXHR, textStatus) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $("#cad-erro").show("slow");
                    });
                }
            });

});
$("#btn_tentar_edit").click(function(event) {
    $("#erro_edit").hide("slow");
});
$("#btn-edit").click(function(event) {
    $("#ok_edit").hide("slow");
});

$("#frm_edit #celular").change(function(event) {
    $.ajax({
        url: endereco + "util/pega_operadora",
        type: "post",
        data: $(this).serialize(),
        dataType: 'json',
        beforeSend: function(x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json;charset=UTF-8");
            }
        },
        error: function(request, error) {
            $('#output').html(error);
        },
        success: function(data) {
            if (data.success == true) {
                $("#frm_edit  #operadora").val(data.operadora);
            }
            else {
                $('#output').html(data.success);
            }

        }
    });
});

////////////////////////////
/// EXCLUINDO
///////////////////////////
$(document).ready(function() {
    $("#exclui_exame").hide();
    $("#frm_delete #valor").maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});
    $("#frm_delete #valor_convenio").maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});
});
$('#busca_excluir').typeahead({
    source: function(query, process) {
        var $url = endereco + 'exame/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof (this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#id_exame').val(item.id);

        $.get(endereco + "exame/pega_exame/" + item.id)
                .done(function(data) {
                    $("#frm_delete #nome").val(data.nome);
                    $("#frm_delete #comissao").val(data.comissao);
                    $("#frm_delete #valor").val(data.valor);
                    $("#frm_delete #valor").maskMoney('mask');
                    $("#frm_delete #valor_convenio").val(data.valor_convenio);
                    $("#frm_delete #valor_convenio").maskMoney('mask');
                    //perfurmaria
                    $("#exclui_exame").fadeTo("slow", 0);
                    $("#exclui_exame").fadeTo("slow", 100);
                });

        $("#exclui_exame").show("slow");


        return item.name;
    }
});

///BTN-DELETE
$('#btn_excluir').click(function() {

    var id = $("#frm_delete #id_exame").val();
    $.ajax({
        url: endereco + "exame/apaga/" + id,
        success: function() {
            $("html, body").animate({scrollTop: 0}, "slow");
            $('#confirma').modal('toggle');
            $("#exclui_exame").hide("slow");
            $("#busca_excluir").val("");
            oTable.fnReloadAjax(); //Atualiza o LISTAR
        },
        error: function() {
            console.log("AJAX request was a failure");
        }
    });

});