
$('#busca_exame').typeahead({
    source: function(query, process) {
        var url = endereco + 'exame/auto_complete/' + query;
        var items = new Array;
        items = [""];
        $.ajax({
            url: url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        documento: data.document,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof(this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    items.push(group);
                });

                process(items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);

        var cart = new Object();
        //Essa coleta de dados é necessária para gravar na sessão da venda
        var pegando = $.fn.pegando = $.get(endereco + "exame/pega_exame/" + item.id, function() {
            console.log("pegou exame");
        })
        pegando.done(function(data) {


            cart.id_exame = data.id;
            cart.exame_nome = data.nome;
            cart.valor = data.valor;
            cart.valorf = data.valorf;
            cart.id_paciente = $("#id_paciente").val();

            //Aqui registra na sessão da venda
            var registra = $.ajax({
                type: "POST",
                url: endereco + "atender/registra_exame/",
                data: cart
            })
            registra.done(function(msg) {
                //Nest momento preenche a tela com os itens da sessão
                var pega = $.get(endereco + "atender/pega_exames_sessao/", function() {
                    console.log("pegou na sessao exame");
                })

                pega.done(function(data) {

                    //Gera a tela que exibe os exames
                    $("#exames").fadeOut(500);
                    $("#exame_dados > tbody").html("");

                    var itemCountE = 0;

                    var vend = new Object();
//                    alert(data.carrinho.total);
                    //O loop abaixo cria as linhas da tabela
                    for (var k in data) {
                        if (data[k].id_exame) {
                            var newLine = '<tr id="e' + itemCountE + '" class="e' + data[k].id_exame + '" idbd="' + data[k].id_exame + '" valor="' + data[k].exame_valor + '"><td>' + data[k].id_exame + '</td><td>' + data[k].exame_nome + '</td><td>' + data[k].valorf + '</td><td><a>#</a></td></tr>';
                            $('#exame_dados tbody').append(newLine);
                        }

                        $("#e" + itemCountE).click(function() {
                            var id_exame = $(this).attr("idbd");
                            var contador = $(this).attr("id");
                            var id = $(this).attr("class");
                            var valor = $(this).attr("valor");

                            //Aqui REMOVE na sessão da venda
                            vend.id_exame = id_exame;
                            vend.itemCount = contador;
                            var removendo = $.ajax({
                                type: "POST",
                                url: endereco + "atender/remove_exame/",
                                data: vend,
                                global: false,
                                success: function(data) {
                                    result = data;
                                    console.log("removido com sucesso");
                                    $("." + id).remove();
                                    $("#subtotal").val(data.total);
                                }
                            }).responseText;

                        });
                        itemCountE++;

                    }
                    $("#subtotal").val(data.carrinho.total);
                    $("#subtotal").maskMoney('mask');
                    $("#exames").fadeIn(1000);
                });
            });

        });
        return item.name;
    }
});

$(document).ready(function() {
    $("#subtotal").maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});
});
