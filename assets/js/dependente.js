//Caminho do sistema, var endereco.
document.write('<script type="text/javascript" src="../assets/js/clinigon.js"></script>');
/*
 *  Controle da tela do dependente
 */

////FRM CAD 
$(document).ready(function() {
    $('#frm_cad #valor').each(function() {
        $(this).maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});
    });
    $("#frm_cad").submit(function(event) {
        event.preventDefault();
    });
    $('#frm_cad').validate(
            {
                onkeyup: false,
                onclick: false,
                messages: {
                    documento: {
                        remote: jQuery.validator.format("{0} já em uso por outro.")
                    },
                    email_dependente: {
                        remote: jQuery.validator.format("{0} já em uso por outro.")
                    }
                },
                submitHandler: function(form) {
                    var cadAssociado = $.ajax({
                        url: $(form).attr('action'),
                        type: $(form).attr('method'),
                        async: false,
                        data: $(form).serialize(),
                        dataType: "json",
                        beforeSend: function() {
                            $("#div-cad-dependente").hide("slow");
                        }
                    });
                    cadAssociado.done(function(msg) {
                        if (msg) { //boolean
                            $("html, body").animate({scrollTop: 0}, "slow");
                            $("#cad-ok").show("slow");
                            oTable.fnReloadAjax(); //Atualiza o LISTAR
                        } else {
                            $("html, body").animate({scrollTop: 0}, "slow");
                            $("#cad-erro").show("slow");
                        }
                    });
                    cadAssociado.fail(function(jqXHR, textStatus) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $("#cad-erro").show("slow");
                    });
                }
            });
});
$("#btn-tentar").click(function(event) {
    $("#cad-erro").hide("slow");
    $("#div-cad-dependente").show("slow");
});
$("#btn-cad-novo").click(function(event) {
    $('#frm_cad').get(0).reset()
    $("#cad-ok").hide("slow");
    $("#div-cad-dependente").show("slow");
});

//Buscando o paciente que este dependente irá conectar
$(document).ready(function() {
    $("#paciente_cad").hide();
    $("#cadastro_frm").hide();
});
$('#busca-paciente').typeahead({
    source: function(query, process) {
        var url = endereco + 'paciente/auto_complete/' + query;
        var items = new Array;
        items = [""];
        $.ajax({
            url: url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        documento: data.document,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof(this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    items.push(group);
                });

                process(items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#id_paciente_FK').val(item.id);

        $.get(endereco + "paciente/pega_paciente/" + item.id)
                .done(function(data) {
            $("#nome_paciente_cad").text(data.nome);
            $("#documento_cad").text(data.documento);
            $("#cidade_cad").text(data.cidade + '/' + data.uf);
            $("#celular_cad").text(data.celular + ' - ' + data.operadora);
        });

        $("#paciente_cad").show();
        $("#cadastro_frm").show("slow");


        return item.name;
    }
});




//////////////////////////
//Modificando #outro formulário
//////////////////////////
/// AUTO COMPLETE com TYPEAHEAD
/////////////////////////////////
$(document).ready(function() {
    $("#modifica-dependente").hide();
    $("#paciente_edit").hide();
});
$('#busca-editar').typeahead({
    source: function(query, process) {
        var $url = endereco + 'dependente/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        id_paciente_FK: data.id_paciente_FK,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof(this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#iddependente').val(item.id);

        var id_paciente_FK = item.id_paciente_FK;

        $.get(endereco + "dependente/pega_dependente/" + item.id)
                .done(function(data) {
            $("#frm_edit #nome").val(data.nome);
            $("#frm_edit #sexo").val(data.sexo);
            $("#frm_edit #nascimento").val(data.nascimento);
            //perfurmaria
            $("#modifica-dependente").fadeTo("slow", 0);
            $("#modifica-dependente").fadeTo("slow", 100);

            $("#modifica-dependente").show("slow");
        });

        $.get(endereco + "paciente/pega_paciente/" + id_paciente_FK)
                .done(function(data) {
            $("#nome_paciente_edit").text(data.nome);
            $("#documento_edit").text(data.documento);
            $("#cidade_edit").text(data.cidade + '/' + data.uf);
            $("#celular_edit").text(data.celular + ' - ' + data.operadora);

            $("#paciente_edit").show();
        });

        return item.name;
    }
});

//////// FRM EDIT , modificando dependente existente
$(document).ready(function() {
    $("#frm_edit #valor").maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});

    $("#frm_edit").submit(function(event) {
        event.preventDefault();

    });
    $('#frm_edit').validate(
            {
                onkeyup: false,
                onclick: false,
                submitHandler: function(form) {
                    var cadAssociado = $.ajax({
                        url: $(form).attr('action'),
                        type: $(form).attr('method'),
                        async: false,
                        data: $(form).serialize(),
                        dataType: "json",
                        beforeSend: function() {
                            $("#modifica-dependente").toggle("slow");
                        }
                    });
                    cadAssociado.done(function(msg) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        if (msg) { //boolean
                            $("#ok_edit").show("slow");
                            oTable.fnReloadAjax(); //Atualiza o LISTAR
                        } else {
                            $("#erro_edit").show("slow");
                        }

                        //Oculta a mensagem de alerta depois de 3 segundos
                        $("#ok_edit").alert();
                        window.setTimeout(function() {
                            $("#ok_edit").alert('close');
                        }, 3000);
                    });
                    cadAssociado.fail(function(jqXHR, textStatus) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $("#cad-erro").show("slow");
                    });
                }
            });

});
$("#btn_tentar_edit").click(function(event) {
    $("#erro_edit").hide("slow");
});
$("#btn-edit").click(function(event) {
    $("#ok_edit").hide("slow");
});



////////////////////////////
/// EXCLUINDO
///////////////////////////
$(document).ready(function() {
    $("#exclui_dependente").hide();
    $("#paciente_delete").hide();
});
$('#busca_excluir').typeahead({
    source: function(query, process) {
        var $url = endereco + 'dependente/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        id_paciente_FK: data.id_paciente_FK,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof(this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#id_dependente').val(item.id);

        var id_paciente_FK = item.id_paciente_FK;
        $.get(endereco + "dependente/pega_dependente/" + item.id)
                .done(function(data) {
            $("#frm_delete #nome").val(data.nome);
            $("#frm_delete #nascimento").val(data.nascimento);
            $("#frm_delete #sexo").val(data.sexo);
            //perfurmaria
            $("#exclui_dependente").fadeTo("slow", 0);
            $("#exclui_dependente").fadeTo("slow", 100);

            $("#exclui_dependente").show("slow");
        });

        $.get(endereco + "paciente/pega_paciente/" + id_paciente_FK)
                .done(function(data) {
            $("#nome_paciente_delete").text(data.nome);
            $("#documento_delete").text(data.documento);
            $("#cidade_delete").text(data.cidade + '/' + data.uf);
            $("#celular_delete").text(data.celular + ' - ' + data.operadora);

            $("#paciente_delete").show();
        });





        return item.name;
    }
});

///BTN-DELETE
$('#btn_excluir').click(function() {

    var id = $("#frm_delete #id_dependente").val();
    $.ajax({
        url: endereco + "dependente/apaga/" + id,
        success: function() {
            $("html, body").animate({scrollTop: 0}, "slow");
            $('#confirma').modal('toggle');
            $("#exclui_dependente").hide("slow");
            $("#paciente_delete").hide();
            $("#busca_excluir").val("");
            oTable.fnReloadAjax(); //Atualiza o LISTAR
        },
        error: function() {
            console.log("AJAX request was a failure");
        }
    });

});