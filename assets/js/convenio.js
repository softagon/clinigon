//Caminho do sistema, var endereco.
document.write('<script type="text/javascript" src="../assets/js/clinigon.js"></script>');
/*
 *  Controle da tela do convenio
 */

////FRM CAD 
$(document).ready(function() {
    $('#frm_cad #valor').each(function() {
        $(this).maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});
    });
    $("#frm_cad").submit(function(event) {
        event.preventDefault();
    });
    $('#frm_cad').validate(
            {
                onkeyup: false,
                onclick: false,
                rules: {
                    cnpj: {
                        required: true,
                        remote: endereco + "convenio/cnpj_valido"
                    }
                },
                messages: {
                    cnpj: {
                        remote: jQuery.validator.format("{0} é inválido.")
                    }
                },
                submitHandler: function(form) {
                    var cadAssociado = $.ajax({
                        url: $(form).attr('action'),
                        type: $(form).attr('method'),
                        async: false,
                        data: $(form).serialize(),
                        dataType: "json",
                        beforeSend: function() {
                            $("#div-cad-convenio").hide("slow");
                        }
                    });
                    cadAssociado.done(function(msg) {
                        if (msg) { //boolean
                            $("html, body").animate({scrollTop: 0}, "slow");
                            $("#cad-ok").show("slow");
                            oTable.fnReloadAjax(); //Atualiza o LISTAR
                        } else {
                            $("html, body").animate({scrollTop: 0}, "slow");
                            $("#cad-erro").show("slow");
                        }
                    });
                    cadAssociado.fail(function(jqXHR, textStatus) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $("#cad-erro").show("slow");
                    });
                }
            });
});
$("#btn-tentar").click(function(event) {
    $("#cad-erro").hide("slow");
    $("#div-cad-convenio").show("slow");
});
$("#btn-cad-novo").click(function(event) {
    $('#frm_cad').get(0).reset()
    $("#cad-ok").hide("slow");
    $("#div-cad-convenio").show("slow");
});


$("#frm_cad #celular").change(function(event) {
    $.ajax({
        url: endereco + "util/pega_operadora",
        type: "post",
        data: $(this).serialize(),
        dataType: 'json',
        beforeSend: function(x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json;charset=UTF-8");
            }
        },
        error: function(request, error) {
            $('#output').html(error);
        },
        success: function(data) {
            if (data.success == true) {
                $("#frm_cad #operadora").val(data.operadora);
            }
            else {
                $('#output').html(data.success);
            }

        }
    });
});

//////////////////////////
//Modificando #outro formulário
//////////////////////////
/// AUTO COMPLETE com TYPEAHEAD
/////////////////////////////////
$(document).ready(function() {
    $("#modifica-convenio").hide();
});
$('#busca-editar').typeahead({
    source: function(query, process) {
        var $url = endereco + 'convenio/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof(this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#idconvenio').val(item.id);

        $.get(endereco + "convenio/pega_convenio/" + item.id)
                .done(function(data) {
            $("#frm_edit #nome").val(data.nome);
            $("#frm_edit #cnpj").val(data.cnpj);
            $("#frm_edit #responsavel").val(data.responsavel);
            $("#frm_edit #telefone").val(data.telefone);
            $("#frm_edit #cidade").val(data.cidade);
            $("#frm_edit #uf").val(data.uf);
            //perfurmaria
            $("#modifica-convenio").fadeTo("slow", 0);
            $("#modifica-convenio").fadeTo("slow", 100);
        });

        $("#modifica-convenio").show("slow");


        return item.name;
    }
});

//////// FRM EDIT , modificando convenio existente
$(document).ready(function() {
    $("#frm_edit #valor").maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});

    $("#frm_edit").submit(function(event) {
        event.preventDefault();

    });
    $('#frm_edit').validate(
            {
                onkeyup: false,
                onclick: false,
                rules: {
                    cnpj: {
                        required: true,
                        remote: endereco + "convenio/cnpj_valido"
                    }
                },
                submitHandler: function(form) {
                    var cadAssociado = $.ajax({
                        url: $(form).attr('action'),
                        type: $(form).attr('method'),
                        async: false,
                        data: $(form).serialize(),
                        dataType: "json",
                        beforeSend: function() {
                            $("#modifica-convenio").toggle("slow");
                        }
                    });
                    cadAssociado.done(function(msg) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        if (msg) { //boolean
                            $("#ok_edit").show("slow");
                            oTable.fnReloadAjax(); //Atualiza o LISTAR
                        } else {
                            $("#erro_edit").show("slow");
                        }

                        //Oculta a mensagem de alerta depois de 3 segundos
                        $("#ok_edit").alert();
                        window.setTimeout(function() {
                            $("#ok_edit").alert('close');
                        }, 3000);
                    });
                    cadAssociado.fail(function(jqXHR, textStatus) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $("#cad-erro").show("slow");
                    });
                }
            });

});
$("#btn_tentar_edit").click(function(event) {
    $("#erro_edit").hide("slow");
});
$("#btn-edit").click(function(event) {
    $("#ok_edit").hide("slow");
});

$("#frm_edit #celular").change(function(event) {
    $.ajax({
        url: endereco + "util/pega_operadora",
        type: "post",
        data: $(this).serialize(),
        dataType: 'json',
        beforeSend: function(x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json;charset=UTF-8");
            }
        },
        error: function(request, error) {
            $('#output').html(error);
        },
        success: function(data) {
            if (data.success == true) {
                $("#frm_edit  #operadora").val(data.operadora);
            }
            else {
                $('#output').html(data.success);
            }

        }
    });
});

////////////////////////////
/// EXCLUINDO
///////////////////////////
$(document).ready(function() {
    $("#exclui_convenio").hide();
    $("#frm_delete #valor").maskMoney({symbol: 'R$ ', thousands: '.', decimal: ',', symbolStay: true});
});
$('#busca_excluir').typeahead({
    source: function(query, process) {
        var $url = endereco + 'convenio/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof(this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#id_convenio').val(item.id);

        $.get(endereco + "convenio/pega_convenio/" + item.id)
                .done(function(data) {
            $("#frm_delete #nome").val(data.nome);
            $("#frm_delete #cnpj").val(data.cnpj);
            $("#frm_delete #responsavel").val(data.responsavel);
            $("#frm_delete #telefone").val(data.telefone);
            $("#frm_delete #cidade").val(data.cidade);
            $("#frm_delete #uf").val(data.uf);
            //perfurmaria
            $("#exclui_convenio").fadeTo("slow", 0);
            $("#exclui_convenio").fadeTo("slow", 100);
        });

        $("#exclui_convenio").show("slow");


        return item.name;
    }
});

///BTN-DELETE
$('#btn_excluir').click(function() {

    var id = $("#frm_delete #id_convenio").val();
    $.ajax({
        url: endereco + "convenio/apaga/" + id,
        success: function() {
            $("html, body").animate({scrollTop: 0}, "slow");
            $('#confirma').modal('toggle');
            $("#exclui_convenio").hide("slow");
            $("#busca_excluir").val("");
            oTable.fnReloadAjax(); //Atualiza o LISTAR
        },
        error: function() {
            console.log("AJAX request was a failure");
        }
    });

});