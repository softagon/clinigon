/*
 * Neste arquivo iremos realizar os seguintes passos:
 * 1 - Encontrar o Paciente
 * 2 - Checar se é o paciente ou um menor de idade, dependente
 * 3 - Selecionar o associado(médico) que atenderá
 * 4 - A forma de pagamento
 * 5 - Registrar a forma de pagamento
 */
//Caminho do sistema, var endereco.
document.write('<script type="text/javascript" src="../assets/js/clinigon.js"></script>');


/*
 * 
 * Controle visual ao clicar nos botoes
 */
$(document).ready(function() {
    $("#btn_convenio_escolha").attr("disabled", false);
    $("#btn_medico_escolha").attr("disabled", false);
    $("#btn_clinica_escolha").attr("disabled", false);
});

$("#btn_paciente").click(function() {
    $("#caixa-busca-paciente").hide("slow");
    $("#confirma-paciente").fadeOut("slow");
    $("#exames").show("slow");
});

$("#btn_exame").click(function() {
    $("#caixa-busca-exame").hide("slow");
    $("#confirma-exame").fadeOut("slow");
    $("#tipo_atendimento").show("slow");
});


//Aqui decide se é convênio, médico ou clínica, escolha.
$("#btn_convenio_escolha").click(function() {
    $("#tipo_consulta").val("convenio");
    $("#btn_medico_escolha").hide("slow");
    $("#btn_clinica_escolha").hide("slow");
    $("#btn_convenio_escolha").attr("disabled", true);
    $("#convenio").show("slow");
});
$("#btn_medico_escolha").click(function() {
    $("#tipo_consulta").val("particular");
    $("#btn_convenio_escolha").hide("slow");
    $("#btn_clinica_escolha").hide("slow");
    $("#btn_medico_escolha").attr("disabled", true);
    $("#associado").show("slow");
});

$("#btn_clinica_escolha").click(function() {
    $("#tipo_consulta").val("clinica");
    $("#btn_convenio_escolha").hide("slow");
    $("#btn_medico_escolha").hide("slow");
    $("#btn_clinica_escolha").attr("disabled", true);
    $("#forma_pagamento").show("slow");

});

//Escolheu convênio
$("#btn_convenio").click(function() {
    $("#forma_pagamento").show("slow");
    $("#confirma_exame").show("slow");
    
});
//Escolheu associado
$("#btn_associado").click(function() {
    $("#caixa-busca-associado").hide("slow");
    $("#confirma-associado").fadeOut("slow");
    $("#forma_pagamento").show("slow");
});


//Caixa de buscas 
$('#busca-paciente').typeahead({
    source: function(query, process) {
        var $url = endereco + 'paciente/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof (this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#id_paciente').val(item.id);

        $.get(endereco + "paciente/pega_paciente/" + item.id)
                .done(function(data) {
                    $("#paciente_dados").show("slow");

                    if (data.operadora.length != 0)
                        data.operadora = ' - ' + data.operadora;

                    $("#paciente_nome").text(data.nome);
                    $("#documento_paciente").text(data.documento);
                    $("#cidade_uf_paciente").text(data.cidade + '/' + data.uf);
                    $("#celular_paciente").text(data.telefone + data.operadora);
                    $("#idade_paciente").text(data.idade + ' anos');
                    $("#nascimento_paciente").text(data.nascimento);
                });

        return item.name;
    }
});


$('#busca-associado').typeahead({
    source: function(query, process) {
        var $url = endereco + 'associado/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof (this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#id_associado').val(item.id);
        $.get(endereco + "associado/pega_associado/" + item.id)
                .done(function(data) {
                    $("#associado_dados").show("slow");
                    $("#associado_nome").text(data.nome);
                    $("#documento_associado").text(data.documento);
                    $("#especialidade").text(data.especialidade);

                    var tipo_consulta = $("#tipo_consulta").val();
                    if (tipo_consulta == "particular") {
                        $("#valor").text(data.valorf);
                    } else {
                        if (data.valor_conveniof == "R$ 0,00") {
                            alert("Atenção, esse Associado(Médico) não tem valor(R$) de convênio registrado.");
                            window.setTimeout('location.reload()', 1000); //para tudo. recarrega a página.
                        } else {
                            $("#valor").text(data.valor_conveniof);
                        }
                    }
                });

        return item.name;
    }
});

/*
 * Aqui começamos a parte do convênio, busca e registro
 */
$('#busca-convenio').typeahead({
    source: function(query, process) {
        var url = endereco + 'convenio/auto_complete/' + query;
        var items = new Array;
        items = [""];
        $.ajax({
            url: url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        documento: data.document,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof (this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    items.push(group);
                });

                process(items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);

        var cart = new Object();
        //Essa coleta de dados é necessária para gravar na sessão da venda
        var pegando = $.fn.pegando = $.get(endereco + "convenio/pega_convenio/" + item.id, function() {
            console.log("pegou convenio");
        })
        pegando.done(function(data) {
            $('#remove_espaco').remove(); //organizando
            $("#convenio_dados").show("slow");
            $("#convenio_nome").text(data.nome);
            $("#convenio_telefone").text(data.telefone);
            $("#convenio_responsavel").text(data.responsavel);
            $("#convenio_cidade_uf").text(data.cidade + '/' + data.uf);

            cart.id_convenio = data.id;
            cart.convenio_nome = data.nome;
            cart.convenio_telefone = data.telefone;
            cart.convenio_responsavel = data.convenio_responsavel;
            $("#id_convenio").val(data.id);

        });
        return item.name;
    }
});

$('#busca-exame').typeahead({
    source: function(query, process) {
        var $url = endereco + 'exame/auto_complete/' + query;
        var $items = new Array;
        $items = [""];
        $.ajax({
            url: $url,
            dataType: "json",
            type: "POST",
            success: function(data) {
                $.map(data, function(data) {
                    var group;
                    group = {
                        id: data.id,
                        name: data.name,
                        toString: function() {
                            return JSON.stringify(this);
                            //return this.app;
                        },
                        toLowerCase: function() {
                            return this.name.toLowerCase();
                        },
                        indexOf: function(string) {
                            return String.prototype.indexOf.apply(this.name, arguments);
                        },
                        replace: function(string) {
                            var value = '';
                            value += this.name;
                            if (typeof (this.level) != 'undefined') {
                                value += ' <span class="pull-right muted">';
                                value += this.level;
                                value += '</span>';
                            }
                            return String.prototype.replace.apply(value, arguments);
                        }
                    };
                    $items.push(group);
                });

                process($items);
            }
        });
    },
    property: 'name',
    items: 10,
    minLength: 2,
    updater: function(item) {
        var item = JSON.parse(item);
        $('#id_exame').val(item.id);
        $.get(endereco + "exame/pega_exame/" + item.id)
                .done(function(data) {
                    
                    if(data.valorf_convenio == "R$ 0,00"){
                        data.valorf_convenio = "Não disponível";
                    }
                    if(data.comissao == null){
                        data.comissao = "Não disponível";
                    }
                    $("#exame_dados").show("slow");
                    $("#nome_exame").text(data.nome);
                    $("#valor_convenio_exame").text(data.valorf_convenio);
                    $("#valor_exame").text(data.valorf);
                    $("#comissao_exame").text(data.comissao);
                });

        return item.name;
    }
});