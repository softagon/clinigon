<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Suporte_library {

    function softagon_ajuda() {
        return "<a href='#' title='Suporte Softagon' target='_blank'>Pedir ajuda a SOFTAGON?</a>";
    }

    function cad_ok() {
        return "<strong>Sucesso</strong> Efetuado corretamente. ";
    }
    
    function cad_erro() {
        return "<strong>Erro:</strong> Houve um erro ao tentar executar. ". $this->softagon_ajuda();
    }

}

/* End of file Someclass.php */