<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package	CodeIgniter
 * @author	Hermes Alves
 * @copyright	Copyright (c) 2013, codiLabs, Inc.
 * @email	eu@dyb.fm
 */
// ------------------------------------------------------------------------

/**
 * Access Class
 *
 * @package	CodeIgniter
 * @subpackage	Libraries
 * @category	Access Control Lists
 * @author	codiLabs Dev Team
 */
class Acesso {

    protected $ignored = false;
    // to get controller name
    protected $uri_segment = 1;
    // login path on your application
    protected $login_path = 'index';
    // session name for initializing user is logged (true/false)
    protected $session_logged_name = 'logged_in';
    // session name for get array access
    protected $session_access_name = 'access';
    // name of menu table
    protected $table_menu = 'menu';
    // fieldname of path menu on your menu table | ex. article or blog/read
    protected $segment_field = 'menu_segment';

    /**
     * logged user allowed to access controller and user inherits from the role
     * ex. array('user'=> array('logout','notification'),'customer'=>array('view'));
     * allowed to access user/logout, user/notification and customer/view
     */
    protected $ignored_access = array();
    //name of query string to bring you referrer role before showing login page
    protected $redirect_query_string = 'redirect';

    // --------------------------------------------------------------------

    /**
     * Constructor
     *
     * @access	public
     * @param	array	initialization parameters
     */
    public function __construct($params = array()) {

        $CI = & get_instance();
        $this->ci = & get_instance();
        $CI->load->model('Acesso_model');
        $this->acesso_model = $CI->Acesso_model;

        $this->session = $CI->session;
        $this->db = $CI->db;
        //get controller segment from uri
        $this->controller_segment = $CI->uri->segment($this->uri_segment);
        $this->function_segment = $CI->uri->segment($this->uri_segment + 1);

        log_message('debug', "Access Class Initialized");
    }

    /*
     * Redirect na opção 1 direciona
     */

    public function login($data, $redirect = 0) {

        if ($this->acesso_model->senha_bate($data)) {
            $dados = $this->acesso_model->get_id($data);

            if (!empty($dados)) {
                $this->session->set_userdata($dados);

                if ($redirect == 1)
                    redirect('/principal/inicio/', 'refresh');
            }
            else {
                $data['titulo'] = "Problema em registrar a sessão.";
                $data['mensagem'] = "Houve um problema do lado servidor, que falar com a SOFTAGON ou quer <a href='" . URL . "'>tentar novamente?</a>";
                $this->ci->load->view("error_msg", $data);
            }
        } else {
            $data['titulo'] = "A senha não confere, dados incorretos.";
            $data['mensagem'] = "A senha não bateu, que tal <a href='" . URL . "'>tentar novamente?</a>";
            $this->ci->load->view("error_msg", $data);
        }
    }

    public function is_logged() {
        $id_usuario_FK = $this->session->userdata('id_usuario');
        if (!empty($id_usuario_FK))
            return true;
        else
            return false;
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('/principal/', 'refresh');
    }

    public function get_level() {
        return $this->session->userdata('id_grupo');
    }

    public function get_id() {
        return $this->session->userdata('id_usuario');
    }

}

// END Permission Class

/* End of file Acesso.php */
/* Location: ./application/libraries/Acesso.php */