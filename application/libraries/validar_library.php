<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Validar_library {
    /**
     * isCnpjValid
     *
     * Esta função testa se um Cnpj é valido ou não. 
     *
     * @author	Raoni Botelho Sporteman <raonibs@gmail.com>
     * @version	1.0 Debugada em 27/09/2011 no PHP 5.3.8
     * @param	string		$cnpj			Guarda o Cnpj como ele foi digitado pelo cliente
     * @param	array		$num			Guarda apenas os números do Cnpj
     * @param	boolean		$isCnpjValid	Guarda o retorno da função
     * @param	int			$multiplica 	Auxilia no Calculo dos Dígitos verificadores
     * @param	int			$soma			Auxilia no Calculo dos Dígitos verificadores
     * @param	int			$resto			Auxilia no Calculo dos Dígitos verificadores
     * @param	int			$dg				Dígito verificador
     * @return	boolean						"true" se o Cnpj é válido ou "false" caso o contrário
     *
     */

    /**
     * Verifica se é um número de CNPJ válido.
     * @author Gabriel Heming <gabriel_heming@hotmail.com>
     * @param $cnpj O número a ser verificado
     * @return boolean
     */
    public function validar_cnpj($cnpj) {

        $cnpj = preg_replace('/\D/', '', $cnpj);

        if (strlen($cnpj) != 14) {
            return false;
        }

        if (preg_match('/^(\d{1})\1{13}$/', $cnpj)) {
            return false;
        }

        $soma = 0;
        for ($i = 0; $i < 12; $i++) {

            /** verifica qual é o multiplicador. Caso o valor do caracter seja entre 0-3, diminui o valor do caracter por 5
             * caso for entre 4-11, diminui por 13 * */
            $multiplicador = ( $i <= 3 ? 5 : 13 ) - $i;

            $soma += $cnpj{$i} * $multiplicador;
        }
        $soma = $soma % 11;


        if ($soma == 0 || $soma == 1) {
            $digitoUm = 0;
        } else {
            $digitoUm = 11 - $soma;
        }

        if ((int) $digitoUm == (int) $cnpj{12}) {

            $soma = 0;

            for ($i = 0; $i < 13; $i++) {

                /** verifica qual é o multiplicador. Caso o valor do caracter seja entre 0-4, diminui o valor do caracter por 6
                 * caso for entre 4-12, diminui por 14 * */
                $multiplicador = ( $i <= 4 ? 6 : 14 ) - $i;
                $soma += $cnpj{$i} * $multiplicador;
            }
            $soma = $soma % 11;
            if ($soma == 0 || $soma == 1) {
                $digitoDois = 0;
            } else {
                $digitoDois = 11 - $soma;
            }
            if ($digitoDois == $cnpj{13}) {
                return true;
            }
        }
        return false;
    }

    function validCPF($cpf) {
        // determina um valor inicial para o digito $d1 e $d2
        // pra manter o respeito ;)
        $d1 = 0;
        $d2 = 0;
        // remove tudo que não seja número
        $cpf = preg_replace("/[^0-9]/", "", $cpf);
        // lista de cpf inválidos que serão ignorados
        $ignore_list = array(
            '00000000000',
            '01234567890',
            '11111111111',
            '22222222222',
            '33333333333',
            '44444444444',
            '55555555555',
            '66666666666',
            '77777777777',
            '88888888888',
            '99999999999'
        );
        // se o tamanho da string for dirente de 11 ou estiver
        // na lista de cpf ignorados já retorna false
        if (strlen($cpf) != 11 || in_array($cpf, $ignore_list)) {
            return false;
        } else {
            // inicia o processo para achar o primeiro
            // número verificador usando os primeiros 9 dígitos
            for ($i = 0; $i < 9; $i++) {
                // inicialmente $d1 vale zero e é somando.
                // O loop passa por todos os 9 dígitos iniciais
                $d1 += $cpf[$i] * (10 - $i);
            }
            // acha o resto da divisão da soma acima por 11
            $r1 = $d1 % 11;
            // se $r1 maior que 1 retorna 11 menos $r1 se não
            // retona o valor zero para $d1
            $d1 = ($r1 > 1) ? (11 - $r1) : 0;
            // inicia o processo para achar o segundo
            // número verificador usando os primeiros 9 dígitos
            for ($i = 0; $i < 9; $i++) {
                // inicialmente $d2 vale zero e é somando.
                // O loop passa por todos os 9 dígitos iniciais
                $d2 += $cpf[$i] * (11 - $i);
            }
            // $r2 será o resto da soma do cpf mais $d1 vezes 2
            // dividido por 11
            $r2 = ($d2 + ($d1 * 2)) % 11;
            // se $r2 mair que 1 retorna 11 menos $r2 se não
            // retorna o valor zeroa para $d2
            $d2 = ($r2 > 1) ? (11 - $r2) : 0;
            // retona true se os dois últimos dígitos do cpf
            // forem igual a concatenação de $d1 e $d2 e se não
            // deve retornar false.
            return (substr($cpf, -2) == $d1 . $d2) ? true : false;
        }
    }

    function validaCNS($cns) {
        if ((strlen(trim($cns))) != 15) {
            return false;
        }
        $pis = substr($cns, 0, 11);
        $soma = (((substr($pis, 0, 1)) * 15) +
                ((substr($pis, 1, 1)) * 14) +
                ((substr($pis, 2, 1)) * 13) +
                ((substr($pis, 3, 1)) * 12) +
                ((substr($pis, 4, 1)) * 11) +
                ((substr($pis, 5, 1)) * 10) +
                ((substr($pis, 6, 1)) * 9) +
                ((substr($pis, 7, 1)) * 8) +
                ((substr($pis, 8, 1)) * 7) +
                ((substr($pis, 9, 1)) * 6) +
                ((substr($pis, 10, 1)) * 5));
        $resto = fmod($soma, 11);
        $dv = 11 - $resto;
        if ($dv == 11) {
            $dv = 0;
        }
        if ($dv == 10) {
            $soma = ((((substr($pis, 0, 1)) * 15) +
                    ((substr($pis, 1, 1)) * 14) +
                    ((substr($pis, 2, 1)) * 13) +
                    ((substr($pis, 3, 1)) * 12) +
                    ((substr($pis, 4, 1)) * 11) +
                    ((substr($pis, 5, 1)) * 10) +
                    ((substr($pis, 6, 1)) * 9) +
                    ((substr($pis, 7, 1)) * 8) +
                    ((substr($pis, 8, 1)) * 7) +
                    ((substr($pis, 9, 1)) * 6) +
                    ((substr($pis, 10, 1)) * 5)) + 2);
            $resto = fmod($soma, 11);
            $dv = 11 - $resto;
            $resultado = $pis . "001" . $dv;
        } else {
            $resultado = $pis . "000" . $dv;
        }
        if ($cns != $resultado) {
            if (!$this->validaCNS_PROVISORIO($cns))
                return false;
            else
                return true;
        } else {
            return true;
        }
    }

    function validaCNS_PROVISORIO($cns) {
        if ((strlen(trim($cns))) != 15) {
            return false;
        }
        $soma = (((substr($cns, 0, 1)) * 15) +
                ((substr($cns, 1, 1)) * 14) +
                ((substr($cns, 2, 1)) * 13) +
                ((substr($cns, 3, 1)) * 12) +
                ((substr($cns, 4, 1)) * 11) +
                ((substr($cns, 5, 1)) * 10) +
                ((substr($cns, 6, 1)) * 9) +
                ((substr($cns, 7, 1)) * 8) +
                ((substr($cns, 8, 1)) * 7) +
                ((substr($cns, 9, 1)) * 6) +
                ((substr($cns, 10, 1)) * 5) +
                ((substr($cns, 11, 1)) * 4) +
                ((substr($cns, 12, 1)) * 3) +
                ((substr($cns, 13, 1)) * 2) +
                ((substr($cns, 14, 1)) * 1));
        $resto = fmod($soma, 11);
        if ($resto != 0) {
            return false;
        } else {
            return true;
        }
    }

}

/* End of file Someclass.php */