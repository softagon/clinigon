<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Operadora_library {
    # http://consultanumero.telein.com.br/sistema/consulta_numero.php?numero=8796604452&chave=senhasite

    function descobre_operadora($celular) {
        $celular = $this->formata_celular($celular);
        $url = "http://consultanumero.telein.com.br/sistema/consulta_numero.php?numero=" . $celular . "&chave=senhasite";
        $handle = @fopen($url, "r");
        if ($handle) {
            while (!feof($handle)) {
                $buffer = fgets($handle, 4096);
            }
            fclose($handle);

            $a = explode("#", $buffer);
            return $this->retorna_nome($a[0]);
        }
    }

    function formata_celular($celular) {
        $a = explode("(", $celular);
        $a = explode(")", $a[1]);
        $b = explode("-", $a[1]);
        $c = explode("_", $b[1]);
        return $a[0] . $b[0] . $c[0];
    }

    function retorna_nome($operadora) {
        switch ($operadora) {
            case "41":
                return "Tim";
                break;
            case "12":
                return "CTBC";
                break;
            case "15":
                return "Vivo";
                break;
            case "21":
                return "Claro/Embratel";
                break;
            case "43":
                return "Sercomtel";
                break;
            case "77":
                return "Nextel";
                break;
            case "31":
                return "Oi";
                break;

            default:
                return null;
                break;
        }
    }

}

/* End of file Someclass.php */