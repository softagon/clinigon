<?php
$data['titulo'] = "Seja bem vindo ao CLINIGON";
$this->load->view('layout/header', $data);
?>

<link href="<?= CSS ?>tab-left.css" rel="stylesheet">

<div class="container">
    <div class="row">
        <div class="pull-right icone">
            <img src="<?= IMG ?>clinigon_icone.jpg" alt="associado"/>
        </div>

        <div class="page-header">
            <h1>
                <?= $data['titulo'] ?>
                <div>
                    <?php $ultima_visita = $this->session->userdata('ultima_visita'); ?>
                    <?php if (!empty($ultima_visita)) { ?>
                        <small>Olá <strong><?= $this->session->userdata('primeiro_nome'); ?></strong>, a última vez que esteve aqui foi em <?= $ultima_visita ?> </small>
                    <?php } ?>
                </div>
            </h1>
        </div>
        <div class="row row-offcanvas row-offcanvas-right">

            <div class="col-xs-12 col-sm-9">
                <p class="pull-right visible-xs">
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Toggle nav</button>
                </p>
                <div class="jumbotron">
                    <h1>CLINIGON</h1>
                    <p>Este é o início de um sistema de gestão de clínicas de saúde. Seu objetivo é ser acessível e de fácil operação.</p>
                </div>
                <div class="row">
                    <div class="col-6 col-sm-6 col-lg-4">
                        <h2>Associados</h2>
                        <p>Todos que vem trabalhar nesta clínica, vem para somar e por isso chamamos de associados. São normalmente as pessoas que alugam ou fazem parcerias com a estrutura da clínica. Médicos, Psiquiatras e Fisioterapeutas são apenas exemplos dos mais variados profissionais de saúde.</p>
                    </div><!--/span-->
                    <div class="col-6 col-sm-6 col-lg-4">
                        <h2>Usuários</h2>
                        <p>Seu e-mail é o login de acesso ao sistema, pois somos um sistema verdadeiramente web, acessado via internet e feito para a Internet, dando a mobilidade e acessibilidade desejada para Clínicas que precisam expandir rapidamente. </p>
                    </div><!--/span-->
                    <div class="col-6 col-sm-6 col-lg-4">
                        <h2>Pacientes</h2>
                        <p>Pacientes são clientes, e podem usar como documento de identificação CPF, Cartão do SUS ou RG. Um banco de dados controlado e acessível por celular, tablet e qualquer computador conectado a Internet pode ver os dados dos pacientes para rápido atendimento.</p>
                    </div><!--/span-->
                    <!--                    <div class="col-6 col-sm-6 col-lg-4">
                                            <h2>Heading</h2>
                                            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                                            <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
                                        </div>/span
                                        <div class="col-6 col-sm-6 col-lg-4">
                                            <h2>Heading</h2>
                                            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                                            <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
                                        </div>/span
                                        <div class="col-6 col-sm-6 col-lg-4">
                                            <h2>Heading</h2>
                                            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
                                            <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
                                        </div>/span-->
                </div><!--/row-->
            </div><!--/span-->

            <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
                <div class="list-group">
                    <a href="#" class="list-group-item active">Atualizações deste</a>

                    <?
                    foreach ($atualizacoes as $value) {
                        $data = data_brasil($value['quando']);
                        echo "<p class='list-group-item'>$data - ".$value['descricao']."</p>";
                    }
                    ?>
                </div>
            </div><!--/span-->
        </div><!--/row-->
    </div>
</div>
<?php $this->load->view('layout/footer'); ?>
