<?php
$data['titulo'] = "Registrar uma consulta";
$this->load->view('layout/header', $data);
?>

<link href="<?= CSS ?>tab-left.css" rel="stylesheet">

<div class="container">
    <div class="row">
        <div class="pull-right icone">
            <img src="<?= IMG ?>registrar_consulta_icone.jpg" alt="Registrar consulta"/>
        </div>

        <div class="page-header">
            <h1>
                <?= $data['titulo'] ?>
                <div>
                    <small>Você conectará o paciente a um associado da Clínica, gerando uma consulta.</small>
                </div>
            </h1>
        </div>
        <form  role="form" id="frm_reg_con" name="frm_reg_con" method="post" action="<?= URL ?>atender/reg_consulta">
            <div class="row">
                <div id="paciente">
                    <div class="alert alert-warning" style="display: none" id="paciente_alerta">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Atenção!</strong> Você tem que selecionar um paciente.
                    </div>


                    <fieldset>
                        <legend>Paciente</legend>
                        <div class="form-group" id="caixa-busca-paciente">
                            <label for="disabledTextInput">Encontre o paciente</label>
                            <input type="text" class="form-control input-lg"
                                   id="busca-paciente" placeholder="Paciente, documento"
                                   onclick="this.value = ''" required title="Digite o nome do paciente para continuar"
                                   autocomplete="off">
                        </div>
                        <div class="row" id="paciente_dados" style="display: none">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="paciente">Nome</label>
                                    <p class="text-muted" id="paciente_nome"></p>
                                    <input type="hidden" name="id_paciente_FK" id="id_paciente_FK" value="" />
                                </div>
                                <div class="form-group">
                                    <label for="paciente">Cidade/UF</label>
                                    <p class="text-muted" id="cidade_uf_paciente"></p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="paciente">Documento</label>
                                    <p class="text-muted" id="documento_paciente"></p>
                                </div>
                                <div class="form-group">
                                    <label for="paciente">Celular</label>
                                    <p class="text-muted" id="celular_paciente"></p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="paciente">Nascimento</label>
                                    <p class="text-muted" id="nascimento_paciente"></p>
                                </div>
                                <div class="form-group">
                                    <label for="paciente">Idade</label>
                                    <p class="text-muted" id="idade_paciente"></p>
                                </div>
                            </div>
                        </div>
                        <div  id="confirma-paciente" align="right">
                            <a id="btn_paciente" type="button" class="btn btn-primary">Confirma paciente?</a>
                        </div>
                    </fieldset>
                </div>

                <div  id="tipo_atendimento" style="display: none">
                    <fieldset>
                        <legend>Defina o tipo de consulta</legend>
                        <div class="col-xs-6">
                            <button type="button" id="btn_convenio_escolha" class="btn btn-warning btn-lg btn-block">Convênio</button>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" id="btn_particular_escolha" class="btn btn-primary btn-lg btn-block">Particular</button>
                        </div>
                    </fieldset>
                    <input type="hidden" name="tipo_consulta" id="tipo_consulta" value="" />
                </div>
                <div id="convenio" style="display: none">
                    <fieldset>
                        <legend>Convênio</legend>
                        <div class="form-group" id="caixa-busca-convenio">
                            <label for="caixa-busca-convenio">Encontre a Empresa convêniada</label>
                            <input type="text" class="form-control input-lg"
                                   id="busca-convenio" placeholder="Empresa convêniada"
                                   onclick="this.value = ''">
                        </div>
                        <input type="hidden" name="id_convenio_FK" id="id_convenio_FK" value="" />
                        <input type="hidden" name="tem_convenio" id="tem_convenio" value="" />
                        <div class="row" id="convenio_dados" style="display: none">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="convenio_nome">Nome</label>
                                    <p class="text-muted" id="convenio_nome"></p>

                                </div>
                                <div class="form-group">
                                    <label for="convenio_cidade_uf">Cidade/UF</label>
                                    <p class="text-muted" id="convenio_cidade_uf"></p>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="convenio_telefone">Telefone</label>
                                    <p class="text-muted" id="convenio_telefone"></p>
                                </div>
                                <div class="form-group">
                                    <label for="convenio_responsavel">Responsável/Contato</label>
                                    <p class="text-muted" id="convenio_responsavel"></p>
                                </div>
                            </div>
                        </div>
                        <div  id="confirma-convenio" align="right">
                            <a id="btn_convenio" type="button" class="btn btn-primary">Confirma Empresa?</a>
                        </div>

                    </fieldset>
                </div>
                <div id="associado" style="display: none">
                    <fieldset name="associado_point">
                        <legend>Associado (médico)</legend>
                        <div class="form-group" id="caixa-busca-associado">
                            <label for="disabledTextInput">Encontre o associado</label>
                            <input type="text" class="form-control input-lg"
                                   id="busca-associado" placeholder="Associado, documento"
                                   onclick="this.value = ''" autocomplete="off">
                        </div>
                        <div class="row" id="associado_dados" style="display: none">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="associado">Nome</label>
                                    <p class="text-muted" id="associado_nome"></p>
                                    <input type="hidden" name="id_associado_FK" id="id_associado_FK" value="" />
                                </div>
                                <div class="form-group">
                                    <label id="associado_valor_lbl">Valor</label>
                                    <p class="text-muted" id="valor"></p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="associado">Documento</label>
                                    <p class="text-muted" id="documento_associado"></p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="associado">Especialidade</label>
                                    <p class="text-muted" id="especialidade"></p>
                                </div>
                            </div>
                        </div>
                        <div  id="confirma-associado" align="right">
                            <a id="btn_associado" type="button" class="btn btn-primary">Confirma associado?</a>
                        </div>
                    </fieldset>
                </div>
                <div id="forma_pagamento" style="display: none">
                    <fieldset>
                        <legend>Registro do pagamento</legend>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="paciente">Total</label>
                                <input type="text" class="form-control" id="total" name="total" required readonly="" title="Escolheu o associado?" value="">
                                <input type="hidden" class="form-control" id="totalsf" name="totalsf" value="">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="paciente">Forma de pagamento</label>
                                <select  class="form-control" required
                                         title="Vai pagar como?" tabindex="7" id="id_forma_pagamento_FK" name="id_forma_pagamento_FK">
                                    <option></option>
                                    <?
                                    foreach ($formas as $value) {
                                        echo '<option value="' . $value['id_forma_pagamento'] . '">' . $value['nome'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="total_desconto">Desconto em R$</label>
                                <input type="text" class="form-control" id="total_desconto" name="total_desconto" value="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="valor_pago_agora">Valor pago agora</label>
                                <input type="text" class="form-control" required="" title="Quanto o cliente pagou?"  id="valor_pago_agora" name="valor_pago_agora" value="">
                            </div>
                        </div>
                        <div  id="finalizando_consulta" align="right">
                            <button  id="btn_finalizar" type="submit" class="btn btn-info">Finalizar e registrar essa consulta?</button>
                        </div>
                    </fieldset>
                </div>
                <div id="sem_pagamento" style="display: none">
                    <fieldset>
                        <legend>Registro do pagamento</legend>

                        <div class="col-md-8">
                            <p>Cliente convêniado não paga a Uniclinic, apenas a Empresa convêniada arca com os custos.</p>
                            <div  id="finalizando_consulta" align="right">
                                <button  id="btn_finalizar" type="submit" class="btn btn-info">Finalizar e registrar essa consulta?</button>
                            </div>

                        </div>
                    </fieldset>
                </div>
            </div>
        </form>
    </div>
</div>
<link href="<?= CSS ?>typeahead.js-bootstrap.css" rel="stylesheet">
<?
$data['include'] = array("inputmask.js", "bootstrap3-typeahead.min.js",
    "jquery.maskMoney.js", "registrar_consulta.js");
?>
<?php $this->load->view('layout/footer', $data); ?>



