<?php
$data['titulo'] = "Gestão de saída de caixa";
$this->load->view('layout/header', $data);
?>

<link href="<?= CSS ?>tab-left.css" rel="stylesheet">

<div class="container">
    <div class="row">
        <div class="pull-right icone">
            <img src="<?= IMG ?>saida_caixa_icone.jpg" alt="saida_caixa"/>
        </div>

        <div class="page-header">
            <h1>
                <?= $data['titulo'] ?>
                <div>
                    <small>São categorias das saídas financeiras.</small>
                </div>
            </h1>
        </div>
        <div class="tabbable tabs-left" id="tabs-693402">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#listar" data-toggle="tab">
                        <span class="glyphicon glyphicon-floppy-save"></span> Listar
                    </a>
                </li>
                <li>
                    <a href="#cadastrar" data-toggle="tab">
                        <span class="glyphicon glyphicon-floppy-open"></span> Cadastrar
                    </a>
                </li>
                <li>
                    <a href="#editar" data-toggle="tab">
                        <span class="glyphicon glyphicon-edit   "></span> Editar
                    </a>
                </li>
                <li>
                    <a href="#excluir" data-toggle="tab">
                        <span class="glyphicon glyphicon-floppy-remove"></span> Excluir
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="listar">
                    <p>
                        <?php if (is_array($saida_caixas)) { ?>
                        <div  style="padding-left: 160px; content: 1px; ">
                            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered"  id="DT-tabela">
                                <thead>
                                    <tr>
                                        <th width="30%">Nome</th>
                                        <th >Valor da Saída </th>
                                        <th >Centro Custo</th>
                                        <th >Cadastrado em</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="dataTables_empty">Carregando dados do servidor</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="spacer"></div>

                    <?php } else {
                        ?>
                        <h3>Informação</h3><br/>
                        <span class="alert alert-info">
                            Ainda não possui nenhum Centro de Custo cadastrado, que tal <a href="#cadastrar" onclick="abreTab('cadastrar', 19);" class="alert-link">cadastrar agora?</a>
                        </span>
                        <?php
                    }
                    ?>
                    </p>
                </div>
                <div class="tab-pane" id="cadastrar">
                    <form  role="form" id="frm_cad" name="frm_cad" method="post" action="<?= URL ?>saida_caixa/cadastra">
                        <fieldset>
                            <legend>Cadastre a saída do caixa</legend>
                            <div class="alerta">
                                <!-- Error no cad saida_caixa -->
                                <div class="alert alert-warning alert-dismissable" id="cad-erro" style="display:none;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?= $this->cad_erro ?>
                                    <div class="btn-alerta">
                                        <button type="button" class="btn btn-warning" id="btn-tentar">Tentar novamente</button>
                                    </div>
                                </div>
                                <!-- Deu tudo certo -->
                                <div class="alert alert-success alert-dismissable" id="cad-ok" style="display:none;" >
                                    <button type="button" class="close">×</button>
                                    <?= $this->cad_ok ?>
                                    <div class="btn-alerta">
                                        <button type="button" class="btn btn-success" id="btn-cad-novo">Cadastrar outro?</button>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="div-cad-saida_caixa">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nome">Nome</label>
                                        <input type="text"
                                               class="form-control" id="nome"
                                               name="nome" required
                                               placeholder="Será exposto em relatórios"
                                               title="Nome obrigatório"
                                               tabindex="1"
                                               maxlength="60">
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="valor">Valor da Saída</label>
                                        <input type="text"
                                               class="form-control" id="valor"
                                               name="valor" required
                                               placeholder="Será exposto em relatórios"
                                               title="Valor obrigatório"
                                               tabindex="1"
                                               maxlength="60">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="centro_custo">Centro de Custo</label>
                                        <select  class="form-control" required
                                                 title="Selecione o nível de custo adequado!" tabindex="7" id="id_centro_custo_FK" name="id_centro_custo_FK">
                                            <option></option>
                                            <?
                                            foreach ($centro_custo as $value) {
                                                echo '<option value="' . $value['id_centro_custo'] . '">' . $value['nome'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div id='mensagens_cad' name='mensagens_cad'></div>

                                    </div>


                                </div>
                                <div class="form-group" align="right">
                                    <button class="btn btn-primary" id="cad_saida_caixa" tabindex="8">
                                        <span class="glyphicon glyphicon-floppy-disk"></span> Cadastrar</button>
                                </div>

                            </div>
                        </fieldset>
                    </form>

                </div>
                <!--- Editando utilizando typeaHead.js -->
                <div class="tab-pane" id="editar">
                    <p>
                    <legend>Primeiro busque a saída de caixa que deseja editar</legend>
                    <input id="busca-editar"
                           name="busca-editar"
                           type="text"
                           placeholder="Digite o nome da saída do caixa" autocomplete="off"
                           spellcheck="false" dir="auto"
                           class="form-control" data-items="12"
                           data-provide="typeahead" style="width: 50%;"
                           onclick="this.value = ''"/>
                    <!-- form para edição do saida_caixa -->
                    <div class="alerta">
                        <!-- Error no cad saida_caixa -->
                        <div class="alert alert-warning alert-dismissable" id="erro_edit" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?= $this->cad_erro ?>
                            <div class="btn-alerta">
                                <button type="button" class="btn btn-warning" id="btn_tentar_edit">Tentar novamente</button>
                            </div>
                        </div>
                        <!-- Deu tudo certo -->
                        <div class="alert alert-success alert-dismissable" id="ok_edit" style="display:none;" >
                            <button type="button" class="close">×</button>
                            <?= $this->cad_ok ?>
                        </div>
                    </div>
                    <div id="modifica-saida_caixa">
                        <form  role="form" id="frm_edit" name="frm_edit" method="post" action="<?= URL ?>saida_caixa/modifica">
                            <fieldset>
                                <legend>Modifique o centro de custo</legend>
                                <div class="alerta">
                                    <!-- Error no cad saida_caixa -->
                                    <div class="alert alert-warning alert-dismissable" id="cad-erro" style="display:none;">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <?= $this->cad_erro ?>
                                        <div class="btn-alerta">
                                            <button type="button" class="btn btn-warning" id="btn-tentar">Tentar novamente</button>
                                        </div>
                                    </div>
                                    <!-- Deu tudo certo -->
                                    <div class="alert alert-success alert-dismissable" id="cad-ok" style="display:none;" >
                                        <button type="button" class="close">×</button>
                                        <?= $this->cad_ok ?>
                                        <div class="btn-alerta">
                                            <button type="button" class="btn btn-success" id="btn-cad-novo">Cadastrar outro?</button>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" class="span1" name="idsaida_caixa" id="idsaida_caixa" value="" />


                                <div class="row" id="div-edit-saida_caixa">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nome">Nome</label>
                                            <input type="text"
                                                   class="form-control" id="nome"
                                                   name="nome" required
                                                   placeholder="Será exposto em relatórios"
                                                   title="Nome obrigatório"
                                                   tabindex="1"
                                                   maxlength="60">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="valor">Valor da Saída do Caixa</label>
                                            <input type="text"
                                                   class="form-control" id="valor"
                                                   name="valor" required
                                                   placeholder="Será exposto em relatórios"
                                                   title="Valor obrigatório"
                                                   tabindex="1"
                                                   maxlength="50">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="centro_custo">Centro de custo</label>
                                            <select  class="form-control" required
                                                     title="Selecione o nível de custo adequado!" tabindex="7" id="id_centro_custo_FK" name="id_centro_custo_FK">
                                                <option></option>
                                                <?
                                                foreach ($centro_custo as $value) {
                                                    echo '<option value="' . $value['id_centro_custo'] . '">' . $value['nome'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div id='mensagens_edit' name='mensagens_edit'></div>

                                        </div>


                                    </div>
                                    <div class="form-group" align="right">
                                        <button class="btn btn-primary" id="btn_edit"><span class="glyphicon glyphicon-edit"></span> Editar</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>


                    </p>
                </div>
                <div class="tab-pane" id="excluir">
                    <p>
                    <legend>Primeiro busque o saída de caixa que deseja EXCLUIR</legend>
                    <input id="busca_excluir"
                           name="busca_excluir"
                           type="text"
                           placeholder="Digite o nome do saída caixa" autocomplete="off"
                           spellcheck="false" dir="auto"
                           class="form-control" data-items="12"
                           data-provide="typeahead" style="width: 50%;"
                           onclick="this.value = ''"/>

                    <div id="exclui_saida_caixa">
                        <form  role="form" id="frm_delete" name="frm_delete" >
                            <fieldset>
                                <legend>Excluindo a Saída do Caixa</legend>
                                <input type="hidden" class="span1" name="id_saida_caixa" id="id_saida_caixa" value="" />
                                <div class="row" id="div-edit-saida_caixa">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nome">Nome</label>
                                            <input type="text" class="form-control"
                                                   id="nome"
                                                   name="nome"
                                                   required
                                                   readonly=""
                                                   maxlength="45">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="valor">Valor da Saída</label>
                                            <input type="text"
                                                   class="form-control" id="valor"
                                                   name="valor" readonly=""
                                                   placeholder="Será exposto em relatórios"
                                                   title="Valor obrigatório"
                                                   tabindex="1"
                                                   maxlength="60">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="Finalidade">Centro de Custo</label>
                                            <select  class="form-control" disabled
                                                     title="Selecione o nível de custo adequado!" tabindex="7" id="id_centro_custo_FK" name="id_centro_custo_FK">
                                                <option></option>
                                                <?
                                                foreach ($centro_custo as $value) {
                                                    echo '<option value="' . $value['id_centro_custo'] . '">' . $value['nome'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="form-group" align="right">
                                            <button class="btn btn-danger" id="btn_edit" data-toggle="modal" data-target="#confirma"><span class="glyphicon glyphicon-floppy-remove"></span> Excluir</button>
                                        </div>
                                    </div>

                                </div>

                            </fieldset>
                        </form>

                        <!-- Modal -->
                        <div class="modal fade" id="confirma" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Você está certo disto?</h4>
                                    </div>
                                    <div class="modal-body">
                                        Ao excluir este processo será irreversível, você realmente tem certeza disto?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
                                        <button type="button" id="btn_excluir" class="btn btn-danger">Tenho certeza e vou excluir</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<link href="<?= CSS ?>dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= CSS ?>TableTools.css" rel="stylesheet">
<link href="<?= CSS ?>typeahead.js-bootstrap.css" rel="stylesheet">
<?
$data['include'] = array("tab.js", "aba-lateral.js", "inputmask.js",
    "jquery.dataTables.min.js", "dataTables.bootstrap.js",
    "TableTools.min.js", "", "bootstrap3-typeahead.min.js", "saida_caixa.js", "jquery.maskMoney.js");
?>
<?php $this->load->view('layout/footer', $data); ?>

<script type="text/javascript">
    var ajaxurl = "saida_caixa/pega_saida_caixas";
</script>
<script type="text/javascript" src="<?= JS ?>listando-tabela.js"></script>


