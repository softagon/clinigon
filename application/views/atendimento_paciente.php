<?php
$data['titulo'] = "Iniciar atendimento";
$this->load->view('layout/header', $data);
?>

<link href="<?= CSS ?>tab-left.css" rel="stylesheet">

<div class="container">
    <div class="row">
        <div class="pull-right icone">
            <img src="<?= IMG ?>atender_icone.jpg" alt="atender"/>
        </div>

        <div class="page-header">
            <h1>
                <?= $data['titulo'] ?>
                <div>
                    <small>Aqui a mágica acontece.</small>
                </div>
            </h1>
        </div>

        
        
        
    </div>


</div>
</div>
<link href="<?= CSS ?>TableTools.css" rel="stylesheet">
<link href="<?= CSS ?>typeahead.js-bootstrap.css" rel="stylesheet">
<?
$data['include'] = array("tab.js", "aba-lateral.js", "inputmask.js",
    "jquery.dataTables.min.js", "dataTables.bootstrap.js",
    "TableTools.min.js", "", "bootstrap3-typeahead.min.js", "atender.js",
    "jquery.maskMoney.js", "atender_associado.js", "atender_exame.js", "atender_convenio.js");
?>
<?php $this->load->view('layout/footer', $data); ?>



