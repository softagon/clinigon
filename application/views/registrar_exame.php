<?php
$data['titulo'] = "Registrar um exame";
$this->load->view('layout/header', $data);
?>

<link href="<?= CSS ?>tab-left.css" rel="stylesheet">

<div class="container">
    <div class="row">
        <div class="pull-right icone">
            <img src="<?= IMG ?>registrar_exame_icone.jpg" alt="Registrar exame"/>
        </div>

        <div class="page-header">
            <h1>
                <?= $data['titulo'] ?>
                <div>
                    <small>Você conectará um exame ao paciente da clínica.</small>
                </div>
            </h1>
        </div>
        <div class="row">

            <div id="paciente">
                <fieldset>
                    <legend>Paciente</legend>
                    <div class="form-group" id="caixa-busca-paciente">
                        <label for="disabledTextInput">Encontre o paciente</label>
                        <input type="text" class="form-control input-lg" 
                               id="busca-paciente" placeholder="Paciente, documento"
                               onclick="this.value = ''">
                    </div>
                    <div class="row" id="paciente_dados" style="display: none">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="paciente">Nome</label>
                                <p class="text-muted" id="paciente_nome"></p>
                                <input type="hidden" name="id_paciente" id="id_paciente" value="" />
                            </div>
                            <div class="form-group">
                                <label for="paciente">Cidade/UF</label>
                                <p class="text-muted" id="cidade_uf_paciente"></p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="paciente">Documento</label>
                                <p class="text-muted" id="documento_paciente"></p>
                            </div>
                            <div class="form-group">
                                <label for="paciente">Celular</label>
                                <p class="text-muted" id="celular_paciente"></p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="paciente">Nascimento</label>
                                <p class="text-muted" id="nascimento_paciente"></p>
                            </div>
                            <div class="form-group">
                                <label for="paciente">Idade</label>
                                <p class="text-muted" id="idade_paciente"></p>
                            </div>
                        </div>
                    </div>
                    <div  id="confirma-paciente" align="right">
                        <a id="btn_paciente" type="button" class="btn btn-primary">Confirma paciente?</a>
                    </div>
                </fieldset>
            </div>
            
           
            </div>
              
                    
            <div id="exames" style="display: none">
                <fieldset name="exame_point">
                    <legend>Buscar exames</legend>
                    <div class="form-group" id="caixa-busca-exame">
                        <label for="disabledTextInput">Encontre o tipo de exame</label>
                        <input type="text" class="form-control input-lg" 
                               id="busca-exame" placeholder="Exame, tipo"
                               onclick="this.value = ''">
                    </div>
                    <div class="row" id="exame_dados" style="display: none">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exame">Nome do exame</label>
                                <p class="text-muted" id="nome_exame"></p>
                                <input type="hidden" name="id_exame" id="id_exame" value="" />  
                            </div>                                                
                            <div class="form-group">
                                <label id="valor_convenio_lbl">Valor Convênio</label>
                                <p class="text-muted" id="valor_convenio_exame"></p>
                            </div>
                         </div>    
                        <div class="col-md-6">    
                           <div class="form-group">
                                <label id="valor_exame_lbl">Valor Particular</label>
                                <p class="text-muted" id="valor_exame"></p>
                            </div>
                                                 
                             <div class="form-group">
                                <label for="exame">Comissão</label>
                                <p class="text-muted" id="comissao_exame"></p>
                            </div>
                        </div>

                    </div>
                    <div  id="confirma-exame" align="right">
                        <a id="btn_exame" type="button" class="btn btn-primary">Confirma exame?</a>
                    </div>
                </fieldset>
        
            <div  id="tipo_atendimento" style="display: none">
                <fieldset>
                    <legend>Defina qual modalidade realizará o exame</legend>
                    <div class="col-xs-4">
                        <button type="button" id="btn_convenio_escolha" class="btn btn-primary btn-lg btn-block">Convênio</button>
                    </div>
                    <div class="col-xs-4">
                        <button type="button" id="btn_clinica_escolha" class="btn btn-info btn-lg btn-block">Clínica</button>
                    </div>
                    <div class="col-xs-4">
                        <button type="button" id="btn_medico_escolha" class="btn btn-warning btn-lg btn-block">Médico</button>
                    </div>
                </fieldset>
                <input type="hidden" name="tipo_consulta" id="tipo_consulta" value="" />
            </div>
                               
                
            <div id="associado" style="display: none">
                <fieldset name="associado_point">
                    <legend>Associado (médico)</legend>
                    <div class="form-group" id="caixa-busca-associado">
                        <label for="disabledTextInput">Encontre o associado</label>
                        <input type="text" class="form-control input-lg" 
                               id="busca-associado" placeholder="Associado, documento"
                               onclick="this.value = ''">
                    </div>
                    <div class="row" id="associado_dados" style="display: none">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="associado">Nome</label>
                                <p class="text-muted" id="associado_nome"></p>
                                <input type="hidden" name="id_associado" id="id_associado" value="" />
                            </div>
                            <div class="form-group">
                                <label id="associado_valor_lbl">Valor</label>
                                <p class="text-muted" id="valor"></p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="associado">Documento</label>
                                <p class="text-muted" id="documento_associado"></p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="associado">Especialidade</label>
                                <p class="text-muted" id="especialidade"></p>
                            </div>
                        </div>
                    </div>
                    <div  id="confirma-associado" align="right">
                        <a id="btn_associado" type="button" class="btn btn-primary">Confirma associado?</a>
                    </div>
                </fieldset>
            </div>
                
             <div id="convenio" style="display: none">
                <fieldset>
                    <legend>Convênio</legend>
                    <div class="form-group" id="caixa-busca-convenio">
                        <label for="caixa-busca-convenio">Encontre a Empresa convêniada</label>
                        <input type="text" class="form-control input-lg" 
                               id="busca-convenio" placeholder="Empresa convêniada"
                               onclick="this.value = ''">
                    </div>
                    <div class="row" id="convenio_dados" style="display: none">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="convenio_nome">Nome</label>
                                <p class="text-muted" id="convenio_nome"></p>

                            </div>
                            <div class="form-group">
                                <label for="convenio_cidade_uf">Cidade/UF</label>
                                <p class="text-muted" id="convenio_cidade_uf"></p>
                            </div>

                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="convenio_telefone">Telefone</label>
                                <p class="text-muted" id="convenio_telefone"></p>
                            </div>
                            <div class="form-group">
                                <label for="convenio_responsavel">Responsável/Contato</label>
                                <p class="text-muted" id="convenio_responsavel"></p>
                            </div>
                        </div>
                    </div>
                    <div  id="confirma-convenio" align="right">
                        <a id="btn_convenio" type="button" class="btn btn-primary">Confirma Empresa?</a>
                    </div>

                </fieldset>
            </div>   
                
                <div id="forma_pagamento" style="display: none">
                <fieldset>
                    <legend>Registro do pagamento</legend>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="paciente">Total</label>
                            <p class="text-muted" id="documento_paciente">Documento</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="paciente">Forma de pagamento</label>
                            <select  class="form-control" required
                                     title="Escolha a função exercida" tabindex="7" id="id_forma_pagamento_FK" name="id_forma_pagamento_FK">
                                <option></option>
                                <?
                                foreach ($formas as $value) {
                                    echo '<option value="' . $value['id_forma_pagamento'] . '">' . $value['nome'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="paciente">Valor pago agora</label>
                            <p class="text-muted" id="documento_paciente">Documento</p>
                        </div>
                    </div>

                    <div  id="confirma-exame" align="right">
                        <a href="#" id="btn_pagamento" type="button" class="btn btn-info">Finalizar e registrar o exame?</a>
                    </div>
                </fieldset>
            </div>

        </div>
        <div class="row">

        </div>
    </div>
    <link href="<?= CSS ?>typeahead.js-bootstrap.css" rel="stylesheet">
    <?
    $data['include'] = array("inputmask.js", "bootstrap3-typeahead.min.js",
        "jquery.maskMoney.js", "registrar_exame.js", "exame.js");
    ?>
    <?php $this->load->view('layout/footer', $data); ?>



