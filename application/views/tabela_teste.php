<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <meta name="description" content="Gestão de Clínicas Médica com o software da SOFTAGON">
        <meta name="author" content="Hermes Alves <hermes@softagon.com.br">
        <link rel="shortcut icon" href="<?= ICO ?>favicon.png">
        <link rel="apple-touch-icon" href="<?= ICO ?>apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= ICO ?>apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= ICO ?>apple-touch-icon-114x114.png">

        <title>CLINIGON - <?= (empty($titulo)) ? "CLINIGON / SOFTAGON" : $titulo; ?></title>

        <!-- Bootstrap core CSS -->
        <link href="<?= CSS ?>bootstrap.css" rel="stylesheet">
        <!-- Bootstrap theme -->
        <link href="<?= CSS ?>bootstrap-theme.css" rel="stylesheet">
        <!-- Bootstrap Icons  -->
        <link href="<?= CSS ?>bootstrap-glyphicons.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="<?= CSS ?>navbar-fixed-top.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="<?= CSS ?>clinigon.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="<?= JS ?>html5shiv.js"></script>
          <script src="<?= JS ?>respond.min.js"></script>
        <![endif]-->
    </head>
    <script type="text/javascript">
        $(".test").click(function() {

            $.get(endereco + "atender/pega_associados_sessao/")
                    .done(function(data) {

                //Gera a tela que exibe os associados
                $("#associados").fadeOut(500);
                $("#associado_dados > tbody").html("");

                var itemCount = 0;
                var subtotal = 0;
                for (var k in data) {
                    subtotal += parseFloat(data[k].associado_valor);
                    var newLine = '<tr><td>' + data[k].associado_nome + '</td><td>' + data[k].associado_doc + '</td><td>' + data[k].especialidade + '</td><td>' + data[k].associado_valor + '</td><td><a href="#" valor="' + data[k].id_associado + '" class="remover" id="ass' + data[k].id_associado + '" name="' + itemCount + '">#</a></td></tr>';
                    $('#associado_dados tbody').append(newLine);


                    $("#" + itemCount).click(function() {
                        var buttonId = $(this).attr("id");
                        alert(buttonId);
                        $("#ass" + buttonId).remove();
                    });
                    itemCount++;
                }
                subtotal = Math.round(subtotal * 100) / 100; //arredonda
                $("#associados_subtotal").val(subtotal);
                $("#associados").fadeIn(1000);
            });
        });
    </script>

    <body>
        <a href="#" class="test">REMVER</a>
        <table class="table table-hover table-condensed" id='associado_dados'>
            <thead>
                <tr>
                    <th>Associado</th>
                    <th>Documento</th>
                    <th>Especialidade</th>
                    <th>Valor</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

        
        
        
        
        <hr>
        <footer>
            <p>&copy; SOFTAGON INOVAÇÃO 2013</p>
        </footer>

        <!-- Bootstrap core JavaScript
                ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="<?= JS ?>jquery.js"></script>
        <script src="<?= JS ?>bootstrap.min.js"></script>
        <script src="<?= JS ?>button.js"></script>
        <script src="<?= JS ?>jquery.validate.min.js"></script>
        <?php
        if (!empty($include)) {
            foreach ($include as $valor) {

                if (!empty($valor))
                    echo "<script src='" . JS . $valor . "'></script>\n";
            }
        }
        ?>
    </body>
</html>
