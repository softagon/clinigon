<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?= ICO ?>favicon.png">

        <title>Entrar no CLINIGON da SOFTAGON</title>

        <!-- Bootstrap core CSS -->
        <link href="<?= CSS ?>bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="<?= CSS ?>signin.css" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="container">
            <center><img src="<?php echo(IMG . 'softagon-logo-login.png'); ?>" alt="SOFTAGON Inovação"/></center>
            <form class="form-signin" role="form" post='<?=URL?>' method="post">
                <h2 class="form-signin-heading" align='center'>CLINIGON</h2>
                <input type="email" id='email_usuario' name='email_usuario' class="form-control" placeholder="Seu E-mail" required autofocus>
                <input type="password" class="form-control" id='senha' name='senha' placeholder="Senha" required>
                <label class="checkbox">
                    <!--<input type="checkbox" value="lembrar senha"> Lembrar-->
                </label>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
            </form>

        </div> <!-- /container -->
<?
$data['include'] = array("inputmask.js");
?>
<?php $this->load->view('layout/footer', $data); ?>