<?php
$data['titulo'] = "Gestão de Dependentes";
$this->load->view('layout/header', $data);
?>

<link href="<?= CSS ?>tab-left.css" rel="stylesheet">

<div class="container">
    <div class="row">
        <div class="pull-right icone">
            <img src="<?= IMG ?>dependente_icone.jpg" alt="dependente"/>
        </div>

        <div class="page-header">
            <h1>
                <?= $data['titulo'] ?>
                <div>
                    <small>Os pacientes podem ter dependentes, menores de idade.</small>
                </div>
            </h1>
        </div>
        <div class="tabbable tabs-left" id="tabs-693402">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#listar" data-toggle="tab">
                        <span class="glyphicon glyphicon-floppy-save"></span> Listar
                    </a>
                </li>
                <li>
                    <a href="#cadastrar" data-toggle="tab">
                        <span class="glyphicon glyphicon-floppy-open"></span> Cadastrar
                    </a>
                </li>
                <li>
                    <a href="#editar" data-toggle="tab">
                        <span class="glyphicon glyphicon-edit   "></span> Editar
                    </a>
                </li>
                <li>
                    <a href="#excluir" data-toggle="tab">
                        <span class="glyphicon glyphicon-floppy-remove"></span> Excluir
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="listar">
                    <p>
                        <?php if (is_array($dependentes)) { ?>
                        <div  style="padding-left: 160px; content: 1px; ">
                            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered"  id="DT-tabela">
                                <thead>
                                    <tr>
                                        <th>Dependente</th>
                                        <th>Paciente</th>
                                        <th>Nascimento</th>
                                        <th>Sexo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="dataTables_empty">Carregando dados do servidor</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="spacer"></div>

                    <?php } else {
                        ?>
                        <h3>Informação</h3><br/>
                        <span class="alert alert-info">
                            Ainda não possui dependente cadastrado, que tal <a href="#cadastrar" onclick="abreTab('cadastrar', 19);" class="alert-link">cadastrar agora?</a>
                        </span>
                        <?php
                    }
                    ?>
                    </p>
                </div>
                <div class="tab-pane" id="cadastrar">
                    <form  role="form" id="frm_cad" name="frm_cad" method="post" action="<?= URL ?>dependente/cadastra">
                        <fieldset>
                            <legend>Cadastre seu novo dependente</legend>
                            <div class="alerta">
                                <!-- Error no cad dependente -->
                                <div class="alert alert-warning alert-dismissable" id="cad-erro" style="display:none;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?= $this->cad_erro ?>
                                    <div class="btn-alerta">
                                        <button type="button" class="btn btn-warning" id="btn-tentar">Tentar novamente</button>
                                    </div>
                                </div>
                                <!-- Deu tudo certo -->
                                <div class="alert alert-success alert-dismissable" id="cad-ok" style="display:none;" >
                                    <button type="button" class="close">×</button>
                                    <?= $this->cad_ok ?>
                                    <div class="btn-alerta">
                                        <button type="button" class="btn btn-success" id="btn-cad-novo">Cadastrar outro?</button>
                                    </div>
                                </div>
                            </div>


                            <div class="row" id="div-cad-dependente">
                                <input id="busca-paciente"
                                       name="busca-paciente"
                                       type="text"
                                       placeholder="Paciente, documento ou nome" autocomplete="off"
                                       spellcheck="false" dir="auto"
                                       class="form-control" data-items="12"
                                       data-provide="typeahead" style="width: 50%;margin-left: 12px;"
                                       onclick="this.value = ''"/>
                                <small style="width: 50%;margin-left: 12px;">Escolha o Paciente que receberá o dependente.</small>

                                <div id="paciente_cad" class="row" style="margin-left: 12px;">
                                    <hr/>
                                    <h4>Paciente que receberá dependente</h4>
                                    <div class="col-md-4">

                                        <p id="nome_paciente_cad" class="text-info"></p>
                                        <p id="documento_cad" class="text-info"> </p>
                                    </div>
                                    <div class="col-md-4">
                                        <p id="cidade_cad" class="text-info"></p>
                                        <p id="celular_cad" class="text-info"></p>
                                    </div>
                                </div>
                                <div id="cadastro_frm">
                                    <hr/>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="hidden" name="id_paciente_FK" id="id_paciente_FK" value="" />
                                            <label for="nome">Nome completo</label>
                                            <input type="text"
                                                   class="form-control" id="nome"
                                                   name="nome" required
                                                   placeholder="Nome do dependente"
                                                   title="Nome obrigatório"
                                                   tabindex="1"
                                                   maxlength="60">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nascimento">Nascimento</label>
                                            <input type="text" class="form-control"
                                                   id="nascimento"
                                                   name="nascimento"
                                                   required 
                                                   data-mask="99/99/9999"
                                                   title="Temos que saber"
                                                   tabindex="2"
                                                   maxlength="20">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="sexo">Sexo</label>
                                            <select class="form-control" id='sexo' name='sexo' tabindex="6"
                                                    required 
                                                    title='Obrigatório'>
                                                <option ></option>
                                                <option value='M'>Masculino</option>
                                                <option value='F'>Feminino</option>
                                            </select>
                                        </div>

                                        <div class="form-group" align="right">
                                            <button class="btn btn-primary" id="cad_dependente" tabindex="8"><span class="glyphicon glyphicon-floppy-disk"></span> Cadastrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                </div>
                <!--- Editando utilizando typeaHead.js -->
                <div class="tab-pane" id="editar">
                    <p>
                    <legend>Primeiro busque o dependente que deseja editar</legend>
                    <input id="busca-editar"
                           name="busca-editar"
                           type="text"
                           placeholder="Digite o nome do dependente" autocomplete="off"
                           spellcheck="false" dir="auto"
                           class="form-control" data-items="12"
                           data-provide="typeahead" style="width: 50%;"
                           onclick="this.value = ''"/>
                    <!-- form para edição do dependente -->
                    <div class="alerta">
                        <!-- Error no cad dependente -->
                        <div class="alert alert-warning alert-dismissable" id="erro_edit" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?= $this->cad_erro ?>
                            <div class="btn-alerta">
                                <button type="button" class="btn btn-warning" id="btn_tentar_edit">Tentar novamente</button>
                            </div>
                        </div>
                        <!-- Deu tudo certo -->
                        <div class="alert alert-success alert-dismissable" id="ok_edit" style="display:none;" >
                            <button type="button" class="close">×</button>
                            <?= $this->cad_ok ?>
                        </div>
                    </div>
                    <div id="modifica-dependente">
                        <form  role="form" id="frm_edit" name="frm_edit" method="post" action="<?= URL ?>dependente/modifica">
                            <fieldset>
                                <legend>Modifique o dependente</legend>
                                <div class="alerta">
                                    <!-- Error no cad dependente -->
                                    <div class="alert alert-warning alert-dismissable" id="cad-erro" style="display:none;">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <?= $this->cad_erro ?>
                                        <div class="btn-alerta">
                                            <button type="button" class="btn btn-warning" id="btn-tentar">Tentar novamente</button>
                                        </div>
                                    </div>
                                    <!-- Deu tudo certo -->
                                    <div class="alert alert-success alert-dismissable" id="cad-ok" style="display:none;" >
                                        <button type="button" class="close">×</button>
                                        <?= $this->cad_ok ?>
                                        <div class="btn-alerta">
                                            <button type="button" class="btn btn-success" id="btn-cad-novo">Cadastrar outro?</button>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" class="span1" name="iddependente" id="iddependente" value="" />

                                <div id="paciente_edit" class="row" style="margin-left: 12px;">

                                    <h4>Paciente responsável por este dependente</h4>
                                    <div class="col-md-4">

                                        <p id="nome_paciente_edit" class="text-info"></p>
                                        <p id="documento_edit" class="text-info"> </p>
                                    </div>
                                    <div class="col-md-4">
                                        <p id="cidade_edit" class="text-info"></p>
                                        <p id="celular_edit" class="text-info"></p>
                                    </div>
                                    
                                </div>

                                <div class="row" id="div-cad-dependente">
                                    <hr/>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nome">Nome completo</label>
                                            <input type="text"
                                                   class="form-control" id="nome"
                                                   name="nome" required
                                                   placeholder="Nome do dependente"
                                                   title="Nome obrigatório"
                                                   tabindex="1"
                                                   maxlength="60">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nascimento">Nascimento</label>
                                            <input type="text" class="form-control"
                                                   id="nascimento"
                                                   name="nascimento"
                                                   required 
                                                   data-mask="99/99/9999"
                                                   title="Temos que saber"
                                                   tabindex="2"
                                                   maxlength="20">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="sexo">Sexo</label>
                                            <select class="form-control" id='sexo' name='sexo' tabindex="6"
                                                    required 
                                                    title='Obrigatório'>
                                                <option ></option>
                                                <option value='M'>Masculino</option>
                                                <option value='F'>Feminino</option>
                                            </select>
                                        </div>

                                        <div class="form-group" align="right">
                                            <button class="btn btn-primary" id="btn_edit"><span class="glyphicon glyphicon-edit"></span> Editar</button>
                                        </div>
                                    </div>

                                </div>
                            </fieldset>
                        </form>
                    </div>


                    </p>
                </div>
                <div class="tab-pane" id="excluir">
                    <p>
                    <legend>Primeiro busque o dependente que deseja EXCLUIR</legend>
                    <input id="busca_excluir"
                           name="busca_excluir"
                           type="text"
                           placeholder="Digite o nome do dependente" autocomplete="off"
                           spellcheck="false" dir="auto"
                           class="form-control" data-items="12"
                           data-provide="typeahead" style="width: 50%;"
                           onclick="this.value = ''"/>

                    <div id="paciente_delete" class="row" style="margin-left: 12px;">

                                    <h4>Paciente responsável por este dependente</h4>
                                    <div class="col-md-4">

                                        <p id="nome_paciente_delete" class="text-info"></p>
                                        <p id="documento_delete" class="text-info"> </p>
                                    </div>
                                    <div class="col-md-4">
                                        <p id="cidade_delete" class="text-info"></p>
                                        <p id="celular_delete" class="text-info"></p>
                                    </div>
                                    
                                </div>
                    
                    <div id="exclui_dependente">
                        <hr>
                        <form  role="form" id="frm_delete" name="frm_delete" >
                            <fieldset>
                                <legend>Excluindo o dependente</legend>
                                <input type="hidden" class="span1" name="id_dependente" id="id_dependente" value="" />
                                <div class="row" id="div-cad-dependente">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nome">Nome completo</label>
                                            <input type="text"
                                                   class="form-control" id="nome"
                                                   name="nome" required readonly
                                                   tabindex="1"
                                                   maxlength="60">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nascimento">Nascimento</label>
                                            <input type="text" class="form-control"
                                                   id="nascimento"
                                                   name="nascimento"
                                                   required readonly
                                                   maxlength="20">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="sexo">Sexo</label>
                                            <select class="form-control" id='sexo' name='sexo' tabindex="6"
                                                    required readonly>
                                                <option ></option>
                                                <option value='M'>Masculino</option>
                                                <option value='F'>Feminino</option>
                                            </select>
                                        </div>

                                        <div class="form-group" align="right">
                                            <button class="btn btn-danger" id="btn_edit" data-toggle="modal" data-target="#confirma"><span class="glyphicon glyphicon-floppy-remove"></span> Excluir</button>
                                        </div>
                                    </div>

                                </div>
                            </fieldset>
                        </form>

                        <!-- Modal -->
                        <div class="modal fade" id="confirma" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Você está certo disto?</h4>
                                    </div>
                                    <div class="modal-body">
                                        Ao excluir este processo será irreversível, você realmente tem certeza disto?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
                                        <button type="button" id="btn_excluir" class="btn btn-danger">Tenho certeza e vou excluir</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<link href="<?= CSS ?>dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= CSS ?>TableTools.css" rel="stylesheet">
<link href="<?= CSS ?>typeahead.js-bootstrap.css" rel="stylesheet">
<?
$data['include'] = array("tab.js", "aba-lateral.js", "inputmask.js",
    "jquery.dataTables.min.js", "dataTables.bootstrap.js",
    "TableTools.min.js", "", "bootstrap3-typeahead.min.js", "dependente.js",
    "jquery.maskMoney.js");
?>
<?php $this->load->view('layout/footer', $data); ?>

<script type="text/javascript">
                                var ajaxurl = "dependente/pega_dependentes";
</script>
<script type="text/javascript" src="<?= JS ?>listando-tabela.js"></script>


