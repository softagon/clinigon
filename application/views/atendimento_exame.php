<?php
$data['titulo'] = "Iniciar atendimento";
$this->load->view('layout/header', $data);
?>

<link href="<?= CSS ?>tab-left.css" rel="stylesheet">

<div class="container">
    <div class="row">
        <div class="pull-right icone">
            <img src="<?= IMG ?>atender_icone.jpg" alt="atender"/>
        </div>

        <div class="page-header">
            <h1>
                <?= $data['titulo'] ?>
                <div>
                    <small>Aqui a mágica acontece.</small>
                </div>
            </h1>
        </div>
<!--- Editando utilizando typeaHead.js -->
                <div class="tab-pane" id="editar">
                    <p>
                    <legend>Primeiro busque o exame que deseja cadastrar</legend>
                    <input id="busca-editar"
                           name="busca-editar"
                           type="text"
                           placeholder="Digite o nome do exame" autocomplete="off"
                           spellcheck="false" dir="auto"
                           class="form-control" data-items="12"
                           data-provide="typeahead" style="width: 50%;"
                           onclick="this.value = ''"/>
                    <!-- form para edição do exame -->
                    <div class="alerta">
                        <!-- Error no cad exame -->
                        <div class="alert alert-warning alert-dismissable" id="erro_edit" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?= $this->cad_erro ?>
                            <div class="btn-alerta">
                                <button type="button" class="btn btn-warning" id="btn_tentar_edit">Tentar novamente</button>
                            </div>
                        </div>
                        <!-- Deu tudo certo -->
                        <div class="alert alert-success alert-dismissable" id="ok_edit" style="display:none;" >
                            <button type="button" class="close">×</button>
                            <?= $this->cad_ok ?>
                        </div>
                    </div>
                    <div id="modifica-exame">
                        <form  role="form" id="frm_edit" name="frm_edit" method="post" action="<?= URL ?>exame/modifica">
                            <fieldset>
                                <legend>Informações do exame</legend>
                                <div class="alerta">
                                    <!-- Error no cad exame -->
                                    <div class="alert alert-warning alert-dismissable" id="cad-erro" style="display:none;">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <?= $this->cad_erro ?>
                                        <div class="btn-alerta">
                                            <button type="button" class="btn btn-warning" id="btn-tentar">Tentar novamente</button>
                                        </div>
                                    </div>
                                    <!-- Deu tudo certo -->
                                    <div class="alert alert-success alert-dismissable" id="cad-ok" style="display:none;" >
                                        <button type="button" class="close">×</button>
                                        <?= $this->cad_ok ?>
                                        <div class="btn-alerta">
                                            <button type="button" class="btn btn-success" id="btn-cad-novo">Cadastrar outro?</button>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" class="span1" name="idexame" id="idexame" value="" />

                                <div class="row" id="div-cad-exame">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nome">Nome do Exame</label>
                                            <input type="text"
                                                   class="form-control" id="nome"
                                                   name="nome" readonly
                                                   placeholder="Tomografia sem contraste"
                                                   title="Exame obrigatório, use detalhes como: Radiografia braço esquerdo"
                                                   tabindex="1"
                                                   maxlength="60">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="comissao">Comissão do médico</label>
                                            <input type="number" class="form-control"
                                                   id="comissao" name="comissao" required
                                                   title="Use algo entre 0 e 100, apenas números."
                                                   size="2" min="0" max="100"
                                                   placeholder="Em porcentagem, 5% por exemplo. "
                                                   tabindex="2">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="valor_convenio">Valor por convênio</label>
                                            <input type="text"
                                                   class="form-control" id="valor_convenio"
                                                   name="valor_convenio" 
                                                   placeholder="Quanto a clínica recebe por convênio?"
                                                   tabindex="3"
                                                   maxlength="20">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="valor">Valor clínica</label>
                                            <input type="text" class="form-control"
                                                   id="valor"
                                                   name="valor"
                                                   required 
                                                   title="Valor cobrado pelo Exame"
                                                   placeholder="Quanto a clínica cobra?"
                                                   tabindex="4"
                                                   maxlength="20">
                                        </div>
                                    </div>
                                    <div class="form-group" align="right">
                                        <button class="btn btn-primary" id="btn_edit"><span class="glyphicon glyphicon-edit"></span> Editar</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>


                    </
        
       
        
    </div>


</div>
</div>
<link href="<?= CSS ?>TableTools.css" rel="stylesheet">
<link href="<?= CSS ?>typeahead.js-bootstrap.css" rel="stylesheet">
<?
$data['include'] = array("tab.js", "aba-lateral.js", "inputmask.js",
    "jquery.dataTables.min.js", "dataTables.bootstrap.js",
    "TableTools.min.js", "", "bootstrap3-typeahead.min.js", "atender.js",
    "jquery.maskMoney.js", "atendimento_exame.js",);
?>
<?php $this->load->view('layout/footer', $data); ?>



