<?php
$data['titulo'] = "Gestão de Exames";
$this->load->view('layout/header', $data);
?>

<link href="<?= CSS ?>tab-left.css" rel="stylesheet">

<div class="container">
    <div class="row">
        <div class="pull-right icone">
            <img src="<?= IMG ?>exame_icone.jpg" alt="exame"/>
        </div>

        <div class="page-header">
            <h1>
                <?= $data['titulo'] ?>
                <div>
                    <small>Adicione, edite ou remova os exames comercializados.</small>
                </div>
            </h1>
        </div>
        <div class="tabbable tabs-left" id="tabs-693402">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#listar" data-toggle="tab">
                        <span class="glyphicon glyphicon-floppy-save"></span> Listar
                    </a>
                </li>
                <li>
                    <a href="#cadastrar" data-toggle="tab">
                        <span class="glyphicon glyphicon-floppy-open"></span> Cadastrar
                    </a>
                </li>
                <li>
                    <a href="#editar" data-toggle="tab">
                        <span class="glyphicon glyphicon-edit   "></span> Editar
                    </a>
                </li>
                <li>
                    <a href="#excluir" data-toggle="tab">
                        <span class="glyphicon glyphicon-floppy-remove"></span> Excluir
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="listar">
                    <p>
                        <?php if (is_array($exames)) { ?>
                        <div  style="padding-left: 160px; content: 1px; ">
                            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered"  id="DT-tabela">
                                <thead>
                                    <tr>
                                        <th width="5%">ID</th>
                                        <th width="20%">Nome</th>
                                        <th width="5%">Comissão</th>
                                        <th width="10%">Valor Particular</th>
                                        <th width="10%">Valor Convênio</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="dataTables_empty">Carregando dados do servidor</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="spacer"></div>

                    <?php } else {
                        ?>
                        <h3>Informação</h3><br/>
                        <span class="alert alert-info">
                            Ainda não possui exame cadastrado, que tal <a href="#cadastrar" onclick="abreTab('cadastrar', 19);" class="alert-link">cadastrar agora?</a>
                        </span>
                        <?php
                    }
                    ?>
                    </p>
                </div>
                <div class="tab-pane" id="cadastrar">
                    <form  role="form" id="frm_cad" name="frm_cad" method="post" action="<?= URL ?>exame/cadastra">
                        <fieldset>
                            <legend>Cadastre seu novo exame</legend>
                            <div class="alerta">
                                <!-- Error no cad exame -->
                                <div class="alert alert-warning alert-dismissable" id="cad-erro" style="display:none;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?= $this->cad_erro ?>
                                    <div class="btn-alerta">
                                        <button type="button" class="btn btn-warning" id="btn-tentar">Tentar novamente</button>
                                    </div>
                                </div>
                                <!-- Deu tudo certo -->
                                <div class="alert alert-success alert-dismissable" id="cad-ok" style="display:none;" >
                                    <button type="button" class="close">×</button>
                                    <?= $this->cad_ok ?>
                                    <div class="btn-alerta">
                                        <button type="button" class="btn btn-success" id="btn-cad-novo">Cadastrar outro?</button>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="div-cad-exame">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nome">Nome do Exame</label>
                                        <input type="text"
                                               class="form-control" id="nome"
                                               name="nome" required
                                               placeholder="Tomografia sem contraste"
                                               title="Exame obrigatório, use detalhes como: Radiografia braço esquerdo"
                                               tabindex="1"
                                               maxlength="60">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="comissao">Comissão do médico</label>
                                        <input type="number" class="form-control"
                                               id="comissao" name="comissao" required
                                               title="Use algo entre 0 e 100, apenas números."
                                               size="2" min="0" max="100"
                                               placeholder="Em porcentagem, 5% por exemplo. "
                                               tabindex="2">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="valor_convenio">Valor por convênio</label>
                                        <input type="text"
                                               class="form-control" id="valor_convenio"
                                               name="valor_convenio" 
                                               placeholder="Quanto a clínica recebe por convênio?"
                                               tabindex="3"
                                               maxlength="20">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="valor">Valor clínica</label>
                                        <input type="text" class="form-control"
                                               id="valor"
                                               name="valor"
                                               required 
                                               title="Valor cobrado pelo Exame"
                                               placeholder="Quanto a clínica cobra?"
                                               tabindex="4"
                                               maxlength="20">
                                    </div>

                                </div>

                            </div>
                            <div class="form-group" style="padding-left: 20px;" align='right'>
                                <button class="btn btn-primary" id="cad_exame" tabindex="8"><span class="glyphicon glyphicon-floppy-disk"></span> Cadastrar</button>
                            </div>
                        </fieldset>
                    </form>

                </div>
                <!--- Editando utilizando typeaHead.js -->
                <div class="tab-pane" id="editar">
                    <p>
                    <legend>Primeiro busque o exame que deseja editar</legend>
                    <input id="busca-editar"
                           name="busca-editar"
                           type="text"
                           placeholder="Digite o nome do exame" autocomplete="off"
                           spellcheck="false" dir="auto"
                           class="form-control" data-items="12"
                           data-provide="typeahead" style="width: 50%;"
                           onclick="this.value = ''"/>
                    <!-- form para edição do exame -->
                    <div class="alerta">
                        <!-- Error no cad exame -->
                        <div class="alert alert-warning alert-dismissable" id="erro_edit" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?= $this->cad_erro ?>
                            <div class="btn-alerta">
                                <button type="button" class="btn btn-warning" id="btn_tentar_edit">Tentar novamente</button>
                            </div>
                        </div>
                        <!-- Deu tudo certo -->
                        <div class="alert alert-success alert-dismissable" id="ok_edit" style="display:none;" >
                            <button type="button" class="close">×</button>
                            <?= $this->cad_ok ?>
                        </div>
                    </div>
                    <div id="modifica-exame">
                        <form  role="form" id="frm_edit" name="frm_edit" method="post" action="<?= URL ?>exame/modifica">
                            <fieldset>
                                <legend>Modifique o exame</legend>
                                <div class="alerta">
                                    <!-- Error no cad exame -->
                                    <div class="alert alert-warning alert-dismissable" id="cad-erro" style="display:none;">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <?= $this->cad_erro ?>
                                        <div class="btn-alerta">
                                            <button type="button" class="btn btn-warning" id="btn-tentar">Tentar novamente</button>
                                        </div>
                                    </div>
                                    <!-- Deu tudo certo -->
                                    <div class="alert alert-success alert-dismissable" id="cad-ok" style="display:none;" >
                                        <button type="button" class="close">×</button>
                                        <?= $this->cad_ok ?>
                                        <div class="btn-alerta">
                                            <button type="button" class="btn btn-success" id="btn-cad-novo">Cadastrar outro?</button>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" class="span1" name="idexame" id="idexame" value="" />

                                <div class="row" id="div-cad-exame">

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nome">Nome do Exame</label>
                                            <input type="text"
                                                   class="form-control" id="nome"
                                                   name="nome" required
                                                   placeholder="Tomografia sem contraste"
                                                   title="Exame obrigatório, use detalhes como: Radiografia braço esquerdo"
                                                   tabindex="1"
                                                   maxlength="60">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="comissao">Comissão do médico</label>
                                            <input type="number" class="form-control"
                                                   id="comissao" name="comissao" required
                                                   title="Use algo entre 0 e 100, apenas números."
                                                   size="2" min="0" max="100"
                                                   placeholder="Em porcentagem, 5% por exemplo. "
                                                   tabindex="2">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="valor_convenio">Valor por convênio</label>
                                            <input type="text"
                                                   class="form-control" id="valor_convenio"
                                                   name="valor_convenio" 
                                                   placeholder="Quanto a clínica recebe por convênio?"
                                                   tabindex="3"
                                                   maxlength="20">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="valor">Valor clínica</label>
                                            <input type="text" class="form-control"
                                                   id="valor"
                                                   name="valor"
                                                   required 
                                                   title="Valor cobrado pelo Exame"
                                                   placeholder="Quanto a clínica cobra?"
                                                   tabindex="4"
                                                   maxlength="20">
                                        </div>
                                    </div>
                                    <div class="form-group" align="right">
                                        <button class="btn btn-primary" id="btn_edit"><span class="glyphicon glyphicon-edit"></span> Editar</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>


                    </p>
                </div>
                <div class="tab-pane" id="excluir">
                    <p>
                    <legend>Primeiro busque o exame que deseja EXCLUIR</legend>
                    <input id="busca_excluir"
                           name="busca_excluir"
                           type="text"
                           placeholder="Digite o nome do exame" autocomplete="off"
                           spellcheck="false" dir="auto"
                           class="form-control" data-items="12"
                           data-provide="typeahead" style="width: 50%;"
                           onclick="this.value = ''"/>

                    <div id="exclui_exame">
                        <form  role="form" id="frm_delete" name="frm_delete" >
                            <fieldset>
                                <legend>Excluindo o exame</legend>
                                <input type="hidden" class="span1" name="id_exame" id="id_exame" value="" />
                                <div class="row" id="div-cad-exame">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nome">Nome do Exame</label>
                                            <input type="text"
                                                   class="form-control" id="nome"
                                                   name="nome" readonly=""
                                                   placeholder="Tomografia sem contraste"
                                                   title="Exame obrigatório, use detalhes como: Radiografia braço esquerdo"
                                                   tabindex="1"
                                                   maxlength="60">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="comissao">Comissão do médico</label>
                                            <input type="number" class="form-control"
                                                   id="comissao" name="comissao" readonly=""
                                                   title="Use algo entre 0 e 100, apenas números."
                                                   size="2" min="0" max="100"
                                                   placeholder="Em porcentagem, 5% por exemplo. "
                                                   tabindex="2">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="valor_convenio">Valor por convênio</label>
                                            <input type="text"
                                                   class="form-control" id="valor_convenio"
                                                   name="valor_convenio" readonly=""
                                                   placeholder="Quanto a clínica recebe por convênio?"
                                                   tabindex="3"
                                                   maxlength="20">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="valor">Valor clínica</label>
                                            <input type="text" class="form-control"
                                                   id="valor"
                                                   name="valor"
                                                   readonly="" 
                                                   title="Valor cobrado pelo Exame"
                                                   placeholder="Quanto a clínica cobra?"
                                                   tabindex="4"
                                                   maxlength="20">
                                        </div>
                                    </div>
                                    <div class="form-group" align="right">
                                        <button class="btn btn-danger" id="btn_edit" data-toggle="modal" data-target="#confirma"><span class="glyphicon glyphicon-floppy-remove"></span> Excluir</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>

                        <!-- Modal -->
                        <div class="modal fade" id="confirma" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Você está certo disto?</h4>
                                    </div>
                                    <div class="modal-body">
                                        Ao excluir este processo será irreversível, você realmente tem certeza disto?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
                                        <button type="button" id="btn_excluir" class="btn btn-danger">Tenho certeza e vou excluir</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<link href="<?= CSS ?>dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= CSS ?>TableTools.css" rel="stylesheet">
<link href="<?= CSS ?>typeahead.js-bootstrap.css" rel="stylesheet">
<?
$data['include'] = array("tab.js", "aba-lateral.js", "inputmask.js",
    "jquery.dataTables.min.js", "dataTables.bootstrap.js",
    "TableTools.min.js", "bootstrap3-typeahead.min.js", "exame.js",
    "jquery.maskMoney.js");
?>
<?php $this->load->view('layout/footer', $data); ?>

<script type="text/javascript">
    var ajaxurl = "exame/pega_exames";
</script>
<script type="text/javascript" src="<?= JS ?>listando-tabela.js"></script>


