<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <meta name="description" content="Gestão de Clínicas Médica com o software da SOFTAGON">
        <meta name="author" content="Hermes Alves <hermes@softagon.com.br">
        <link rel="shortcut icon" href="<?= ICO ?>favicon.png">
        <link rel="apple-touch-icon" href="<?= ICO ?>apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= ICO ?>apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= ICO ?>apple-touch-icon-114x114.png">

        <title>CLINIGON - <?= (empty($titulo)) ? "CLINIGON / SOFTAGON" : $titulo; ?></title>

        <!-- Bootstrap core CSS -->
        <link href="<?= CSS ?>bootstrap.css" rel="stylesheet">
        <!-- Bootstrap theme -->
        <link href="<?= CSS ?>bootstrap-theme.css" rel="stylesheet">
        <!-- Bootstrap Icons  -->
        <link href="<?= CSS ?>bootstrap-glyphicons.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="<?= CSS ?>navbar-fixed-top.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="<?= CSS ?>clinigon.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="<?= JS ?>html5shiv.js"></script>
          <script src="<?= JS ?>respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <a name="topo"></a>
        <!-- Fixed navbar -->
        <div class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="<?= URL ?>"><img src="<?= IMG ?>softagon-clinigon-software.png" alt="O PDV da SOFTAGON" /></a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?= URL ?>">Inicial</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Atender <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= MENU ?>paciente">Adicionar paciente</a></li>
                                <li><a href="<?= MENU ?>registrar_consulta">Registrar consulta</a></li>
                                <li><a href="<?= MENU ?>registrar_exame">Registrar exame</a></li>
                                <!--<li><a href="<?= MENU ?>paciente">Em andamento</a></li>-->
                                <li><a href="<?= URL ?>atender/finalizados">Atendimentos registrados</a></li>
                            </ul>
                        </li>
                        <!--<li><a href="#contact">Contact</a></li>-->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gestão Interna<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= MENU ?>associado">Cadastrar Associados(médicos)</a></li>
                                <li><a href="<?= MENU ?>saida_caixa">Cadastrar saída do Caixa</a></li>
                                <li><a href="<?= MENU ?>centro_custo">Centros de Custos</a></li>
                                <li><a href="<?= MENU ?>convenio">Empresas convêniadas</a></li>
                                <li><a href="<?= MENU ?>especialidade">Especialidades da Clínica</a></li>
                                <li><a href="<?= MENU ?>exame">Exames oferecidos</a></li>

                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Financeiro <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= URL ?>caixa/entrada_financeira">Entrada financeira diária</a></li>
                                  <li><a href="<?= URL ?>caixa/saida">Saída financeira</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Configurações <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= MENU ?>usuario">Usuários</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><span class="glyphicon glyphicon-user"></span> <?= $this->session->userdata('nome'); ?></a></li>
                        <li><a href="<?= MENU ?>sair"><span class="glyphicon glyphicon-log-out"></span> Sair</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>