<?php
$data['titulo'] = "Gestão de Convenios";
$this->load->view('layout/header', $data);
?>

<link href="<?= CSS ?>tab-left.css" rel="stylesheet">

<div class="container">
    <div class="row">
        <div class="pull-right icone">
            <img src="<?= IMG ?>convenio_icone.jpg" alt="convenio"/>
        </div>

        <div class="page-header">
            <h1>
                <?= $data['titulo'] ?>
                <div>
                    <small>Adicione, edite ou remova seus convenios.</small>
                </div>
            </h1>
        </div>
        <div class="tabbable tabs-left" id="tabs-693402">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#listar" data-toggle="tab">
                        <span class="glyphicon glyphicon-floppy-save"></span> Listar
                    </a>
                </li>
                <li>
                    <a href="#cadastrar" data-toggle="tab">
                        <span class="glyphicon glyphicon-floppy-open"></span> Cadastrar
                    </a>
                </li>
                <li>
                    <a href="#editar" data-toggle="tab">
                        <span class="glyphicon glyphicon-edit   "></span> Editar
                    </a>
                </li>
                <li>
                    <a href="#excluir" data-toggle="tab">
                        <span class="glyphicon glyphicon-floppy-remove"></span> Excluir
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="listar">
                    <p>
                        <?php if (is_array($convenios)) { ?>
                        <div  style="padding-left: 160px; content: 1px; ">
                            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered"  id="DT-tabela">
                                <thead>
                                    <tr>
                                        <th width="20%">Nome</th>
                                        <th width="10%">CNPJ</th>
                                        <th>Responsável</th>
                                        <th>Telefone</th>
                                        <th  width="15%">Lugar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="dataTables_empty">Carregando dados do servidor</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="spacer"></div>

                    <?php } else {
                        ?>
                        <h3>Informação</h3><br/>
                        <span class="alert alert-info">
                            Ainda não possui convenio cadastrado, que tal <a href="#cadastrar" onclick="abreTab('cadastrar', 19);" class="alert-link">cadastrar agora?</a>
                        </span>
                        <?php
                    }
                    ?>
                    </p>
                </div>
                <div class="tab-pane" id="cadastrar">
                    <form  role="form" id="frm_cad" name="frm_cad" method="post" action="<?= URL ?>convenio/cadastra">
                        <fieldset>
                            <legend>Cadastre seu novo convenio</legend>
                            <div class="alerta">
                                <!-- Error no cad convenio -->
                                <div class="alert alert-warning alert-dismissable" id="cad-erro" style="display:none;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?= $this->cad_erro ?>
                                    <div class="btn-alerta">
                                        <button type="button" class="btn btn-warning" id="btn-tentar">Tentar novamente</button>
                                    </div>
                                </div>
                                <!-- Deu tudo certo -->
                                <div class="alert alert-success alert-dismissable" id="cad-ok" style="display:none;" >
                                    <button type="button" class="close">×</button>
                                    <?= $this->cad_ok ?>
                                    <div class="btn-alerta">
                                        <button type="button" class="btn btn-success" id="btn-cad-novo">Cadastrar outro?</button>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="div-cad-convenio">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nome">Nome para convênio</label>
                                        <input type="text"
                                               class="form-control" id="nome"
                                               name="nome" required
                                               placeholder="Diga o nome da Empresa ou Órgão público,"
                                               title="Nome obrigatório"
                                               tabindex="1"
                                               maxlength="60">
                                    </div>
                                    <div class="form-group">
                                        <label for="telefone">Telefone</label>
                                        <input type="text" class="form-control"
                                               id="telefone"
                                               name="telefone"
                                               data-mask="(99)9999-9999" 
                                               maxlength="20"
                                               tabindex="4">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cnpj">CNPJ</label>
                                        <input type="text" class="form-control"
                                               id="cnpj"
                                               name="cnpj"
                                               data-mask="99.999.999/9999-99"
                                               placeholder="CNPJ da Empresa ou do Órgão Público"
                                               tabindex="2"
                                               maxlength="20">
                                    </div>
                                    <div class="form-group">
                                        <label for="cidade">Cidade</label>
                                        <input type="text" class="form-control"
                                               id="cidade"
                                               name="cidade"
                                               required
                                               title="Diga onde fica"
                                               tabindex="5"
                                               maxlength="20">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="responsavel">Responsável (contato)</label>
                                        <input type="text" class="form-control"
                                               id="responsavel" name="responsavel" required
                                               placeholder="Pessoa de contato para cobrança"
                                               title="Obrigatório para cobrar"
                                               tabindex="3">
                                    </div>

                                    <div class="form-group">
                                        <label for="uf">UF</label>
                                        <select class="form-control" name="uf" id="uf" tabindex="7" required>
                                            <option></option>
                                            <?
                                            foreach ($uf as $value) {

                                                if ($value == "PE")
                                                    echo '<option value="' . $value . '" selected>' . $value . '</option>';
                                                else
                                                    echo '<option value="' . $value . '">' . $value . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="form-group" align="right">
                                        <button class="btn btn-primary" id="cad_convenio" tabindex="8"><span class="glyphicon glyphicon-floppy-disk"></span> Cadastrar</button>
                                    </div>
                                </div>

                            </div>
                        </fieldset>
                    </form>

                </div>
                <!--- Editando utilizando typeaHead.js -->
                <div class="tab-pane" id="editar">
                    <p>
                    <legend>Primeiro busque o convenio que deseja editar</legend>
                    <input id="busca-editar"
                           name="busca-editar"
                           type="text"
                           placeholder="Digite o nome do convenio" autocomplete="off"
                           spellcheck="false" dir="auto"
                           class="form-control" data-items="12"
                           data-provide="typeahead" style="width: 50%;"
                           onclick="this.value = ''"/>
                    <!-- form para edição do convenio -->
                    <div class="alerta">
                        <!-- Error no cad convenio -->
                        <div class="alert alert-warning alert-dismissable" id="erro_edit" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?= $this->cad_erro ?>
                            <div class="btn-alerta">
                                <button type="button" class="btn btn-warning" id="btn_tentar_edit">Tentar novamente</button>
                            </div>
                        </div>
                        <!-- Deu tudo certo -->
                        <div class="alert alert-success alert-dismissable" id="ok_edit" style="display:none;" >
                            <button type="button" class="close">×</button>
                            <?= $this->cad_ok ?>
                        </div>
                    </div>
                    <div id="modifica-convenio">
                        <form  role="form" id="frm_edit" name="frm_edit" method="post" action="<?= URL ?>convenio/modifica">
                            <fieldset>
                                <legend>Modifique o convenio</legend>
                                <div class="alerta">
                                    <!-- Error no cad convenio -->
                                    <div class="alert alert-warning alert-dismissable" id="cad-erro" style="display:none;">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <?= $this->cad_erro ?>
                                        <div class="btn-alerta">
                                            <button type="button" class="btn btn-warning" id="btn-tentar">Tentar novamente</button>
                                        </div>
                                    </div>
                                    <!-- Deu tudo certo -->
                                    <div class="alert alert-success alert-dismissable" id="cad-ok" style="display:none;" >
                                        <button type="button" class="close">×</button>
                                        <?= $this->cad_ok ?>
                                        <div class="btn-alerta">
                                            <button type="button" class="btn btn-success" id="btn-cad-novo">Cadastrar outro?</button>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" class="span1" name="idconvenio" id="idconvenio" value="" />

                                <div class="row" id="div-cad-convenio">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nome">Nome para convênio</label>
                                            <input type="text"
                                                   class="form-control" id="nome"
                                                   name="nome" required
                                                   placeholder="Diga o nome da Empresa ou Órgão público,"
                                                   title="Nome obrigatório"
                                                   tabindex="1"
                                                   maxlength="60">
                                        </div>
                                        <div class="form-group">
                                            <label for="telefone">Telefone</label>
                                            <input type="text" class="form-control"
                                                   id="telefone"
                                                   name="telefone"
                                                   data-mask="(99)9999-9999" 
                                                   maxlength="20"
                                                   tabindex="4">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="cnpj">CNPJ</label>
                                            <input type="text" class="form-control"
                                                   id="cnpj"
                                                   name="cnpj"
                                                   data-mask="99.999.999/9999-99"
                                                   placeholder="CNPJ da Empresa ou do Órgão Público"
                                                   tabindex="2"
                                                   maxlength="20">
                                        </div>
                                        <div class="form-group">
                                            <label for="cidade">Cidade</label>
                                            <input type="text" class="form-control"
                                                   id="cidade"
                                                   name="cidade"
                                                   required
                                                   title="Diga onde fica"
                                                   tabindex="5"
                                                   maxlength="20">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="responsavel">Responsável (contato)</label>
                                            <input type="text" class="form-control"
                                                   id="responsavel" name="responsavel" required
                                                   placeholder="Pessoa de contato para cobrança"
                                                   title="Obrigatório para cobrar"
                                                   tabindex="3">
                                        </div>

                                        <div class="form-group">
                                            <label for="uf">UF</label>
                                            <select class="form-control" name="uf" id="uf" tabindex="7" required>
                                                <option></option>
                                                <?
                                                foreach ($uf as $value) {

                                                    if ($value == "PE")
                                                        echo '<option value="' . $value . '" selected>' . $value . '</option>';
                                                    else
                                                        echo '<option value="' . $value . '">' . $value . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group" align="right">
                                            <button class="btn btn-primary" id="btn_edit"><span class="glyphicon glyphicon-edit"></span> Editar</button>
                                        </div>
                                    </div>

                                </div>
                            </fieldset>
                        </form>
                    </div>


                    </p>
                </div>
                <div class="tab-pane" id="excluir">
                    <p>
                    <legend>Primeiro busque o convenio que deseja EXCLUIR</legend>
                    <input id="busca_excluir"
                           name="busca_excluir"
                           type="text"
                           placeholder="Digite o nome do convenio" autocomplete="off"
                           spellcheck="false" dir="auto"
                           class="form-control" data-items="12"
                           data-provide="typeahead" style="width: 50%;"
                           onclick="this.value = ''"/>

                    <div id="exclui_convenio">
                        <form  role="form" id="frm_delete" name="frm_delete" >
                            <fieldset>
                                <legend>Excluindo o convenio</legend>
                                <input type="hidden" class="span1" name="id_convenio" id="id_convenio" value="" />
                                <div class="row" id="div-cad-convenio">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nome">Nome para convênio</label>
                                            <input type="text"
                                                   class="form-control" id="nome"
                                                   name="nome" readonly=""
                                                   maxlength="60">
                                        </div>
                                        <div class="form-group">
                                            <label for="telefone">Telefone</label>
                                            <input type="text" class="form-control"
                                                   id="telefone"
                                                   name="telefone"
                                                   readonly="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="cnpj">CNPJ</label>
                                            <input type="text" class="form-control"
                                                   id="cnpj"
                                                   name="cnpj"
                                                   readonly="">
                                        </div>
                                        <div class="form-group">
                                            <label for="cidade">Cidade</label>
                                            <input type="text" class="form-control"
                                                   id="cidade"
                                                   name="cidade"
                                                   readonly="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="responsavel">Responsável (contato)</label>
                                            <input type="text" class="form-control"
                                                   id="responsavel" name="responsavel" 
                                                   readonly="">
                                        </div>

                                        <div class="form-group">
                                            <label for="uf">UF</label>
                                            <select class="form-control" name="uf" id="uf" tabindex="7" readyonly>
                                                <option></option>
                                                <?
                                                foreach ($uf as $value) {

                                                    if ($value == "PE")
                                                        echo '<option value="' . $value . '" selected>' . $value . '</option>';
                                                    else
                                                        echo '<option value="' . $value . '">' . $value . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="form-group" align="right">
                                            <button class="btn btn-danger" id="btn_edit" data-toggle="modal" data-target="#confirma"><span class="glyphicon glyphicon-floppy-remove"></span> Excluir</button>
                                        </div>
                                    </div>

                                </div>
                            </fieldset>
                        </form>

                        <!-- Modal -->
                        <div class="modal fade" id="confirma" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Você está certo disto?</h4>
                                    </div>
                                    <div class="modal-body">
                                        Ao excluir este processo será irreversível, você realmente tem certeza disto?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
                                        <button type="button" id="btn_excluir" class="btn btn-danger">Tenho certeza e vou excluir</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<link href="<?= CSS ?>dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= CSS ?>TableTools.css" rel="stylesheet">
<link href="<?= CSS ?>typeahead.js-bootstrap.css" rel="stylesheet">
<?
$data['include'] = array("tab.js", "aba-lateral.js", "inputmask.js",
    "jquery.dataTables.min.js", "dataTables.bootstrap.js",
    "TableTools.min.js", "", "bootstrap3-typeahead.min.js", "convenio.js",
    "jquery.maskMoney.js");
?>
<?php $this->load->view('layout/footer', $data); ?>

<script type="text/javascript">
                            var ajaxurl = "convenio/pega_convenios";
</script>
<script type="text/javascript" src="<?= JS ?>listando-tabela.js"></script>


