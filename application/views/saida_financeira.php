<?php
$data['titulo'] = "Saída de dinheiro da Empresa";
$this->load->view('layout/header', $data);
?>

<link href="<?= CSS ?>tab-left.css" rel="stylesheet">

<div class="container" >
    <div class="row">
        <div class="pull-right icone">
            <img src="<?= IMG ?>caixa_diario_icone.jpg" alt="atender"/>
        </div>

        <div class="page-header">
            <h1>
                <?= $data['titulo'] ?>
                <div>
                    <small>Depesas dessa data  <?= date("d/m/Y") ?> </small>
                </div>
            </h1>
        </div>
        <div class="row" align="center">
            <center>
                <div class=" offset3 " align="center">
                    Despesas hoje <?= $hoje['totalf'] ?>
                </div>
                <div class="offset3" algin="center">
                    Despesas ontem <?= $ontem['totalf'] ?>
                </div>
            </center>
        </div>
        <div class="row" id="tabela">
            <div  style="content: 1px; ">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered"  id="DT-tabela">
                    <thead>
                                    <tr>
                                        <th width="30%">Nome</th>
                                        <th >Valor da Saída </th>
                                        <th >Centro Custo</th>
                                        <th >Cadastrado em</th>
                                    </tr>
                                </thead>
                    <tbody>
                        <tr>
                            <td class="dataTables_empty">Carregando dados do servidor</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>
</div>

<link href="<?= CSS ?>dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= CSS ?>TableTools.css" rel="stylesheet">
<?
$data['include'] = array("jquery.dataTables.min.js", "dataTables.bootstrap.js",
    "TableTools.min.js", "jquery.dataTables.columnFilter.js");
?>
<?php $this->load->view('layout/footer', $data); ?>

<script type="text/javascript">
    var ajaxurl = "caixa/despesas_diario";
</script>
<script type="text/javascript" src="<?= JS ?>1coluna-tabela.js"></script>
