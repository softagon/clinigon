<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?= ICO ?>favicon.png">

        <title><?=$titulo?> - SOFTAGON</title>

        <!-- Bootstrap core CSS -->
        <link href="<?= CSS ?>bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="<?= CSS ?>signin.css" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="container">
            <center><img src="<?php echo(IMG . 'softagon-logo-login.png'); ?>" alt="SOFTAGON Inovação"/></center>
           <br/>
           <div class='row' align='center'>
           <h1><?=$titulo?></h1>
           <br/>
           <h3><?=$mensagem?></h3>
           </div>

        </div> <!-- /container -->
<?
$data['include'] = array("inputmask.js");
?>
<?php $this->load->view('layout/footer', $data); ?>