<?php
$data['titulo'] = "Gestão de Pacientes";
$this->load->view('layout/header', $data);
?>

<link href="<?= CSS ?>tab-left.css" rel="stylesheet">

<div class="container">
    <div class="row">
        <div class="pull-right icone">
            <img src="<?= IMG ?>paciente_icone.jpg" alt="paciente"/>
        </div>

        <div class="page-header">
            <h1>
                <?= $data['titulo'] ?>
                <div>
                    <small>São as pessoas que possuem acesso ao sistema.</small>
                </div>
            </h1>
        </div>
        <div class="tabbable tabs-left" id="tabs-693402">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#listar" data-toggle="tab">
                        <span class="glyphicon glyphicon-floppy-save"></span> Listar
                    </a>
                </li>
                <li>
                    <a href="#cadastrar" data-toggle="tab">
                        <span class="glyphicon glyphicon-floppy-open"></span> Cadastrar
                    </a>
                </li>
                <li>
                    <a href="#editar" data-toggle="tab">
                        <span class="glyphicon glyphicon-edit   "></span> Editar
                    </a>
                </li>
                <li>
                    <a href="#excluir" data-toggle="tab">
                        <span class="glyphicon glyphicon-floppy-remove"></span> Excluir
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="listar">
                    <p>
                        <?php if (is_array($pacientes)) { ?>
                        <div  style="padding-left: 160px; content: 1px; ">
                            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered"  id="DT-tabela">
                                <thead>
                                    <tr>
                                        <th width="30%">Nome</th>
                                        <th >CPF</th>
                                        <th >Título Eleitor</th>
                                        <th>Cartão SUS</th>
                                        <th>Telefone</th>
                                        <th>Celular</th>
                                        <th>Estado Cívil</th>
                                        <th>Cidade</th>
                                        <th width="17%">UF</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="dataTables_empty">Carregando dados do servidor</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="spacer"></div>

                    <?php } else {
                        ?>
                        <h3>Informação</h3><br/>
                        <span class="alert alert-info">
                            Ainda não possui paciente cadastrado, que tal <a href="#cadastrar" onclick="abreTab('cadastrar', 19);" class="alert-link">cadastrar agora?</a>
                        </span>
                        <?php
                    }
                    ?>
                    </p>
                </div>
                <div class="tab-pane" id="cadastrar">
                    <form  role="form" id="frm_cad" name="frm_cad" method="post" action="<?= URL ?>paciente/cadastra">
                        <fieldset>
                            <legend>Cadastre seu novo paciente</legend>
                            <div class="alerta">
                                <!-- Error no cad paciente -->
                                <div class="alert alert-warning alert-dismissable" id="cad-erro" style="display:none;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?= $this->cad_erro ?>
                                    <div class="btn-alerta">
                                        <button type="button" class="btn btn-warning" id="btn-tentar">Tentar novamente</button>
                                    </div>
                                </div>
                                <!-- Deu tudo certo -->
                                <div class="alert alert-success alert-dismissable" id="cad-ok" style="display:none;" >
                                    <button type="button" class="close">×</button>
                                    <?= $this->cad_ok ?>
                                    <div class="btn-alerta">
                                        <button type="button" class="btn btn-success" id="btn-cad-novo">Cadastrar outro?</button>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="div-cad-paciente">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cpf">CPF</label>
                                        <input type="cpf"
                                               class="form-control" id="cpf"
                                               name="cpf"
                                               tabindex="1"
                                               data-mask='999.999.999-99'
                                               maxlength="60">
                                    </div>
                                    <div class="form-group" >
                                        <label for="nome">Nome Completo</label>
                                        <input type="text" class="form-control"
                                               id="nome"
                                               name="nome"
                                               required
                                               tabindex="4"
                                               title='Qual o nome do Paciente?'
                                               maxlength="45">
                                    </div>
                                    <div class="form-group">
                                        <label for="estado_civil">Estado Cívil</label>
                                        <select class="form-control" id='estado_civil'
                                                name='estado_civil'
                                                tabindex="7"
                                                required 
                                                title='Obrigatório'>
                                            <option></option>
                                            <option value='Solteiro(a)'>Solteiro(a)</option>
                                            <option value='Casado(a)'>Casado(a)</option>
                                            <option value='Divorciado(a)'>Divorciado(a)</option>
                                            <option value='Viúvo(a)'>Viúvo(a)</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="email_paciente">E-mail do paciente</label>
                                        <input type="email" class="form-control"
                                               id="email_paciente"
                                               name="email_paciente"
                                               tabindex="10"
                                               maxlength="45">
                                    </div>
                                    <div class="form-group">
                                        <label for="uf">UF</label>
                                        <select class="form-control" name="uf" id="uf" tabindex="13">
                                            <?
                                            foreach ($uf as $value) {

                                                if ($value == "PE")
                                                    echo '<option value="' . $value . '" selected>' . $value . '</option>';
                                                else
                                                    echo '<option value="' . $value . '">' . $value . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="sus">Cartão do SUS</label>
                                        <input type="text" class="form-control"
                                               id="sus"
                                               name="sus"
                                               tabindex="2"
                                               maxlength="15">
                                    </div>
                                    <div class="form-group">
                                        <label for="nascimento">Nascimento</label>
                                        <input type="text" class="form-control"
                                               id="nascimento"
                                               name="nascimento"
                                               required
                                               title='Campo obrigatório'
                                               tabindex="5"
                                               data-mask='99/99/9999'
                                               maxlength="45">
                                    </div>
                                    <div class="form-group">
                                        <label for="telefone">Telefone</label>
                                        <input type="text" class="form-control"
                                               id="telefone"
                                               name="telefone"
                                               tabindex="8"
                                               data-mask='(99)9999-9999'>
                                    </div>
                                    <div class="form-group">
                                        <label for="cep">CEP</label>
                                        <input type="text" class="form-control"
                                               id="cep"
                                               name="cep"
                                               tabindex="11"
                                               data-mask='99.999-999'
                                               required
                                               title='Para enviar correspondência'
                                               maxlength="45">
                                    </div>
                                    <div class="form-group">
                                        <label for="endereco">Rua, número e bairro</label>
                                        <textarea class="form-control" rows="3"
                                                  tabindex="14" id='endereco' required
                                                  name='endereco'
                                                  title='Importante o endereço'>
                                        </textarea>
                                    </div>


                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="titulo_eleitor">Título de Eleitor 
                                            <a href="http://www.tre-pe.jus.br/eleitor/situacao-eleitoral/situacao-eleitoral-consulta-por-nome" target="_blank" 
                                               class="glyphicon glyphicon-info-sign" title="Buscar título no TRE"></a></label>
                                        <input type="text" class="form-control"
                                               id="titulo_eleitor"
                                               name="titulo_eleitor"
                                               tabindex="3"
                                               maxlength="12">
                                         
                                    </div>
                                    <div class="form-group">
                                        <label for="sexo">Sexo</label>
                                        <select class="form-control" id='sexo' name='sexo' tabindex="6"
                                                required 
                                                title='Obrigatório'>
                                            <option ></option>
                                            <option value='M'>Masculino</option>
                                            <option value='F'>Feminino</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="celular">Celular</label>
                                        <input type="text" class="form-control"
                                               id="celular"
                                               name="celular"
                                               required
                                               title='Comunicação é importante'
                                               tabindex="9"
                                               data-mask='(99)9999-9999*'
                                               maxlength="15">
                                        <input type="hidden" name="operadora" id="operadora" value="" />
                                    </div>
                                    <div class="form-group">
                                        <label for="cidade">Cidade</label>
                                        <input type="text" class="form-control"
                                               id="cidade"
                                               name="cidade"
                                               required
                                               title='Qual cidade?'
                                               tabindex="12"
                                               maxlength="45">
                                    </div>
                                    <div class="form-group">
                                        <label for="observacoes">Observação</label>
                                        <textarea class="form-control" rows="3"
                                                  tabindex="14" id='observacoes'
                                                  name='observacoes'>
                                        </textarea>
                                    </div>


                                    <div class="form-group" align="right">
                                        <button class="btn btn-primary" id="cad_paciente" tabindex="16">
                                            <span class="glyphicon glyphicon-floppy-disk"></span> Cadastrar</button>
                                    </div>
                                </div>

                            </div>
                        </fieldset>
                    </form>

                </div>
                <!--- Editando utilizando typeaHead.js -->
                <div class="tab-pane" id="editar">
                    <p>
                    <legend>Primeiro busque o paciente que deseja editar</legend>
                    <input id="busca-editar"
                           name="busca-editar"
                           type="text"
                           placeholder="Digite o nome do paciente" autocomplete="off"
                           spellcheck="false" dir="auto"
                           class="form-control" data-items="12"
                           data-provide="typeahead" style="width: 50%;"
                           onclick="this.value = ''"/>
                    <!-- form para edição do paciente -->
                    <div class="alerta">
                        <!-- Error no cad paciente -->
                        <div class="alert alert-warning alert-dismissable" id="erro_edit" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?= $this->cad_erro ?>
                            <div class="btn-alerta">
                                <button type="button" class="btn btn-warning" id="btn_tentar_edit">Tentar novamente</button>
                            </div>
                        </div>
                        <!-- Deu tudo certo -->
                        <div class="alert alert-success alert-dismissable" id="ok_edit" style="display:none;" >
                            <button type="button" class="close">×</button>
                            <?= $this->cad_ok ?>
                        </div>
                    </div>
                    <div id="modifica-paciente">
                        <form  role="form" id="frm_edit" name="frm_edit" method="post" action="<?= URL ?>paciente/modifica">
                            <fieldset>
                                <legend>Modifique o paciente</legend>
                                <div class="alerta">
                                    <!-- Error no cad paciente -->
                                    <div class="alert alert-warning alert-dismissable" id="cad-erro" style="display:none;">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <?= $this->cad_erro ?>
                                        <div class="btn-alerta">
                                            <button type="button" class="btn btn-warning" id="btn-tentar">Tentar novamente</button>
                                        </div>
                                    </div>
                                    <!-- Deu tudo certo -->
                                    <div class="alert alert-success alert-dismissable" id="cad-ok" style="display:none;" >
                                        <button type="button" class="close">×</button>
                                        <?= $this->cad_ok ?>
                                        <div class="btn-alerta">
                                            <button type="button" class="btn btn-success" id="btn-cad-novo">Cadastrar outro?</button>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" class="span1" name="idpaciente" id="idpaciente" value="" />

                                <div class="row" id="div-edit-paciente">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="cpf">CPF</label>
                                            <input type="cpf"
                                                   class="form-control" id="cpf"
                                                   name="cpf"
                                                   tabindex="1"
                                                   data-mask='999.999.999-99'
                                                   maxlength="60">
                                        </div>
                                        <div class="form-group" >
                                            <label for="nome">Nome Completo</label>
                                            <input type="text" class="form-control"
                                                   id="nome"
                                                   name="nome"
                                                   required
                                                   tabindex="4"
                                                   title='Qual o nome do Paciente?'
                                                   maxlength="45">
                                        </div>
                                        <div class="form-group">
                                            <label for="estado_civil">Estado Cívil</label>
                                            <select class="form-control" id='estado_civil'
                                                    name='estado_civil'
                                                    tabindex="7"
                                                    required 
                                                    title='Obrigatório'>
                                                <option></option>
                                                <option value='Solteiro(a)'>Solteiro(a)</option>
                                                <option value='Casado(a)'>Casado(a)</option>
                                                <option value='Divorciado(a)'>Divorciado(a)</option>
                                                <option value='Viúvo(a)'>Viúvo(a)</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="email_paciente">E-mail do paciente</label>
                                            <input type="email" class="form-control"
                                                   id="email_paciente"
                                                   name="email_paciente"
                                                   tabindex="10"
                                                   maxlength="45">
                                        </div>
                                        <div class="form-group">
                                            <label for="uf">UF</label>
                                            <select class="form-control" name="uf" id="uf" tabindex="13">
                                                <?
                                                foreach ($uf as $value) {

                                                    if ($value == "PE")
                                                        echo '<option value="' . $value . '" selected>' . $value . '</option>';
                                                    else
                                                        echo '<option value="' . $value . '">' . $value . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="sus">Cartão do SUS</label>
                                            <input type="text" class="form-control"
                                                   id="sus"
                                                   name="sus"
                                                   tabindex="2"
                                                   maxlength="15">
                                        </div>
                                        <div class="form-group">
                                            <label for="nascimento">Nascimento</label>
                                            <input type="text" class="form-control"
                                                   id="nascimento"
                                                   name="nascimento"
                                                   required
                                                   title='Campo obrigatório'
                                                   tabindex="5"
                                                   data-mask='99/99/9999'
                                                   maxlength="45">
                                        </div>
                                        <div class="form-group">
                                            <label for="telefone">Telefone</label>
                                            <input type="text" class="form-control"
                                                   id="telefone"
                                                   name="telefone"
                                                   tabindex="8"
                                                   data-mask='(99)9999-9999'>
                                        </div>
                                        <div class="form-group">
                                            <label for="cep">CEP</label>
                                            <input type="text" class="form-control"
                                                   id="cep"
                                                   name="cep"
                                                   tabindex="11"
                                                   data-mask='99.999-999'
                                                   required
                                                   title='Para enviar correspondência'
                                                   maxlength="45">
                                        </div>
                                        <div class="form-group">
                                            <label for="endereco">Rua, número e bairro</label>
                                            <textarea class="form-control" rows="3"
                                                      tabindex="14" id='endereco' required
                                                      name='endereco'
                                                      title='Importante o endereço'>
                                            </textarea>
                                        </div>


                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="titulo_eleitor">Título de Eleitor 
                                            <a href="http://www.tre-pe.jus.br/eleitor/situacao-eleitoral/situacao-eleitoral-consulta-por-nome" target="_blank" 
                                               class="glyphicon glyphicon-info-sign" title="Buscar título no TRE"></a></label>
                                            <input type="text" class="form-control"
                                                   id="titulo_eleitor"
                                                   name="titulo_eleitor"
                                                   tabindex="3"
                                                   maxlength="12">
                                           
                                        </div>
                                        <div class="form-group">
                                            <label for="sexo">Sexo</label>
                                            <select class="form-control" id='sexo' name='sexo' tabindex="6"
                                                    required 
                                                    title='Obrigatório'>
                                                <option ></option>
                                                <option value='M'>Masculino</option>
                                                <option value='F'>Feminino</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="celular">Celular</label>
                                            <input type="text" class="form-control"
                                                   id="celular"
                                                   name="celular"
                                                   required
                                                   title='Comunicação é importante'
                                                   tabindex="9"
                                                   data-mask='(99)9999-9999*'
                                                   maxlength="15">
                                            <input type="hidden" name="operadora" id="operadora" value="" />
                                        </div>
                                        <div class="form-group">
                                            <label for="cidade">Cidade</label>
                                            <input type="text" class="form-control"
                                                   id="cidade"
                                                   name="cidade"
                                                   required
                                                   title='Qual cidade?'
                                                   tabindex="12"
                                                   maxlength="45">
                                        </div>
                                        <div class="form-group">
                                            <label for="observacoes">Observação</label>
                                            <textarea class="form-control" rows="3"
                                                      tabindex="14" id='observacoes'
                                                      name='observacoes'>
                                            </textarea>
                                        </div>
                                        <div class="form-group" align="right">
                                            <button class="btn btn-primary" id="btn_edit"><span class="glyphicon glyphicon-edit"></span> Editar</button>
                                        </div>
                                    </div>


                            </fieldset>
                        </form>
                    </div>


                    </p>
                </div>
                <div class="tab-pane" id="excluir">
                    <p>
                    <legend>Primeiro busque o paciente que deseja EXCLUIR</legend>
                    <input id="busca_excluir"
                           name="busca_excluir"
                           type="text"
                           placeholder="Digite o nome do paciente" autocomplete="off"
                           spellcheck="false" dir="auto"
                           class="form-control" data-items="12"
                           data-provide="typeahead" style="width: 50%;"
                           onclick="this.value = ''"/>

                    <div id="exclui_paciente">
                        <form  role="form" id="frm_delete" name="frm_delete" >
                            <fieldset>
                                <legend>Excluindo o paciente</legend>
                                <input type="hidden" class="span1" name="id_paciente" id="id_paciente" value="" />
                                <div class="row" id="div-edit-paciente">

                                    
                                     <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="cpf">CPF</label>
                                            <input type="cpf"
                                                   class="form-control" id="cpf"
                                                   name="cpf"
                                                   readonly="">
                                        </div>
                                        <div class="form-group" >
                                            <label for="nome">Nome Completo</label>
                                            <input type="text" class="form-control"
                                                   id="nome"
                                                   name="nome"
                                                   readonly="">
                                        </div>
                                        <div class="form-group">
                                            <label for="estado_civil">Estado Cívil</label>
                                            <select class="form-control" id='estado_civil'
                                                    name='estado_civil'
                                                    readonly=""
                                                    >
                                                <option></option>
                                                <option value='Solteiro(a)'>Solteiro(a)</option>
                                                <option value='Casado(a)'>Casado(a)</option>
                                                <option value='Divorciado(a)'>Divorciado(a)</option>
                                                <option value='Viúvo(a)'>Viúvo(a)</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="email_paciente">E-mail do paciente</label>
                                            <input type="email" class="form-control"
                                                   id="email_paciente"
                                                   name="email_paciente"
                                                   readonly="">
                                        </div>
                                        <div class="form-group">
                                            <label for="uf">UF</label>
                                            <select class="form-control" name="uf" id="uf" tabindex="13" readonly="">
                                                <?
                                                foreach ($uf as $value) {

                                                    if ($value == "PE")
                                                        echo '<option value="' . $value . '" selected>' . $value . '</option>';
                                                    else
                                                        echo '<option value="' . $value . '">' . $value . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="sus">Cartão do SUS</label>
                                            <input type="text" class="form-control"
                                                   id="sus"
                                                   name="sus"
                                                   readonly="">
                                        </div>
                                        <div class="form-group">
                                            <label for="nascimento">Nascimento</label>
                                            <input type="text" class="form-control"
                                                   id="nascimento"
                                                   name="nascimento"
                                                   readonly="">
                                        </div>
                                        <div class="form-group">
                                            <label for="telefone">Telefone</label>
                                            <input type="text" class="form-control"
                                                   id="telefone"
                                                   name="telefone"
                                                   readonly="">
                                        </div>
                                        <div class="form-group">
                                            <label for="cep">CEP</label>
                                            <input type="text" class="form-control"
                                                   id="cep"
                                                   name="cep"
                                                   readonly="">
                                        </div>
                                        <div class="form-group">
                                            <label for="endereco">Rua, número e bairro</label>
                                            <textarea class="form-control" rows="3"
                                                      tabindex="14" id='endereco' required
                                                      name='endereco'
                                                      readonly="">
                                            </textarea>
                                        </div>


                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="titulo_eleitor">Título de Eleitor</label>
                                            <input type="text" class="form-control"
                                                   id="titulo_eleitor"
                                                   name="titulo_eleitor"
                                                   readonly="">
                                        </div>
                                        <div class="form-group">
                                            <label for="sexo">Sexo</label>
                                            <select class="form-control" id='sexo' name='sexo' tabindex="6"
                                                    readonly="">
                                                <option ></option>
                                                <option value='M'>Masculino</option>
                                                <option value='F'>Feminino</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="celular">Celular</label>
                                            <input type="text" class="form-control"
                                                   id="celular"
                                                   name="celular"
                                                   readonly="">
                                            <input type="hidden" name="operadora" id="operadora" value="" />
                                        </div>
                                        <div class="form-group">
                                            <label for="cidade">Cidade</label>
                                            <input type="text" class="form-control"
                                                   id="cidade"
                                                   name="cidade"
                                                   readonly="">
                                        </div>
                                        <div class="form-group">
                                            <label for="observacoes">Observação</label>
                                            <textarea class="form-control" rows="3"
                                                      tabindex="14" id='observacoes'
                                                      name='observacoes' readonly="">
                                            </textarea>
                                        </div>
                                    

                                    <div class="form-group" align="right">
                                        <button class="btn btn-danger" id="btn_edit" data-toggle="modal" data-target="#confirma"><span class="glyphicon glyphicon-floppy-remove"></span> Excluir</button>
                                    </div>
                                </div>

                                </div>

                            </fieldset>
                        </form>

                        <!-- Modal -->
                        <div class="modal fade" id="confirma" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Você está certo disto?</h4>
                                    </div>
                                    <div class="modal-body">
                                        Ao excluir este processo será irreversível, você realmente tem certeza disto?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
                                        <button type="button" id="btn_excluir" class="btn btn-danger">Tenho certeza e vou excluir</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<link href="<?= CSS ?>dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= CSS ?>TableTools.css" rel="stylesheet">
<link href="<?= CSS ?>typeahead.js-bootstrap.css" rel="stylesheet">
<?
$data['include'] = array("tab.js", "aba-lateral.js", "inputmask.js",
    "jquery.dataTables.min.js", "dataTables.bootstrap.js",
    "TableTools.min.js", "", "bootstrap3-typeahead.min.js", "paciente.js");
?>
<?php $this->load->view('layout/footer', $data); ?>

<script type="text/javascript">
                                var ajaxurl = "paciente/pega_pacientes";
</script>
<script type="text/javascript" src="<?= JS ?>listando-tabela.js"></script>


<div id="output"></div>