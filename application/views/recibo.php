<?php
$data['titulo'] = "Atendimento finalizado de " . $paciente["nome"];
$this->load->view('layout/header', $data);
?>

<link href="<?= CSS ?>tab-left.css" rel="stylesheet">

<div class="container" >
    <div class="row">
        <div class="pull-right icone">
            <img src="<?= IMG ?>atender_icone.jpg" alt="atender"/>
        </div>

        <div class="page-header">
            <h1>
                <?= $data['titulo'] ?>
                <div>
                    <small>Comprovante do atendimento.</small>
                </div>
            </h1>
        </div>
        <div align="center">
            <button id="imprimir" class="btn btn-info pull-center" name="imprimir" class="imprimir"><span class="glyphicon glyphicon-print"></span> Imprimir</button>
        </div>
        <br/>
        <fieldset id="outprint">
            <legend>Recibo</legend>
            <div id="uniclinic" class="row">
                <div class="col-xs-4">
                    <img src="<?= IMG ?>uniclinic-logo.jpg" /><br/>
                </div>
                <div class="col-xs-4">
                    <p> Rua Pedro José Rodrigues, s/n – Bairro Centro<br/>
                        Araripina Pernambuco<br/>
                        87 3873 2866<br/>
                        sac@uniclinicdoararipe.com.br<br/>
                        http://www.uniclinicdoararipe.com.br</p>

                </div>

                <div class="col-xs-3">
                    Convênio<br/> <strong><?= $convenio_nome ?></strong>
                    <br/><br/>
                    Atendimento registrado em <strong><?= $data_atendimento ?></strong>

                </div>
            </div>
            <fieldset>
                <legend>Paciente</legend>
                <div id="paciente" class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="paciente">Nome</label>
                            <p class="text-muted" id="paciente_nome"><?= $paciente["nome"] ?></p>
                            <input type="hidden" name="id_paciente" id="id_paciente" value="" />
                        </div>
                        <div class="form-group">
                            <label for="paciente">Cidade/UF</label>
                            <p class="text-muted" id="cidade_uf"><?= $paciente["lugar"] ?></p>
                        </div>
                        <div class="form-group">
                            <label for="paciente">Estado cívil</label>
                            <p class="text-muted" id="cidade_uf"><?= $paciente["estado_civil"] ?></p>
                        </div>

                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="paciente">Documento</label>
                            <p class="text-muted" id="documento_paciente"><?= $paciente["documento"] ?></p>
                        </div>
                        <div class="form-group">
                            <label for="paciente">Celular</label>
                            <p class="text-muted" id="celular"><?= $paciente["celular"] ?></p>
                        </div>
                        <div class="form-group">
                            <label for="paciente">Sexo</label>
                            <p class="text-muted" id="celular"><?= $paciente["sexo"] ?></p>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="paciente">Nascimento</label>
                            <p class="text-muted" id="nascimento_paciente"><?= $paciente["nascimento"] ?></p>
                        </div>
                        <div class="form-group">
                            <label for="paciente">Idade</label>
                            <p class="text-muted" id="idade"><?= $paciente["idade"] ?> anos</p>
                        </div>
                        <? if (!empty($paciente['observacoes'])) { ?>
                            <div class="form-group">
                                <label for="paciente">Observações</label>
                                <p class="text-muted" id="idade"><?= $paciente["observacoes"] ?></p>
                            </div>
                        <? } ?>
                    </div>
                </div>
            </fieldset>

            <?php if ($tipo == 'particular') { ?>

                <?php if (!empty($exames)) { ?>
                    <fieldset>
                        <legend>Exames</legend>
                        <div id="exame" class="row">
                            <table  width="90%" class="table table-hover table-condensed" id='exames' >
                                <thead>
                                    <tr>
                                        <th>Exame</th>
                                        <th>Valor</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($exames as $value) { ?>
                                        <tr>
                                            <td><?= $value['nome'] ?></td>
                                            <td><?= $value['valorf'] ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </fieldset>
                <?php } ?>

                <?php if (!empty($associados)) { ?>
                    <fieldset>
                        <legend>Atendimentos</legend>
                        <div id="associado" class="row">
                            <table   class="table table-hover table-condensed" id='associados' >
                                <thead>
                                    <tr>
                                        <th>Médico</th>
                                        <th>Especialidade</th>
                                        <th>Documento</th>
                                        <th>Valor</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($associados as $value) { ?>
                                        <tr>
                                            <td><?= $value['nome'] ?></td>
                                            <td><?= $value['especialidade'] ?></td>
                                            <td><?= $value['documento'] ?></td>
                                            <td><?= $value['valorf'] ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </fieldset>

                <?php } ?>
                <fieldset>
                    <legend>Valores</legend>
                    <div id="valores" class="row" >

                        <table  class="table table-hover table-condensed" id='valores' >
                            <thead>
                                <tr>
                                    <th>Atividade</th>
                                    <th>Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($s_exame)) { ?>
                                    <tr>
                                        <td>Exame</td>
                                        <td><?= $s_exame ?></td>
                                    </tr>
                                <?php } ?>
                                <?php if (!empty($s_associado)) { ?>
                                    <tr>
                                        <td>Atendimento</td>
                                        <td><?= $s_associado ?></td>
                                    </tr>
                                <?php } ?>
                                <?php if (!empty($desconto)) { ?>
                                    <tr class="danger">
                                        <td>Desconto</td>
                                        <td>- <?= $desconto ?></td>
                                    </tr>
                                <?php } ?>
                                <?php if (!empty($total)) { ?>
                                    <tr class="warning">
                                        <td>Total</td>
                                        <td><?= $total ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <div class="col-xs-7">
                            <strong>Forma de pagamento</strong>: <?= $forma_pagamento ?>
                        </div>
                    </div>
                </fieldset>

                <!-- Em convenio não exibe os valores cobrados -->
            <?php } else { ?>

                <?php if (!empty($exames)) { ?>
                    <fieldset>
                        <legend>Exames</legend>
                        <div id="exame" class="row">
                            <table  width="90%" class="table table-hover table-condensed" id='exames' >
                                <thead>
                                    <tr>
                                        <th>Exame</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($exames as $value) { ?>
                                        <tr>
                                            <td><?= $value['nome'] ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </fieldset>
                <?php } ?>

                <?php if (!empty($associados)) { ?>
                    <fieldset>
                        <legend>Atendimentos</legend>
                        <div id="associado" class="row">
                            <table   class="table table-hover table-condensed" id='associados' >
                                <thead>
                                    <tr>
                                        <th>Médico</th>
                                        <th>Especialidade</th>
                                        <th>Documento</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($associados as $value) { ?>
                                        <tr>
                                            <td><?= $value['nome'] ?></td>
                                            <td><?= $value['especialidade'] ?></td>
                                            <td><?= $value['documento'] ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </fieldset>

                <?php } ?>

            <?php } ?>
                <div class="mensagem">Anotações devem ser escritas no verso desta página.</div>
        </fieldset>

    </div>
</div>
<link href="<?= CSS ?>typeahead.js-bootstrap.css" rel="stylesheet">

<?
$data['include'] = array("printThis.js");
?>


<?php $this->load->view('layout/footer', $data); ?>

<script type="text/javascript">
    $("#imprimir").click(function() {
        $("#outprint").printThis({
            loadCSS: "<?= CSS ?>print.css",
            pageTitle: "<?= $paciente["nome"] ?>"
        });
    });

</script>

<div id="printMsg">Versão de impressão</div>