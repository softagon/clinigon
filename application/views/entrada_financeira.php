<?php
$data['titulo'] = "Entrada diária";
$this->load->view('layout/header', $data);
?>

<link href="<?= CSS ?>tab-left.css" rel="stylesheet">

<div class="container" >
    <div class="row">
        <div class="pull-right icone">
            <img src="<?= IMG ?>caixa_diario_icone.jpg" alt="atender"/>
        </div>

        <div class="page-header">
            <h1>
                <?= $data['titulo'] ?>
                <div>
                    <small>Entrada financeira de  <?= date("d/m/Y") ?> </small>
                </div>
            </h1>
        </div>
        <div class="row" align="center">
            <center>
                <div class=" offset3 " align="center">
                    Total comercializado hoje <?= $hoje['totalf'] ?>
                </div>
                <div class="offset3" algin="center">
                    Total comercializado ontem <?= $ontem['totalf'] ?>
                </div>
            </center>
        </div>
        <div class="row" id="tabela">
            <div  style="content: 1px; ">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered"  id="DT-tabela">
                    <thead>
                        <tr>
                            <th width="1%">ID</th>
                            <th width="25%">Nome</th>
                            <th width="5%">Documento</th>
                            <th>Lugar</th>
                            <th>Nascimento</th>
                            <th width="25%">Atendido por</th>
                            <th width="200px">Exame</th>
                            <th width="5%">Form Pagamento</th>
                             <th>Tipo</th>
                            <th>Quando</th>
                            <th width="9%">Total</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="dataTables_empty">Carregando dados do servidor</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>
</div>

<link href="<?= CSS ?>dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= CSS ?>TableTools.css" rel="stylesheet">
<?
$data['include'] = array("jquery.dataTables.min.js", "dataTables.bootstrap.js",
    "TableTools.min.js", );
?>
<?php $this->load->view('layout/footer', $data); ?>

<script type="text/javascript">
    var ajaxurl = "caixa/lista_hoje";
</script>
<script type="text/javascript" src="<?= JS ?>1coluna-tabela.js"></script>
