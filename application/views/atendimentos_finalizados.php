<?php
$data['titulo'] = "Atendimentos finalizados";
$this->load->view('layout/header', $data);
?>

<link href="<?= CSS ?>tab-left.css" rel="stylesheet">

<div class="container" >
    <div class="row">
        <div class="pull-right icone">
            <img src="<?= IMG ?>atendimentos_finalizados.jpg" alt="atender"/>
        </div>

        <div class="page-header">
            <h1>
                <?= $data['titulo'] ?>
                <div>
                    <small>[Atenção] - Dados atualizados a cada 15 minutos. </small>
                </div>
            </h1>
        </div>
        <div class="row">
            <p>
            <div  style="content: 1px; ">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered"  id="DT-tabela">
                    <thead>
                        <tr>
                            <th width="1%">ID</th>
                            <th width="20%">Nome</th>
                            <th>CPF</th>
                            <th>SUS</th>
                            <th>Título</th>
                            <th>Lugar</th>
                            <th>Nascimento</th>
                            <th>Atendido por</th>
                            <th width="200px">Exame</th>
                            <th>Quando</th>
                            <th width="100px">Total</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="dataTables_empty">Carregando dados do servidor</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="spacer"></div>
            </p>
        </div>


    </div>
</div>

<link href="<?= CSS ?>dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= CSS ?>TableTools.css" rel="stylesheet">
<link href="<?= CSS ?>typeahead.js-bootstrap.css" rel="stylesheet">
<?
$data['include'] = array("tab.js", "aba-lateral.js", "inputmask.js",
    "jquery.dataTables.min.js", "dataTables.bootstrap.js",
    "TableTools.min.js", "", "bootstrap3-typeahead.min.js", "dependente.js",
    "jquery.maskMoney.js");
?>
<?php $this->load->view('layout/footer', $data); ?>

<script type="text/javascript">
    var ajaxurl = "atender/atendimentos";
</script>
<script type="text/javascript" src="<?= JS ?>1coluna-tabela.js"></script>
