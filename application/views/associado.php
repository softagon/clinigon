<?php
$data['titulo'] = "Gestão de Associados";
$this->load->view('layout/header', $data);
?>

<link href="<?= CSS ?>tab-left.css" rel="stylesheet">

<div class="container">
    <div class="row">
        <div class="pull-right icone">
            <img src="<?= IMG ?>associado_icone.jpg" alt="associado"/>
        </div>

        <div class="page-header">
            <h1>
                <?= $data['titulo'] ?>
                <div>
                    <small>Adicione, edite ou remova seus associados.</small>
                </div>
            </h1>
        </div>
        <div class="tabbable tabs-left" id="tabs-693402">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#listar" data-toggle="tab">
                        <span class="glyphicon glyphicon-floppy-save"></span> Listar
                    </a>
                </li>
                <li>
                    <a href="#cadastrar" data-toggle="tab">
                        <span class="glyphicon glyphicon-floppy-open"></span> Cadastrar
                    </a>
                </li>
                <li>
                    <a href="#editar" data-toggle="tab">
                        <span class="glyphicon glyphicon-edit   "></span> Editar
                    </a>
                </li>
                <li>
                    <a href="#excluir" data-toggle="tab">
                        <span class="glyphicon glyphicon-floppy-remove"></span> Excluir
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="listar">
                    <p>
                        <?php if (is_array($associados)) { ?>
                        <div  style="padding-left: 160px; content: 1px; ">
                            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered"  id="DT-tabela">
                                <thead>
                                    <tr>    
                                        <th width="18%">Nome</th>
                                        <th width="10%">Documento</th>
                                        <th width="3%">E-mail</th>
                                        <th width="2%">Telefone</th>
                                        <th width="10%">Celular</th>
                                        <th width="2%">Comissão</th>
                                        <th width="5%">Especialidade</th>
                                        <th width="5%">Valor Particular</th>
                                        <th width="5%">Valor Convênio</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="dataTables_empty">Carregando dados do servidor</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="spacer"></div>

                    <?php } else {
                        ?>
                        <h3>Informação</h3><br/>
                        <span class="alert alert-info">
                            Ainda não possui associado cadastrado, que tal <a href="#cadastrar" onclick="abreTab('cadastrar', 19);" class="alert-link">cadastrar agora?</a>
                        </span>
                        <?php
                    }
                    ?>
                    </p>
                </div>
                <div class="tab-pane" id="cadastrar">
                    <form  role="form" id="frm_cad" name="frm_cad" method="post" action="<?= URL ?>associado/cadastra">
                        <fieldset>
                            <legend>Cadastre seu novo associado</legend>
                            <div class="alerta">
                                <!-- Error no cad associado -->
                                <div class="alert alert-warning alert-dismissable" id="cad-erro" style="display:none;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?= $this->cad_erro ?>
                                    <div class="btn-alerta">
                                        <button type="button" class="btn btn-warning" id="btn-tentar">Tentar novamente</button>
                                    </div>
                                </div>
                                <!-- Deu tudo certo -->
                                <div class="alert alert-success alert-dismissable" id="cad-ok" style="display:none;" >
                                    <button type="button" class="close">×</button>
                                    <?= $this->cad_ok ?>
                                    <div class="btn-alerta">
                                        <button type="button" class="btn btn-success" id="btn-cad-novo">Cadastrar outro?</button>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="div-cad-associado">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nome">Nome completo</label>
                                        <input type="text"
                                               class="form-control" id="nome"
                                               name="nome" required
                                               placeholder="Profissional de Saúde"
                                               title="Nome obrigatório"
                                               tabindex="1"
                                               maxlength="60">
                                    </div>
                                    <div class="form-group">
                                        <label for="telefone">Telefone</label>
                                        <input type="text" class="form-control"
                                               id="telefone"
                                               name="telefone"
                                               data-mask="(99)9999-9999" 
                                               maxlength="20"
                                               tabindex="4">
                                    </div>
                                    <div class="form-group">
                                        <label for="especialidade">Especialidade</label>
                                        <select  class="form-control" required
                                                 title="Escolha a função exercida" tabindex="7" id="id_especialidade_FK" name="id_especialidade_FK">
                                            <option></option>
                                            <?
                                            foreach ($especialidade as $value) {
                                                echo '<option value="' . $value['id_especialidade'] . '">' . $value['especialidade'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>



                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="documento">Documento</label>
                                        <input type="text" class="form-control"
                                               id="documento"
                                               name="documento"
                                               required 
                                               placeholder="CRM, CRO, CRP.."
                                               title="Apenas números"
                                               tabindex="2"
                                               maxlength="20">
                                    </div>
                                    <div class="form-group">
                                        <label for="celular">Celular</label>
                                        <input type="text" class="form-control"
                                               id="celular"
                                               name="celular"
                                               required
                                               data-mask='(99)9999-9999*'
                                               title="Em caso de emergência"
                                               tabindex="5"
                                               maxlength="20">
                                        <input type="hidden" name="operadora" id="operadora" value="" />
                                    </div>
                                    <div class="form-group">
                                        <label for="valor">Valor particular</label>
                                        <input type="text" class="form-control"
                                               id="valor"
                                               name="valor"
                                               required 
                                               title="Valor que este profissional cobra"
                                               tabindex="8"
                                               maxlength="20">
                                    </div>
                                                                        
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email_associado">E-mail</label>
                                        <input type="email" class="form-control"
                                               id="email_associado" name="email_associado" required
                                               placeholder="Para comunicados."
                                               title="Use um e-mail válido"
                                               tabindex="3">
                                    </div>

                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="comissao">Comissão cobrada deste associado</label>
                                            <input type="number" class="form-control"
                                                   id="comissao" name="comissao" required
                                                   title="Use algo entre 0 e 100, apenas números."
                                                   size="2" min="0" max="100"
                                                   placeholder="Em porcentagem, 5% por exemplo. "
                                                   tabindex="6">
                                        </div>
                                    <div class="form-group">
                                        <label for="valor_convenio">Valor convênio</label>
                                        <input type="text" class="form-control"
                                               id="valor_convenio"
                                               name="valor_convenio"
                                               title="Valor que este profissional cobra pelo convênio"
                                               tabindex="9"
                                               maxlength="20">
                                    </div>
                                    </div>
                                        
                                    <div class="form-group" align="right">
                                        <button class="btn btn-primary" id="cad_associado" tabindex="8"><span class="glyphicon glyphicon-floppy-disk"></span> Cadastrar</button>
                                    </div>
                                </div>

                            </div>
                        </fieldset>
                    </form>

                </div>
                <!--- Editando utilizando typeaHead.js -->
                <div class="tab-pane" id="editar">
                    <p>
                    <legend>Primeiro busque o associado que deseja editar</legend>
                    <input id="busca-editar"
                           name="busca-editar"
                           type="text"
                           placeholder="Digite o nome do associado" autocomplete="off"
                           spellcheck="false" dir="auto"
                           class="form-control" data-items="12"
                           data-provide="typeahead" style="width: 50%;"
                           onclick="this.value = ''"/>
                    <!-- form para edição do associado -->
                    <div class="alerta">
                        <!-- Error no cad associado -->
                        <div class="alert alert-warning alert-dismissable" id="erro_edit" style="display:none;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?= $this->cad_erro ?>
                            <div class="btn-alerta">
                                <button type="button" class="btn btn-warning" id="btn_tentar_edit">Tentar novamente</button>
                            </div>
                        </div>
                        <!-- Deu tudo certo -->
                        <div class="alert alert-success alert-dismissable" id="ok_edit" style="display:none;" >
                            <button type="button" class="close">×</button>
                            <?= $this->cad_ok ?>
                        </div>
                    </div>
                    <div id="modifica-associado">
                        <form  role="form" id="frm_edit" name="frm_edit" method="post" action="<?= URL ?>associado/modifica">
                            <fieldset>
                                <legend>Modifique o associado</legend>
                                <div class="alerta">
                                    <!-- Error no cad associado -->
                                    <div class="alert alert-warning alert-dismissable" id="cad-erro" style="display:none;">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <?= $this->cad_erro ?>
                                        <div class="btn-alerta">
                                            <button type="button" class="btn btn-warning" id="btn-tentar">Tentar novamente</button>
                                        </div>
                                    </div>
                                    <!-- Deu tudo certo -->
                                    <div class="alert alert-success alert-dismissable" id="cad-ok" style="display:none;" >
                                        <button type="button" class="close">×</button>
                                        <?= $this->cad_ok ?>
                                        <div class="btn-alerta">
                                            <button type="button" class="btn btn-success" id="btn-cad-novo">Cadastrar outro?</button>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" class="span1" name="idassociado" id="idassociado" value="" />

                                <div class="row" id="div-cad-associado">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nome">Nome completo</label>
                                            <input type="text"
                                                   class="form-control" id="nome"
                                                   name="nome" required
                                                   placeholder="Profissional de Saúde"
                                                   title="Nome obrigatório"
                                                   tabindex="1"
                                                   maxlength="60">
                                        </div>
                                        <div class="form-group">
                                            <label for="telefone">Telefone</label>
                                            <input type="text" class="form-control"
                                                   id="telefone"
                                                   name="telefone"
                                                   data-mask="(99)9999-9999" 
                                                   maxlength="20"
                                                   tabindex="4">
                                        </div>
                                        <div class="form-group">
                                            <label for="especialidade">Especialidade</label>
                                            <select  class="form-control" required
                                                     title="Escolha a função exercida" tabindex="7" id="id_especialidade_FK" name="id_especialidade_FK">
                                                <option></option>
                                                <?
                                                foreach ($especialidade as $value) {
                                                    echo '<option value="' . $value['id_especialidade'] . '">' . $value['especialidade'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>


                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="documento">Documento</label>
                                            <input type="text" class="form-control"
                                                   id="documento"
                                                   name="documento"
                                                   required 
                                                   placeholder="CRM, CRO, CRP.."
                                                   title="Apenas números"
                                                   tabindex="2"
                                                   maxlength="20">
                                        </div>
                                        <div class="form-group">
                                            <label for="celular">Celular</label>
                                            <input type="text" class="form-control"
                                                   id="celular"
                                                   name="celular"
                                                   required 
                                                   data-mask='(99)9999-9999*'
                                                   title="Em caso de emergência"
                                                   tabindex="5"
                                                   maxlength="20">   
                                            <input type="hidden" name="operadora" id="operadora" value="" />
                                        </div>
                                        <div class="form-group">
                                            <label for="valor">Valor cobrado</label>
                                            <input type="text" class="form-control"
                                                   id="valor"
                                                   name="valor"
                                                   required 
                                                   title="Valor que este profissional cobra"
                                                   tabindex="8"
                                                   maxlength="20">
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="email_associado">E-mail</label>
                                            <input type="email" class="form-control"
                                                   id="email_associado" name="email_associado" required
                                                   placeholder="Para comunicados."
                                                   title="Use um e-mail válido"
                                                   tabindex="3">
                                        </div>

                                        <div class="form-group">
                                            <label for="comissao">Comissão cobrada deste associado</label>
                                            <input type="number" class="form-control"
                                                   id="comissao" name="comissao" required
                                                   title="Use algo entre 0 e 100, apenas números."
                                                   size="2" min="0" max="100"
                                                   placeholder="Em porcentagem, 5% por exemplo. "
                                                   tabindex="6">
                                        </div>
                                        <div class="form-group">
                                            <label for="valor_convenio">Valor convênio</label>
                                            <input type="text" class="form-control"
                                                   id="valor_convenio"
                                                   name="valor_convenio"
                                                   required 
                                                   title="Valor que este profissional cobra pelo convênio"
                                                   tabindex="9"
                                                   maxlength="20">
                                        </div>
                                        
                                        <div class="form-group" align="right">
                                            <button class="btn btn-primary" id="btn_edit"><span class="glyphicon glyphicon-edit"></span> Editar</button>
                                        </div>
                                    </div>

                                </div>
                            </fieldset>
                        </form>
                    </div>


                    </p>
                </div>
                <div class="tab-pane" id="excluir">
                    <p>
                    <legend>Primeiro busque o associado que deseja EXCLUIR</legend>
                    <input id="busca_excluir"
                           name="busca_excluir"
                           type="text"
                           placeholder="Digite o nome do associado" autocomplete="off"
                           spellcheck="false" dir="auto"
                           class="form-control" data-items="12"
                           data-provide="typeahead" style="width: 50%;"
                           onclick="this.value = ''"/>

                    <div id="exclui_associado">
                        <form  role="form" id="frm_delete" name="frm_delete" >
                            <fieldset>
                                <legend>Excluindo o associado</legend>
                                <input type="hidden" class="span1" name="id_associado" id="id_associado" value="" />
                                <div class="row" id="div-cad-associado">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nome">Nome completo</label>
                                            <input type="text"
                                                   class="form-control" id="nome"
                                                   name="nome" required
                                                   readonly=""
                                                   maxlength="60">
                                        </div>
                                        <div class="form-group">
                                            <label for="telefone">Telefone</label>
                                            <input type="text" class="form-control"
                                                   id="telefone"
                                                   name="telefone"
                                                   readonly="">
                                        </div>
                                        <div class="form-group">
                                            <label for="especialidade">Especialidade</label>
                                            <select  class="form-control" readonly id="id_especialidade_FK" name="id_especialidade_FK">
                                                <option></option>
                                                <?
                                                foreach ($especialidade as $value) {
                                                    echo '<option value="' . $value['id_especialidade'] . '">' . $value['especialidade'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>


                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="documento">Documento</label>
                                            <input type="number" class="form-control"
                                                   id="documento"
                                                   name="documento"
                                                   required 
                                                   readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="celular">Celular</label>
                                            <input type="text" class="form-control"
                                                   id="celular"
                                                   name="celular"
                                                   readonly>   
                                        </div>
                                        <div class="form-group">
                                            <label for="valor">Valor cobrado</label>
                                            <input type="text" class="form-control"
                                                   id="valor"
                                                   name="valor"
                                                   readonly=""
                                                   tabindex="8"
                                                   >
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="email_associado">E-mail</label>
                                            <input type="email" class="form-control"
                                                   id="email_associado" name="email_associado" required
                                                   readonly>
                                        </div>

                                        <div class="form-group">
                                            <label for="comissao">Comissão cobrada deste associado</label>
                                            <input type="number" class="form-control"
                                                   id="comissao" name="comissao" required
                                                   readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="valor_convenio">Valor convênio</label>
                                            <input type="text" class="form-control"
                                                   id="valor_convenio"
                                                   name="valor_convenio"
                                                   readonly=""
                                                   tabindex="9"
                                                   >
                                        </div>
                                        <div class="form-group" align="right">
                                            <button class="btn btn-danger" id="btn_edit" data-toggle="modal" data-target="#confirma"><span class="glyphicon glyphicon-floppy-remove"></span> Excluir</button>
                                        </div>
                                    </div>

                                </div>
                            </fieldset>
                        </form>

                        <!-- Modal -->
                        <div class="modal fade" id="confirma" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">Você está certo disto?</h4>
                                    </div>
                                    <div class="modal-body">
                                        Ao excluir este processo será irreversível, você realmente tem certeza disto?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
                                        <button type="button" id="btn_excluir" class="btn btn-danger">Tenho certeza e vou excluir</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<link href="<?= CSS ?>dataTables.bootstrap.css" rel="stylesheet">
<link href="<?= CSS ?>TableTools.css" rel="stylesheet">
<link href="<?= CSS ?>typeahead.js-bootstrap.css" rel="stylesheet">
<?
$data['include'] = array("tab.js", "aba-lateral.js", "inputmask.js",
    "jquery.dataTables.min.js", "dataTables.bootstrap.js",
    "TableTools.min.js", "", "bootstrap3-typeahead.min.js", "associado.js",
    "jquery.maskMoney.js");
?>
<?php $this->load->view('layout/footer', $data); ?>

<script type="text/javascript">
                                var ajaxurl = "associado/pega_associados";
</script>
<script type="text/javascript" src="<?= JS ?>listando-tabela.js"></script>


