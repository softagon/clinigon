<?php
$data['titulo'] = "Iniciar atendimento";
$this->load->view('layout/header', $data);
?>

<link href="<?= CSS ?>tab-left.css" rel="stylesheet">

<div class="container">
    <div class="row">
        <div class="pull-right icone">
            <img src="<?= IMG ?>atender_icone.jpg" alt="atender"/>
        </div>

        <div class="page-header">
            <h1>
                <?= $data['titulo'] ?>
                <div>
                    <small>Aqui a mágica acontece.</small>
                </div>
            </h1>
        </div>

        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a id="passo01" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            Passo 01, defina o paciente
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="disabledTextInput">Encontre o paciente</label>
                            <input type="text" class="form-control input-lg" 
                                   id="busca-paciente" placeholder="Paciente, documento"
                                   onclick="this.value = ''">
                        </div>
                        <div class="row" id="paciente_dados">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="paciente">Nome</label>
                                    <p class="text-muted" id="paciente_nome"></p>
                                    <input type="hidden" name="id_paciente" id="id_paciente" value="" />
                                </div>
                                <div class="form-group">
                                    <label for="paciente">Cidade/UF</label>
                                    <p class="text-muted" id="cidade_uf"></p>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="paciente">Documento</label>
                                    <p class="text-muted" id="documento_paciente"></p>
                                </div>
                                <div class="form-group">
                                    <label for="paciente">Celular</label>
                                    <p class="text-muted" id="celular"></p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="paciente">Nascimento</label>
                                    <p class="text-muted" id="nascimento_paciente"></p>
                                </div>
                                <div class="form-group">
                                    <label for="paciente">Idade</label>
                                    <p class="text-muted" id="idade"></p>
                                </div>
                            </div>
                        </div>
                        <div id="dependente">
                            <div class="alert alert-info">O atendimento será com este paciente ou com algum dependente menor de idade?</div>
                            <div class="col-md-2">
                                <a href="#" id="este_paciente" type="button" class="btn btn-primary btn-lg">Este paciente</a>
                            </div>
<!--                            <div class="col-md-2">
                                <a href="#" id="menor_depentende" type="button" class="btn btn-warning btn-lg">Menor dependente</a>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a id="passo02" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                            Passo 02, particular ou convênio?
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="panel-body">

                        <h4>O cliente pode preferir pagar à vista no caso particular ou utilizar um convênio de alguma Empresa parceira da Uniclinic</h4>
                        <br/>
                        <div class="row">
                            <div class="col-md-2" id="particular-div"  data-toggle="collapse">
                                <button type="button" class="btn btn-info btn-lg" data-toggle="button"  id="particular">
                                    Particular
                                </button>
                            </div>
                            <div class="col-md-1" id="convenio-div" data-toggle="collapse">
                                <button type="button" class="btn btn-warning btn-lg" data-toggle="button"  id="convenio">
                                    Convênio
                                </button>
                            </div>
                        </div>
                        <br/>
                        <div class="row" id="busca-convenio">
                            <div class="form-group col-md-6" >
                                <label for="disabledTextInput">Busque o convênio que será utilizado</label>
                                <input type="text" class="form-control input-lg"  
                                       id="busca_convenio" placeholder="Nome, responsável ou CNPJ"
                                       onclick="this.value = ''">
                                <div id="remove_espaco"><br/><br/><br/><br/></div>
                            </div>
                        </div>
                        
                        <div class="row" id="convenio_dados">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="convenio_nome">Nome</label>
                                    <p class="text-muted" id="convenio_nome"></p>
                                    
                                </div>
                                <div class="form-group">
                                    <label for="convenio_cidade_uf">Cidade/UF</label>
                                    <p class="text-muted" id="convenio_cidade_uf"></p>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="convenio_telefone">Telefone</label>
                                    <p class="text-muted" id="convenio_telefone"></p>
                                </div>
                                <div class="form-group">
                                    <label for="convenio_responsavel">Responsável/Contato</label>
                                    <p class="text-muted" id="convenio_responsavel"></p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group"></div>
                                <div class="form-group">
                                <button class="btn btn-primary" id="confirmar_convenio">Confirmar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="log" name="log"></div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a id="passo03" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        Passo 03, registre o atendimento
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row" id="paciente2_dados">

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="paciente2">Nome</label>
                                <p class="text-muted" id="paciente2_nome"></p>
                                <input type="hidden" name="id_paciente2" id="id_paciente2" value="" />
                            </div>
                            <div class="form-group">
                                <label for="paciente2">Cidade/UF</label>
                                <p class="text-muted" id="paciente2_cidade_uf"></p>
                            </div>

                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="paciente2">Documento</label>
                                <p class="text-muted" id="documento_paciente2"></p>
                            </div>
                            <div class="form-group">
                                <label for="paciente2">Celular</label>
                                <p class="text-muted" id="paciente2_celular"></p>
                            </div>
                        </div>
                    </div>
                    <div class="bs-example bs-example-tabs">
                        <ul id="myTab" class="nav nav-tabs">
                            <li class="active"><a href="#associado" data-toggle="tab">Médico (associado)</a></li>
                            <li><a href="#exames" data-toggle="tab">Exames</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade in active atende-tab" id="associado">
                                <div class="form-group">
                                    <label for="disabledTextInput">Qual o associado?</label>
                                    <input type="text" class="form-control input-lg" 
                                           id="busca_associado" placeholder="Escolha o médico, nome ou documento"
                                           onclick="this.value = ''">
                                </div>

                                <div class='row'>
                                    <div class="col-md-10" id='associados' align='center'>

                                        <table class="table table-hover table-condensed" id='associado_dados'>
                                            <thead>
                                                <tr>
                                                    <th>Associado</th>
                                                    <th>Documento</th>
                                                    <th>Especialidade</th>
                                                    <th>Valor</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade  atende-tab" id="exames">
                                <div class="form-group">
                                    <label for="disabledTextInput">Qual o exame?</label>
                                    <input type="text" class="form-control input-lg" 
                                           id="busca_exame" placeholder="Escolha o médico, nome ou documento"
                                           onclick="this.value = ''">
                                </div>

                                <div class='row'>
                                    <div class="col-md-10" id='exames' align='center'>

                                        <table class="table table-hover table-condensed" id='exame_dados'>
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Exame</th>
                                                    <th>Valor</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div><!-- /example -->
                    <form action="<?= URL ?>atender/finalizando" method="post">
                        <input type="hidden" name="id_convenio" id="id_convenio" value="" />
                        <div class="row" id="subtotal-div" data-toggle="modal">
                            <div class="col-md-6">
                                <fieldset>
                                    <legend>SUBTOTAL</legend>
                                    <div class="form-group">
                                        <label for="subtotal" class="col-sm-6 control-label">Valor do atendimento</label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control" id="subtotal" name="subtotal" value="">
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-group">
                                    <label for="forma_pagamento">Forma de pagamento</label>
                                    <select  class="form-control" required
                                             title="Escolha a função exercida" tabindex="7" id="id_forma_pagamento_FK" name="id_forma_pagamento_FK">
                                        <option></option>
                                        <?
                                        foreach ($formas as $value) {
                                            echo '<option value="' . $value['id_forma_pagamento'] . '">' . $value['nome'] . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row" style="padding: 20px;">
                            <div class="form-group" align="right">
                                <button  class="btn btn-primary" id="finalizando"><span class="glyphicon glyphicon-floppy-disk"></span> Finalizar atendimento</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>


</div>
</div>
<link href="<?= CSS ?>TableTools.css" rel="stylesheet">
<link href="<?= CSS ?>typeahead.js-bootstrap.css" rel="stylesheet">
<?
$data['include'] = array("tab.js", "aba-lateral.js", "inputmask.js",
    "jquery.dataTables.min.js", "dataTables.bootstrap.js",
    "TableTools.min.js", "", "bootstrap3-typeahead.min.js", "atender.js",
    "jquery.maskMoney.js", "atender_associado.js", "atender_exame.js", "atender_convenio.js");
?>
<?php $this->load->view('layout/footer', $data); ?>



