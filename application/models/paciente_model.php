<?php

class Paciente_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->clinica = 1;
    }

    /*
     * @return Testar se retorna um array
     */

    function pega_pacientes() {

        $query = $this->db->get('paciente');
        if ($query->num_rows() > 0) {
            return $query->row_array(); #BUG BUG BUG 
        }
        else
            return false;
    }

    function pega_nivel() {
        $this->db->order_by("nivel", "asc");
        $query = $this->db->get('paciente_nivel');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        else
            return false;
    }

    //Pode ser que já seja um associado
    function pega_dados($email) {

        $this->db->select("nome");
        $query = $this->db->get_where('associado', array('email_associado' => $email,
            'ativo' => 's'));
        if ($query->num_rows() > 0) {
            return $query->row_array(); #BUG BUG BUG 
        }
        else
            return null;
    }

    function gravar($post) {
        if (is_array($post)) {
            $this->load->helper("formatar_helper");
            $post['nascimento'] = data_americana($post['nascimento']);
            $post['celular'] = formata_celular($post['celular']);

            $res = $this->db->insert('paciente', $post);
            if (!$res)
                return false;
            else
                return true;
        }
        else
            return false;
    }

    function pega_asssociado($email) {
        $query = $this->db->get_where('associado', array('email_associado' => $email,
            'ativo' => 's'));
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id_associado;
        }
        else
            return null;
    }

    function atualizar($_POST) {
        if (is_array($_POST)) {
            $id = $_POST['idpaciente'];
            unset($_POST['idpaciente']);

            $this->load->helper("formatar_helper");
            $_POST['nascimento'] = data_americana($_POST['nascimento']);
            $_POST['celular'] = formata_celular($_POST['celular']);

            if (!$this->db->update('paciente', $_POST, array('id_paciente' => $id))) {
                return false;
            }
            else
                return true;
        }
        else
            return false;
    }

    function apagar($id) {
        $data['ativo'] = "n";
        if (!$this->db->update('paciente', $data, array('id_paciente' => $id)))
            return false;
        else
            return true;
    }

    function tem_email($email) {
        $query = $this->db->get_where('paciente', array('email_paciente' => $email, 'ativo' => 's'));
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function tem_cpf($cpf) {
        $query = $this->db->get_where('paciente', array('cpf' => $cpf, 'ativo' => 's'));
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function tem_sus($sus) {
        $query = $this->db->get_where('paciente', array('sus' => $sus, 'ativo' => 's'));
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function tem_titulo($titulo) {
        $query = $this->db->get_where('paciente', array('titulo_eleitor' => $titulo, 'ativo' => 's'));
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function busca($query) {
        $this->db->select('id_paciente, nome, email_paciente, titulo_eleitor, cpf, sus');
        $this->db->like('nome', $query);
        $this->db->or_like('titulo_eleitor', $query);
        $this->db->or_like('cpf', $query);
        $this->db->or_like('sus', $query);
        $this->db->limit(10);
        $this->db->where("ativo", "s");
        $query = $this->db->get('paciente');
        if ($query->num_rows() > 0) {
            $x = 0;
            foreach ($query->result() as $row) {
                $data[$x]['id'] = $row->id_paciente;


                if (!empty($row->titulo_eleitor))
                    $documento = $row->titulo_eleitor;

                if (!empty($row->cpf))
                    $documento = $row->cpf;

                if (!empty($row->sus))
                    $documento = $row->sus;

                $data[$x]['document'] = $documento;

                $data[$x]['name'] = $row->nome . ' - ' . $documento . ' - ' . $row->email_paciente;

                $x++;
            }
            return $data;
        }
        else
            return null;
    }

    function por_id($id) {
        $sql = "SELECT *, 
            DATE_FORMAT( NOW( ), '%Y' ) - DATE_FORMAT( nascimento, '%Y' ) - 
            ( DATE_FORMAT( NOW( ), '00-%m-%d') < DATE_FORMAT( nascimento, '00-%m-%d' ) ) AS idade
            FROM (`paciente`)
            WHERE `id_paciente` =  '" . $id . "'";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row();

            $this->load->helper("formatar_helper");
            $data['nascimento'] = data_brasil($row->nascimento);
            $data['nome'] = $row->nome;
            $data['email_paciente'] = $row->email_paciente;
            $data['sexo'] = $row->sexo;
            $data['cidade'] = $row->cidade;
            $data['endereco'] = $row->endereco;
            $data['cep'] = $row->cep;
            $data['uf'] = $row->UF;
            $data['telefone'] = $row->telefone;
            $data['celular'] = $row->celular;
            $data['operadora'] = $row->operadora;
            $data['estado_civil'] = $row->estado_civil;
            $data['cpf'] = $row->cpf;
            $data['sus'] = $row->sus;
            $data['titulo_eleitor'] = $row->titulo_eleitor;
            $data['observacoes'] = $row->observacoes;
            $data['idade'] = $row->idade;

            if (!empty($row->titulo_eleitor))
                $documento = $row->titulo_eleitor;

            if (!empty($row->cpf))
                $documento = $row->cpf;

            if (!empty($row->sus))
                $documento = $row->sus;

            $data["documento"] = $documento;

            return $data;
        }
        else
            return null;
    }

    function por_atendimento($id_atendimento) {
        $sql = "SELECT *, DATE_FORMAT( NOW( ), '%Y' ) - DATE_FORMAT( nascimento, '%Y' ) - 
            ( DATE_FORMAT( NOW( ), '00-%m-%d') < DATE_FORMAT( nascimento, '00-%m-%d' ) ) AS idade
            FROM paciente AS p, atendimento AS at
            WHERE p.id_paciente =  at.id_paciente_FK
            AND at.id_atendimento = '".$id_atendimento."' ";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row();

            $this->load->helper("formatar_helper");
            $data['nascimento'] = data_brasil($row->nascimento);
            $data['nome'] = $row->nome;
            $data['email_paciente'] = $row->email_paciente;
            $data['sexo'] = ($row->sexo >= "F") ? "Feminino" : "Masculino";
            $data['lugar'] = $row->cidade."/".$row->UF;
            $data['endereco'] = $row->endereco;
            $data['cep'] = $row->cep;
            $data['telefone'] = $row->telefone;
            $data['celular'] = $row->celular." - ".$row->operadora;
            $data['operadora'] = $row->operadora;
            $data['estado_civil'] = $row->estado_civil;
            $data['cpf'] = $row->cpf;
            $data['sus'] = $row->sus;
            $data['titulo_eleitor'] = $row->titulo_eleitor;
            $data['observacoes'] = $row->observacoes;
            $data['idade'] = $row->idade;

            if (!empty($row->titulo_eleitor))
                
            $documento = $row->titulo_eleitor;

            if (!empty($row->cpf))
                
            $documento = $row->cpf;

            if (!empty($row->sus))
                
            $documento = $row->sus;

            $data["documento"] = $documento;

            return $data;
        }
        else
            return null;
    }

}

?>