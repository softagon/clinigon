<?php

class Associado_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->clinica = 1;
    }

    /*
     * @return Testar se retorna um array
     */

    function pega_associados() {

        $query = $this->db->get('associado');
        if ($query->num_rows() > 0) {
            return $query->row_array(); #BUG BUG BUG 
        }
        else
            return false;
    }

    function pega_funcoes() {
        $this->db->order_by("especialidade", "asc");
        $query = $this->db->get_where('especialidade', array('ativo' => 's'));
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        else
            return false;
    }

    function gravar($post) {
        if (is_array($post)) {
            $this->load->helper("formatar_helper");
            $post['id_clinica_FK'] = $this->clinica;

            $post['valor'] = dinheiro($post['valor']);
            $post['celular'] = formata_celular($post['celular']);

            $res = $this->db->insert('associado', $post);
            if (!$res) {
                return false;
            } else {
                return true;
            }
        }
        else
            return false;
    }

    function atualizar($_POST) {
        if (is_array($_POST)) {
            $this->load->helper("formatar_helper");
            $data['nome'] = $_POST['nome'];
            $data['documento'] = $_POST['documento'];
            $data['email_associado'] = $_POST['email_associado'];
            $data['telefone'] = $_POST['telefone'];
            $data['comissao'] = $_POST['comissao'];
            $data['id_especialidade_FK'] = $_POST['id_especialidade_FK'];
            $data['operadora'] = $_POST['operadora'];
            $data['valor'] = dinheiro($_POST['valor']);
            $data['valor_convenio'] = dinheiro($_POST['valor_convenio']);
            $data['celular'] = formata_celular($_POST['celular']);

            if (!$this->db->update('associado', $data, array('id_associado' => $_POST['idassociado']))) {
                return false;
            }
            else
                return true;
        }
        else
            return false;
    }

    function apagar($id) {
        $data['ativo'] = "n";
        if (!$this->db->update('associado', $data, array('id_associado' => $id)))
            return false;
        else
            return true;
    }

    function tem_documento($documento) {
        $query = $this->db->get_where('associado', array('documento' => $documento, 'ativo' => 's'));
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function tem_email($email) {
        $query = $this->db->get_where('associado', array('email_associado' => $email, 'ativo' => 's'));
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function busca($query) {
        $this->db->select('id_associado, nome, documento, email_associado, celular');
        $this->db->like('nome', $query);
        $this->db->or_like('documento', $query);
        $this->db->limit(10);
        $this->db->where("ativo", "s");
        $query = $this->db->get('associado');
        if ($query->num_rows() > 0) {
            $x = 0;
            foreach ($query->result() as $row) {
                $data[$x]['id'] = $row->id_associado;
                $data[$x]['name'] = $row->nome . ' - ' . $row->documento;

                $x++;
            }
            return $data;
        }
        else
            return null;
    }

    function por_id($id) {
        $query = $this->db->get_where('associado', array('id_associado' => $id, "ativo" => "s"));
        if ($query->num_rows() > 0) {
            $this->load->helper("formatar_helper");
            
            foreach ($query->result() as $row) {
                $data['id'] = $row->id_associado;
                $data['nome'] = $row->nome;
                $data['documento'] = $row->documento;
                $data['email_associado'] = $row->email_associado;
                $data['telefone'] = $row->telefone;
                $data['celular'] = $row->celular;
                $data['comissao'] = $row->comissao;
                $data['id_especialidade_FK'] = $row->id_especialidade_FK;
                $data['valor'] = $row->valor;
                $data['valorf'] = real_brasileiro($row->valor);
                $data['valor_convenio'] = $row->valor_convenio;
                $data['valor_conveniof'] = real_brasileiro($row->valor_convenio);
                $data['especialidade'] = $this->pega_especialidade($row->id_especialidade_FK);
            }
            return $data;
        }
        else
            return null;
    }
    
    function pega_especialidade($id_especialidade_FK){
         $query = $this->db->get_where('especialidade', array('id_especialidade' => $id_especialidade_FK, "ativo" => "s"));
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                return $row->especialidade;
            }
        }
        else
            return null;
    }

}

?>