<?php

class Util_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /*
     * @cep Recebe o cep sem pontos ou espaços
     */

    function pega_endereco($cep) {

        if (!empty($cep)) {
            $sql = "SELECT cep, endereco, cidade, uf, bairro FROM 
                    cepbr_endereco AS end, 
                    cepbr_cidade AS cid, 
                    cepbr_bairro as bai 
                    WHERE cep = '" . $cep . "'
                    AND end.id_cidade = cid.id_cidade
                    AND end.id_bairro = bai.id_bairro
                    ORDER BY bairro DESC
                    ";
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                return $query->row_array();
            }
            else
                return null;
        }
        else
            return null;
    }


}

?>