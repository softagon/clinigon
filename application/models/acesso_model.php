<?php

/**
 * Gerência dos dados da Empresa 
 * ao banco de dados
 *
 * @author Hermes Alves
 */
class Acesso_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function senha_bate($data) {

        if (!empty($data['email_usuario'])) {
            $query = $this->db->get_where('usuario', 
                    array('email_usuario' => $data['email_usuario'], 'ativo' => 's'));
            if (is_object($query) AND $query->num_rows() > 0) {
                $row = $query->row();
                $senha = $row->senha;

                if ($senha == $data['senha'])
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        else
            return false;
    }

    function get_id($data) {

        $query = $this->db->get_where('usuario', array('email_usuario' => $data['email_usuario']));
        if ($query->num_rows() > 0) {
            $this->load->helper("formatar_helper");
            $row = $query->row();
            $dados['id_usuario'] = $row->id_usuario;
            $dados['nome'] = $row->nome;
            $a = explode(" ", $row->nome);
            $dados['primeiro_nome'] = $a[0];
            $dados['id_usuario_nivel_FK'] = $row->id_usuario_nivel_FK;
            $dados['ultima_visita'] = data_brasil($row->visitou);
            
            $data['visitou'] = date("Y-m-d H:i:s");
            $this->db->update('usuario', $data, array('id_usuario' => $row->id_usuario));
            
            return $dados;
        }
        else
            return null;
    }

    

}

?>
