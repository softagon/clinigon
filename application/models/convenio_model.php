<?php

class Convenio_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->clinica = 1;
    }

    /*
     * @return Testar se retorna um array
     */

    function pega_convenios() {

        $query = $this->db->get('convenio');
        if ($query->num_rows() > 0) {
            return $query->row_array(); #BUG BUG BUG 
        }
        else
            return false;
    }

    function pega_funcoes() {
        $this->db->order_by("especialidade", "asc");
        $query = $this->db->get_where('especialidade', array('ativo' => 's'));
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        else
            return false;
    }

    function gravar($post) {
        if (is_array($post)) {
            $this->load->helper("formatar_helper");

            $post['telefone'] = formata_celular($post['telefone']);

            $res = $this->db->insert('convenio', $post);
            if (!$res) {
                return false;
            } else {
                return true;
            }
        }
        else
            return false;
    }

    function atualizar($_POST) {
        if (is_array($_POST)) {
            $this->load->helper("formatar_helper");
            $data['nome'] = $_POST['nome'];
            $data['cnpj'] = $_POST['cnpj'];
            $data['cidade'] = $_POST['cidade'];
            $data['uf'] = $_POST['uf'];
            $data['telefone'] = formata_celular($_POST['telefone']);
            $data['responsavel'] = $_POST['responsavel'];

            if (!$this->db->update('convenio', $data, array('id_convenio' => $_POST['idconvenio']))) {
                return false;
            }
            else
                return true;
        }
        else
            return false;
    }

    function apagar($id) {
        $data['ativo'] = "n";
        if (!$this->db->update('convenio', $data, array('id_convenio' => $id)))
            return false;
        else
            return true;
    }

    function busca($query) {
        $this->db->select('id_convenio, nome, cnpj, responsavel, telefone');
        $this->db->like('nome', $query);
        $this->db->or_like('responsavel', $query);
        $this->db->or_like('cnpj', $query);
        $this->db->limit(10);
        $this->db->where("ativo", "s");
        $query = $this->db->get('convenio');
        if ($query->num_rows() > 0) {
            $x = 0;
            foreach ($query->result() as $row) {
                $data[$x]['id'] = $row->id_convenio;
                $data[$x]['name'] = $row->nome . ' - ' . $row->telefone . ' - ' . $row->cnpj . ' - ' . $row->responsavel;

                $x++;
            }
            return $data;
        }
        else
            return null;
    }

    function por_id($id) {
        $query = $this->db->get_where('convenio', array('id_convenio' => $id, "ativo" => "s"));
        if ($query->num_rows() > 0) {
            $this->load->helper("formatar_helper");

            foreach ($query->result() as $row) {
                $data['id'] = $row->id_convenio;
                $data['nome'] = $row->nome;
                $data['cnpj'] = $row->cnpj;
                $data['telefone'] = $row->telefone;
                $data['cidade'] = $row->cidade;
                $data['uf'] = $row->uf;
                $data['responsavel'] = $row->responsavel;
            }
            return $data;
        }
        else
            return null;
    }

    function pega_nome($id_atendimento) {
        $sql = "SELECT *
            FROM convenio AS c, atendimento AS at
            WHERE c.id_convenio =  at.id_convenio_FK
            AND at.id_atendimento = ' " . $id_atendimento . " ' ";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $nome = $row->nome;

            return $nome;
        }
        else
            return null;
    }

}

?>