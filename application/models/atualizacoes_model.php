<?php

/**
 * @author Hermes Alves
 * @since 07/01/2014
 */
class Atualizacoes_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function ultimas() {
        $this->db->limit(10);
        $this->db->order_by("quando", "desc"); 
        $query = $this->db->get('atualizacoes');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        else
            return null;
    }

}

?>
