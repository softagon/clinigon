<?php

class Saida_caixa_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->clinica = 1;
    }

    /*
     * @return Testar se retorna um array
     */

    function pega_saida_caixas() {

        $query = $this->db->get('saida_caixa');
        if ($query->num_rows() > 0) {
            $retorno = $query->row_array(); #BUG BUG BUG 
            return $retorno; #BUG BUG BUG 
        } else
            return false;
    }

    function pega_nivel() {
        $this->db->order_by("nivel", "asc");
        $query = $this->db->get('saida_caixa_nivel');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else
            return false;
    }

    //Pode ser que já seja um associado
    function pega_dados($email) {

        $this->db->select("nome");
        $query = $this->db->get_where('associado', array('email_associado' => $email,
            'ativo' => 's'));
        if ($query->num_rows() > 0) {
            return $query->row_array(); #BUG BUG BUG 
        } else
            return null;
    }

    function gravar($post) {

        if (is_array($post)) {
            $this->load->helper("formatar_helper");
            $post['valor'] = dinheiro($post['valor']);
            $res = $this->db->insert('saida_caixa', $post);
            if (!$res)
                return false;
            else
                return true;
        } else {
            return false;
        }
    }

    function pega_asssociado($email) {
        $query = $this->db->get_where('associado', array('email_associado' => $email,
            'ativo' => 's'));
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id_associado;
        } else
            return null;
    }

    function atualizar($_POST) {
        if (is_array($_POST)) {
            $this->load->helper("formatar_helper");
            $dinheiro = dinheiro($_POST['valor']);
            $data = array(
                "nome" => $_POST['nome'],
                "id_centro_custo_FK" => $_POST['id_centro_custo_FK'],
                "valor" => $dinheiro,
            );

            if (!$this->db->update('saida_caixa', $data, array('id_saida_caixa' => $_POST['idsaida_caixa']))) {
                return false;
            } else
                return true;
        } else
            return false;
    }

    function apagar($id) {
        $data['ativo'] = "n";
        if (!$this->db->update('saida_caixa', $data, array('id_saida_caixa' => $id)))
            return false;
        else
            return true;
    }

    function tem_email($email) {
        $query = $this->db->get_where('saida_caixa', array('email_saida_caixa' => $email, 'ativo' => 's'));
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function busca($query) {
        $this->db->select('saida_caixa.nome AS nomecaixa, valor, centro_custo.nome AS centrocusto, id_saida_caixa');
        $this->db->from('saida_caixa');
        $this->db->join('centro_custo', 'id_centro_custo = id_centro_custo_FK');
        $this->db->where("saida_caixa.ativo", "s");
        $this->db->like("saida_caixa.nome", $query);
        $this->db->limit(10);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $x = 0;
            foreach ($query->result() as $row) {
                $data[$x]['id'] = $row->id_saida_caixa;
                $data[$x]['name'] = $row->nomecaixa . ' - ' . $row->centrocusto;
                $x++;
            }
            return $data;
        } else
            return null;
    }

    function por_id($id) {
        $query = $this->db->get_where('saida_caixa', array('id_saida_caixa' => $id, "ativo" => "s"));
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data['nome'] = $row->nome;
                $data['id_centro_custo_FK'] = $row->id_centro_custo_FK;
                $data['valor'] = $row->valor;
            }
            return $data;
        } else
            return null;
    }

}

?>