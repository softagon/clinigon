<?php

class Caixa_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function hoje() {
        $sql = "SELECT SUM(total) AS stotal 
        FROM atendimento AS a, pagamento AS p, forma_pagamento AS fp
        WHERE DATE(a.quando) = DATE(NOW())
        AND id_convenio_FK = 1
        AND fp.id_forma_pagamento = p.id_forma_pagamento_FK
        AND a.id_atendimento = p.id_atendimento_FK
        AND fp.nome !='Grátis' ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row();

            $this->load->helper("formatar_helper");
            $data['totalf'] = real_brasileiro($row->stotal);
            $data['total'] = $row->stotal;

            return $data;
        } else
            return null;
    }

    function ontem() {
        $sql = "SELECT SUM(total) AS stotal 
        FROM atendimento AS a, pagamento AS p, forma_pagamento AS fp
        WHERE DATE(NOW() - INTERVAL 1 DAY)
        AND id_convenio_FK = 1
        AND fp.id_forma_pagamento = p.id_forma_pagamento_FK
        AND a.id_atendimento = p.id_atendimento_FK
        AND fp.nome !='Grátis'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $this->load->helper("formatar_helper");
            $data['totalf'] = real_brasileiro($row->stotal);
            $data['total'] = $row->stotal;

            return $data;
        } else
            return null;
    }
    
    
    function saida_hoje() {
        $sql = "SELECT SUM(valor) AS valor "
                . "FROM saida_caixa "
                . "WHERE DATE(quando) = DATE(NOW()) ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row();

            $this->load->helper("formatar_helper");
            $data['totalf'] = real_brasileiro($row->valor);
            $data['total'] = $row->valor;

            return $data;
        } else
            return null;
    }

    function saida_ontem() {
        $sql = "SELECT SUM(valor) AS valor "
                . "FROM saida_caixa "
                . "WHERE DATE(quando) = DATE(NOW() - INTERVAL 1 DAY)  ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $this->load->helper("formatar_helper");
            $data['totalf'] = real_brasileiro($row->valor);
            $data['total'] = $row->valor;

            return $data;
        } else
            return null;
    }

    function busca($query) {
        $this->db->select('id_caixa, nome, valor');
        $this->db->like('nome', $query);
        $this->db->or_like('valor', $query);
        $this->db->limit(10);
        $this->db->where("ativo", "s");
        $query = $this->db->get('caixa');
        if ($query->num_rows() > 0) {
            $x = 0;
            foreach ($query->result() as $row) {
                $data[$x]['id'] = $row->id_caixa;
                $data[$x]['name'] = $row->nome . ' - ' . $row->valor;

                $x++;
            }
            return $data;
        } else
            return null;
    }

    function grafico_hora() {
        $sql = "SELECT   CONCAT(Hour, ':00-', Hour+1, ':00') AS horas
         ,COUNT(quando) AS `quantos`
        FROM     atendimento
        RIGHT JOIN (
                   SELECT  0 AS Hour
         UNION ALL SELECT  1 UNION ALL SELECT  2 UNION ALL SELECT  3
         UNION ALL SELECT  4 UNION ALL SELECT  5 UNION ALL SELECT  6
         UNION ALL SELECT  7 UNION ALL SELECT  8 UNION ALL SELECT  9
         UNION ALL SELECT 10 UNION ALL SELECT 11 UNION ALL SELECT 12
         UNION ALL SELECT 13 UNION ALL SELECT 14 UNION ALL SELECT 15
         UNION ALL SELECT 16 UNION ALL SELECT 17 UNION ALL SELECT 18
         UNION ALL SELECT 19 UNION ALL SELECT 20 UNION ALL SELECT 21
         UNION ALL SELECT 22 UNION ALL SELECT 23)      
        AS AllHours ON HOUR(quando) = Hour
        WHERE DATE(quando) = CURDATE() 
        OR quando IS NULL
        GROUP BY Hour
        ORDER BY Hour";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
           $retorno = $query->result_array();
            return $retorno;
        } else
            return null;
    }

}

?>