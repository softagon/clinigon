<?php

class Exame_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /*
     * @return Testar se retorna um array
     */

    function pega_exames() {

        $query = $this->db->get('exame');
        if ($query->num_rows() > 0) {
            return $query->row_array(); #BUG BUG BUG 
        }
        else
            return false;
    }

    function gravar($post) {
        if (is_array($post)) {
            $this->load->helper("formatar_helper");
            $post['valor'] = dinheiro($post['valor']);
            $post['valor_convenio'] = dinheiro($post['valor_convenio']);
            $res = $this->db->insert('exame', $post);
            if (!$res) {
                return false;
            } else {
                return true;
            }
        }
        else
            return false;
    }

    function atualizar($_POST) {
        if (is_array($_POST)) {
            $this->load->helper("formatar_helper");
            $data['nome'] = $_POST['nome'];
            $data['comissao'] = $_POST['comissao'];
            $data['valor'] = dinheiro($_POST['valor']);
            $data['valor_convenio'] = dinheiro($_POST['valor_convenio']);

            if (!$this->db->update('exame', $data, array('id_exame' => $_POST['idexame']))) {
                return false;
            }
            else
                return true;
        }
        else
            return false;
    }

    function apagar($id) {
        $data['ativo'] = "n";
        if (!$this->db->update('exame', $data, array('id_exame' => $id)))
            return false;
        else
            return true;
    }

   
    function busca($query) {
        $this->db->select('id_exame, nome, valor');
        $this->db->like('nome', $query);
        $this->db->or_like('valor', $query);
        $this->db->limit(10);
        $this->db->where("ativo", "s");
        $query = $this->db->get('exame');
        if ($query->num_rows() > 0) {
            $x = 0;
            foreach ($query->result() as $row) {
                $data[$x]['id'] = $row->id_exame;
                $data[$x]['name'] = $row->nome . ' - ' . ' R$ ' . $row->valor;

                $x++;
            }
            return $data;
        }
        else
            return null;
    }

    function por_id($id) {
        $query = $this->db->get_where('exame', array('id_exame' => $id, "ativo" => "s"));
        if ($query->num_rows() > 0) {
            $this->load->helper("formatar_helper");
            
            foreach ($query->result() as $row) {
                $data['id'] = $row->id_exame;
                $data['nome'] = $row->nome;
                $data['comissao'] = $row->comissao;
                $data['valor'] = $row->valor;
                $data['valorf'] = real_brasileiro($row->valor);
                $data['valor_convenio'] = $row->valor_convenio;
                $data['valorf_convenio'] = real_brasileiro($row->valor_convenio);
            }
            return $data;
        }
        else
            return null;
    }
    
}

?>