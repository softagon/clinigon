<?php

class Usuario_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->clinica = 1;
    }

    /*
     * @return Testar se retorna um array
     */

    function pega_usuarios() {

        $query = $this->db->get('usuario');
        if ($query->num_rows() > 0) {
            return $query->row_array(); #BUG BUG BUG 
        }
        else
            return false;
    }

    function pega_nivel() {
        $this->db->order_by("nivel", "asc");
        $query = $this->db->get('usuario_nivel');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        else
            return false;
    }

    //Pode ser que já seja um associado
    function pega_dados($email) {

        $this->db->select("nome");
        $query = $this->db->get_where('associado', array('email_associado' => $email,
            'ativo' => 's'));
        if ($query->num_rows() > 0) {
            return $query->row_array(); #BUG BUG BUG 
        }
        else
            return null;
    }

    function gravar($post) {
        if (is_array($post)) {

            $data = array(
                "nome" => $post['nome'],
                "email_usuario" => $post['email_usuario'],
                "id_usuario_nivel_FK" => $post['id_usuario_nivel_FK'],
                "senha" => $post['senha'],
                "id_clinica_FK" => $this->clinica,
            );
            $res = $this->db->insert('usuario', $data);
            if (!$res) {
                return false;
            } else {
                # Se o email for de um associado, ele criar o vinculo de n:m mysql
                $tem = $this->pega_asssociado($post['email_usuario']);
                if (!empty($tem)) {
                    $rel['id_usuario_FK'] = $this->db->insert_id();
                    $rel['id_associado_FK'] = $this->pega_asssociado($post['email_usuario']);
                    $res = $this->db->insert('associado_tem_usuario', $rel);
                    if (!$res)
                        return false;
                    else
                        return true;
                } else
                    return true;
            }
        }
        else
            return false;
    }

    function pega_asssociado($email) {
        $query = $this->db->get_where('associado', array('email_associado' => $email,
            'ativo' => 's'));
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id_associado;
        }
        else
            return null;
    }

    function atualizar($_POST) {
        if (is_array($_POST)) {

            $data = array(
                "nome" => $_POST['nome'],
                "email_usuario" => $_POST['email_usuario'],
                "id_usuario_nivel_FK" => $_POST['id_usuario_nivel_FK'],
                "senha" => $_POST['senha']
            );

            if (!$this->db->update('usuario', $data, array('id_usuario' => $_POST['idusuario']))) {
                return false;
            }
            else
                return true;
        }
        else
            return false;
    }

    function apagar($id) {
        $data['ativo'] = "n";
        if (!$this->db->update('usuario', $data, array('id_usuario' => $id)))
            return false;
        else
            return true;
    }

    function tem_email($email) {
        $query = $this->db->get_where('usuario', array('email_usuario' => $email, 'ativo' => 's'));
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function busca($query) {
        $this->db->select('id_usuario, nome, email_usuario');
        $this->db->like('nome', $query);
        $this->db->limit(10);
        $this->db->where("ativo", "s");
        $query = $this->db->get('usuario');
        if ($query->num_rows() > 0) {
            $x = 0;
            foreach ($query->result() as $row) {
                $data[$x]['id'] = $row->id_usuario;
                $data[$x]['name'] = $row->nome . ' - ' . $row->email_usuario;

                $x++;
            }
            return $data;
        }
        else
            return null;
    }

    function por_id($id) {
        $query = $this->db->get_where('usuario', array('id_usuario' => $id, "ativo" => "s"));
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data['nome'] = $row->nome;
                $data['email_usuario'] = $row->email_usuario;
                $data['id_usuario_nivel_FK'] = $row->id_usuario_nivel_FK;
            }
            return $data;
        }
        else
            return null;
    }

}

?>