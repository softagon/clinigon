<?php

class Atender_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $CI = & get_instance();
        $this->session = $CI->session;
    }

    function busca($query) {
        $this->db->select('id_associado, nome, documento, email_associado, celular');
        $this->db->like('nome', $query);
        $this->db->or_like('documento', $query);
        $this->db->limit(10);
        $this->db->where("ativo", "s");
        $query = $this->db->get('associado');
        if ($query->num_rows() > 0) {
            $x = 0;
            foreach ($query->result() as $row) {
                $data[$x]['id'] = $row->id_associado;
                $data[$x]['name'] = $row->nome . ' - ' . $row->documento;

                $x++;
            }
            return $data;
        } else
            return null;
    }

    function por_id($id) {
        $query = $this->db->get_where('associado', array('id_associado' => $id, "ativo" => "s"));
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data['nome'] = $row->nome;
                $data['documento'] = $row->documento;
                $data['email_associado'] = $row->email_associado;
                $data['telefone'] = $row->telefone;
                $data['celular'] = $row->celular;
                $data['comissao'] = $row->comissao;
                $data['id_especialidade_FK'] = $row->id_especialidade_FK;
                $data['valor'] = $row->valor;
            }
            return $data;
        } else
            return null;
    }

    function sessao_associados() {

        $carrinho = $this->session->all_userdata();
        $carrinho['associados']['carrinho']['total'] = $this->soma_carrinho($carrinho); //Nao guarda na sessão o total.
        return $carrinho['associados'];
    }

    function add_associado($_POST) {
        $valor = $this->session->userdata('associados');
        if (!empty($valor)) {
            $sessao = $valor;
            $sessao[] = $_POST;
        } else
            $sessao[] = $_POST;

        if (!empty($_POST['id_paciente']))
            $this->session->set_userdata("id_paciente", $_POST['id_paciente']);

        if (!empty($_POST['id_dependente']))
            $this->session->set_userdata("id_dependente", $_POST['id_dependente']);


        $this->session->set_userdata("associados", $sessao);
    }

    function add_convenio($_POST) {
        if (!empty($_POST['id_convenio']))
            $this->session->set_userdata("id_convenio", $_POST['id_convenio']);
    }

    function del_associado($_POST) {
        $id_associado = $_POST['id_associado'];
        $carrinho = $this->session->all_userdata();

        foreach ($carrinho['associados'] as $key => $value) {
            if (!empty($value['associado_nome'])) {
                if ($value['id_associado'] == $id_associado) {
                    unset($carrinho['associados'][$key]);
                }
            } else {
                unset($carrinho['associados'][$key]);
            }
        }

        $this->session->unset_userdata('associados');
        $this->session->set_userdata('associados', $carrinho['associados']);

        $carrinho = $this->session->all_userdata();
        $total = $this->soma_carrinho($carrinho);
        return $total;
    }

    function sessao_exames() {

        $carrinho = $this->session->all_userdata();
        $carrinho['exames']['carrinho']['total'] = $this->soma_carrinho($carrinho); //Nao guarda na sessão o total.
        return $carrinho['exames'];
    }

    function add_exame($_POST) {
        $valor = $this->session->userdata('exames');
        if (!empty($valor)) {
            $sessao = $valor;
            $sessao[] = $_POST;
        } else
            $sessao[] = $_POST;

        if (!empty($_POST['id_paciente']))
            $this->session->set_userdata("id_paciente", $_POST['id_paciente']);

        if (!empty($_POST['id_dependente']))
            $this->session->set_userdata("id_dependente", $_POST['id_dependente']);

        $this->session->set_userdata("exames", $sessao);
    }

    function del_exame($_POST) {
        $id_exame = $_POST['id_exame'];
        $carrinho = $this->session->all_userdata();

        foreach ($carrinho['exames'] as $key => $value) {
            if (!empty($value['exame_nome'])) {
                if ($value['id_exame'] == $id_exame) {
                    unset($carrinho['exames'][$key]);
                }
            } else {
                unset($carrinho['exames'][$key]);
            }
        }

        $this->session->unset_userdata('exames');
        $this->session->set_userdata('exames', $carrinho['exames']);

        $carrinho = $this->session->all_userdata();
        $total = $this->soma_carrinho($carrinho);
        return $total;
    }

    //Retorna formatado com R$
    function soma_associado($carrinho) {
        $soma = 0;
        if (!empty($carrinho['associados'])) {
            foreach ($carrinho['associados'] as $key => $value) {
                if (!empty($value['associado_valor'])) {
                    $soma += $value['associado_valor'];
                }
            }
        }

        return $soma;
    }

    function soma_exame($carrinho) {
        $soma = 0;
        if (!empty($carrinho['exames'])) {
            foreach ($carrinho['exames'] as $key => $value) {
                if (!empty($value['valor'])) {
                    $soma += $value['valor'];
                }
            }
        }
        return $soma;
    }

    function soma_carrinho($carrinho) {
        $this->load->helper("formatar_helper");
        $exame = $this->soma_exame($carrinho);
        $associado = $this->soma_associado($carrinho);

        $total = $exame + $associado;
        return real_brasileiro($total);
    }

    function pega_paciente($carrinho) {
        if (!empty($carrinho['id_paciente']))
            return $carrinho['id_paciente'];
        else
            return null;
    }

    function pega_dependente($carrinho) {
        if (!empty($carrinho['id_dependente']))
            return $carrinho['id_dependente'];
        else
            return null;
    }

    function add_atendimento($ate) {
        $res = $this->db->insert('atendimento', $ate);
        if ($res)
            return $this->db->insert_id();
        else
            return null;
    }

    function add_pagamento($pag) {
        $res = $this->db->insert('pagamento', $pag);
        if ($res)
            return true;
        else
            return false;
    }

    function liga_associado($ass) {
        $res = $this->db->insert('atendimento_tem_associado', $ass);
        if ($res)
            return true;
        else
            return false;
    }

//Lembrando que ID_CONVENIO = 1 é particular
    function cria_atendimento($_POST) {
        $this->load->helper("formatar_helper");

        $carrinho = $this->session->all_userdata();

        $data['id_paciente_FK'] = $this->pega_paciente($carrinho);
        if (!empty($data['id_paciente_FK'])) {
            $data['total'] = dinheiro($_POST['subtotal']);
            $data['id_dependente_FK'] = $this->pega_dependente($carrinho);

            $data['id_convenio_FK'] = $_POST['id_convenio'];
            $data['id_forma_pagamento_FK'] = $_POST['id_forma_pagamento_FK'];

            $res = $this->db->insert('atendimento', $data);
            if ($res) {
                $id_atendimento = $this->db->insert_id();
                $rassoc = $this->atendimento_tem_associado($id_atendimento);
                $rexame = $this->atendimento_tem_exame($id_atendimento);
                if ($rassoc OR $rexame)
                    return $id_atendimento;
                else
                    return null;
            } else
                return null;
        } else
            return null;
    }

    function atendimento_tem_exame($id_atendimento) {
        $carrinho = $this->session->all_userdata();
        if (!empty($carrinho['exames'])) {
            foreach ($carrinho['exames'] as $key => $value) {
                if (!empty($value['id_exame'])) {
                    $res = $this->db->insert('atendimento_tem_exame', array("id_atendimento_FK" => $id_atendimento,
                        "id_exame_FK" => $value['id_exame'],
                        "valor" => $value['valor']));
                }
            }
            if ($res)
                return true;
            else
                return false;
        }else {
            return false;
        }
    }

    function atendimento_tem_associado($id_atendimento) {
        $carrinho = $this->session->all_userdata();
        if (!empty($carrinho['associados'])) {
            foreach ($carrinho['associados'] as $key => $value) {
                if (!empty($value['id_associado'])) {
                    $res = $this->db->insert('atendimento_tem_associado', array("id_atendimento_FK" => $id_atendimento,
                        "id_associado_FK" => $value['id_associado'],
                        "valor" => $value['associado_valor']));
                }
            }
            if ($res)
                return true;
            else
                return false;
        }else {
            return false;
        }
    }

    function pega_associados($id_atendimento) {
        $sql = "SELECT ass.nome, es.especialidade, ass.documento, ass.valor, ass.valor_convenio,
            CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(ass.valor, 2),'.',';'),',','.'),';',',')) AS valorf,
            CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(ass.valor_convenio, 2),'.',';'),',','.'),';',',')) AS valor_conveniof
                FROM atendimento_tem_associado AS at , associado AS ass, especialidade AS es
                WHERE id_atendimento_FK = " . $id_atendimento . "
                AND at.id_associado_FK = ass.id_associado
                AND es.id_especialidade = ass.id_especialidade_FK";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else
            return null;
    }

    function pega_associado($id_atendimento) {
        $sql = "SELECT ass.nome, es.especialidade, ass.documento, ass.valor, ass.valor_convenio,
            CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(ass.valor, 2),'.',';'),',','.'),';',',')) AS valorf,
            CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(ass.valor_convenio, 2),'.',';'),',','.'),';',',')) AS valor_conveniof
                FROM atendimento_tem_associado AS at , associado AS ass, especialidade AS es
                WHERE id_atendimento_FK = " . $id_atendimento . "
                AND at.id_associado_FK = ass.id_associado
                AND es.id_especialidade = ass.id_especialidade_FK";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->row_array();
        else
            return null;
    }

    function pega_exames($id_atendimento) {
        $sql = "SELECT ex.nome, at.valor, CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(at.valor, 2),'.',';'),',','.'),';',',')) AS valorf
                FROM atendimento_tem_exame AS at , exame AS ex
                WHERE id_atendimento_FK = " . $id_atendimento . "
                AND at.id_exame_FK = ex.id_exame";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else
            return null;
    }

    function pega_forma_pagamento($id_atendimento) {
        $sql = "SELECT fp.nome
                FROM pagamento AS pag , forma_pagamento AS fp
                WHERE pag.id_atendimento_FK = " . $id_atendimento . "
                AND pag.id_forma_pagamento_FK = fp.id_forma_pagamento";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->nome;
        } else
            return null;
    }

    function total_atendimento($id_atendimento) {
        $sql = "SELECT at.total, at.desconto, at.todo_pago, pag.parte_paga,
        CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(at.total, 2),'.',';'),',','.'),';',',')) AS totalf,
        CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(at.desconto, 2),'.',';'),',','.'),';',',')) AS descontof,
        CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(pag.parte_paga, 2),'.',';'),',','.'),';',',')) AS parte_pagaf
        FROM atendimento AS at
        JOIN pagamento AS pag ON pag.id_atendimento_FK = at.id_atendimento
        WHERE id_atendimento = ".$id_atendimento." ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else
            return null;
    }

    function pega_desconto($id_atendimento) {
        $query = $this->db->get_where('atendimento', array('id_atendimento' => $id_atendimento,
            'ativo' => 's'));
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->desconto;
        } else
            return null;
    }

    function todo_pago($id_atendimento) {
        $query = $this->db->get_where('atendimento', array('id_atendimento' => $id_atendimento,
            'ativo' => 's', 'todo_pago' => 's'));
        if ($query->num_rows() > 0) {
            return true;
        } else
            return false;
    }

    function forma_pagamento() {
        $query = $this->db->get('forma_pagamento');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else
            return null;
    }

    function data($id_atendimento) {
        $this->load->helper("formatar_helper");
        $query = $this->db->get_where('atendimento', array('id_atendimento' => $id_atendimento,
            'ativo' => 's'));
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return data_brasil($row->quando);
        } else
            return null;
    }

    /* Pega se é CONVENIO ou PARTICULAR
     * Lembrando que id_convenio = 1 é particular
     * 
     * return 1 para particular e 0 para convenio
     * 
     */

    function pega_tipo($id_atendimento) {
        $sql = "SELECT *
            FROM convenio AS c, atendimento AS at
            WHERE c.id_convenio =  at.id_convenio_FK
            AND at.id_atendimento = ' " . $id_atendimento . " ' ";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $id_convenio = $row->id_convenio;
            if ($id_convenio == 1) {
                return 'particular';
            } else
                return 'convenio';
        } else
            return null;
    }

}

?>