<?php

class Dependente_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->clinica = 1;
    }

    /*
     * @return Testar se retorna um array
     */

    function pega_dependentes() {

        $query = $this->db->get('dependente');
        if ($query->num_rows() > 0) {
            return $query->row_array(); #BUG BUG BUG 
        }
        else
            return false;
    }

    function pega_funcoes() {
        $this->db->order_by("especialidade", "asc");
        $query = $this->db->get_where('especialidade', array('ativo' => 's'));
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        else
            return false;
    }

    function gravar($post) {
        if (is_array($post)) {
            $this->load->helper("formatar_helper");
            unset($post['busca-paciente']);
            $post['nascimento'] = data_americana($post['nascimento']);
            $res = $this->db->insert('dependente', $post);
            if (!$res) {
                return false;
            } else {
                return true;
            }
        }
        else
            return false;
    }

    function atualizar($_POST) {
        if (is_array($_POST)) {
            $this->load->helper("formatar_helper");
            $data['nome'] = $_POST['nome'];
            $data['sexo'] = $_POST['sexo'];
            $data['nascimento'] = data_americana($_POST['nascimento']);

            if (!$this->db->update('dependente', $data, array('id_dependente' => $_POST['iddependente']))) {
                return false;
            }
            else
                return true;
        }
        else
            return false;
    }

    function apagar($id) {
        $data['ativo'] = "n";
        if (!$this->db->update('dependente', $data, array('id_dependente' => $id)))
            return false;
        else
            return true;
    }

    function tem_documento($documento) {
        $query = $this->db->get_where('dependente', array('documento' => $documento, 'ativo' => 's'));
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function tem_email($email) {
        $query = $this->db->get_where('dependente', array('email_dependente' => $email, 'ativo' => 's'));
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function busca($query) {
        $this->db->select('id_dependente, nome, nascimento, sexo, id_paciente_FK');
        $this->db->like('nome', $query);
        $this->db->limit(10);
        $this->db->where("ativo", "s");
        $query = $this->db->get('dependente');
        if ($query->num_rows() > 0) {
            $x = 0;
            foreach ($query->result() as $row) {
                $data[$x]['id'] = $row->id_dependente;
                $data[$x]['name'] = $row->nome . ' - ' . $row->nascimento;
                $data[$x]['id_paciente_FK'] = $row->id_paciente_FK;

                $x++;
            }
            return $data;
        }
        else
            return null;
    }

    function por_id($id) {
        $query = $this->db->get_where('dependente', array('id_dependente' => $id, "ativo" => "s"));
        if ($query->num_rows() > 0) {
            $this->load->helper("formatar_helper");
            foreach ($query->result() as $row) {
                $data['nome'] = $row->nome;
                $data['nascimento'] = data_brasil($row->nascimento);
                $data['sexo'] = $row->sexo;
                $data['id_paciente_FK'] = $row->id_paciente_FK;
            }
            return $data;
        }
        else
            return null;
    }

}

?>