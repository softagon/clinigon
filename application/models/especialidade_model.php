<?php

class Especialidade_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->clinica = 1;
    }

    /*
     * @return Testar se retorna um array
     */

    function pega_especialidades() {
        $query = $this->db->get('especialidade');
        if ($query->num_rows() > 0) {
            return $query->row_array(); #BUG BUG BUG 
        }
        else
            return false;
    }

    function gravar($post) {
        if (is_array($post)) {
            $res = $this->db->insert('especialidade', $post);
            if (!$res) {
                return false;
            } else 
                return true;
        }
        else
            return false;
    }


    function atualizar($_POST) {
        if (is_array($_POST)) {

            $data = array(
                "especialidade" => $_POST['especialidade']);

            if (!$this->db->update('especialidade', $data, array('id_especialidade' => $_POST['idespecialidade']))) {
                return false;
            }
            else
                return true;
        }
        else
            return false;
    }

    function apagar($id) {
        $data['ativo'] = "n";
        if (!$this->db->update('especialidade', $data, array('id_especialidade' => $id)))
            return false;
        else
            return true;
    }

   
    function busca($query) {
        $this->db->select('id_especialidade, especialidade');
        $this->db->like('especialidade', $query);
        $this->db->limit(10);
        $this->db->where("ativo", "s");
        $query = $this->db->get('especialidade');
        if ($query->num_rows() > 0) {
            $x = 0;
            foreach ($query->result() as $row) {
                $data[$x]['id'] = $row->id_especialidade;
                $data[$x]['name'] = $row->especialidade;

                $x++;
            }
            return $data;
        }
        else
            return  'false';
    }

    function por_id($id) {
        $query = $this->db->get_where('especialidade', array('id_especialidade' => $id, "ativo" => "s"));
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data['especialidade'] = $row->especialidade;
            }
            return $data;
        }
        else
            return null;
    }

}

?>