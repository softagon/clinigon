<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Convenio extends CI_Controller {
    /*
     * @author Hermes Alves
     * @since 27/09/2013
     * 
     * @description Gestão direta ao convenio
     */

    public function __construct() {
        parent::__construct();
        $this->load->model("convenio_model");
        $this->load->library("suporte_library");
        ob_start(); //Evita erro do header  
    }

    public function cadastra() {

        if ($this->convenio_model->gravar($_POST))
            echo "true"; //se deu certo
        else
            echo "false"; //deu errado
    }

    public function modifica() {
        if ($this->convenio_model->atualizar($_POST))
            echo "true";
        else
            echo "false";
    }

    public function apaga($id = null) {
        if (!empty($id)) {
            if ($this->convenio_model->apagar($id))
                echo "true";
            else
                echo "false";
        }
    }

    public function documento_existe() {
        $documento = $_GET['documento'];
        if ($this->convenio_model->tem_documento($documento))
            echo "false";
        else {
            echo "true";
        }
    }

    public function email_existe() {
        $email = $_GET['email_convenio'];
        if ($this->convenio_model->tem_email($email))
            echo "false";
        else {
            echo "true";
        }
    }

    # No plural pega mais de 1

    public function pega_convenios() {
        $this->load->library("Datatables");
        
         $this->datatables
                ->select("nome, cnpj, responsavel, telefone,
                    CONCAT(cidade,' / ', uf) AS lugar", FALSE)
                ->from('convenio')
                ->where("convenio.ativo", "s");
         
        $data['result'] = $this->datatables->generate();
        echo $data['result'];
    }

    public function auto_complete($query = null) {
        if (!empty($query)) {
            $retorno = $this->convenio_model->busca($query);
            header('Content-type: application/json');
            echo json_encode($retorno);
        }
        else
            echo json_encode(null);
    }

    public function lista_convenio() {
        $this->load->view("post");
    }

    # Singular pega apenas 1, pelo id.

    public function pega_convenio($id = null) {
        $retorno = $this->convenio_model->por_id($id);
        header('Content-type: application/json');
        echo json_encode($retorno);
    }
    
    
    public function cnpj_valido(){
        $this->load->library("validar_library");
        $this->load->helper("formatar_helper");
         $documento = limpa_cnpj_cpf($_GET['cnpj']);
         
         header('Content-type: application/json');
        if (!$this->validar_library->validar_cnpj($documento))
            echo "false";
        else {
            echo "true";
        }
        
    }

}

/* End of file convenio.php */
/* Location: ./application/controllers/convenio.php */