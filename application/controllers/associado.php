<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Associado extends CI_Controller {
    /*
     * @author Hermes Alves
     * @since 27/09/2013
     * 
     * @description Gestão direta ao associado
     */

    public function __construct() {
        parent::__construct();
        $this->load->model("associado_model");
        $this->load->library("suporte_library");
        ob_start(); //Evita erro do header  
    }

    public function cadastra() {

        if ($this->associado_model->gravar($_POST))
            echo "true"; //se deu certo
        else
            echo "false"; //deu errado
    }

    public function modifica() {
        if ($this->associado_model->atualizar($_POST))
            echo "true";
        else
            echo "false";
    }

    public function apaga($id = null) {
        if (!empty($id)) {
            if ($this->associado_model->apagar($id))
                echo "true";
            else
                echo "false";
        }
    }

    public function documento_existe() {
        $documento = $_GET['documento'];
        if ($this->associado_model->tem_documento($documento))
            echo "false";
        else {
            echo "true";
        }
    }

    public function email_existe() {
        $email = $_GET['email_associado'];
        if ($this->associado_model->tem_email($email))
            echo "false";
        else {
            echo "true";
        }
    }

    # No plural pega mais de 1

    public function pega_associados() {
        $this->load->library("Datatables");
        
         $this->datatables
                ->select("nome, documento, email_associado, telefone,
                    CONCAT(celular,' - ', operadora) as cel, telefone, CONCAT(comissao,'%')  , especialidade, 
                    CONCAT ('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(valor, 2),'.',';'),',','.'),';',',')), 
                    CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(valor_convenio, 2),'.',';'),',','.'),';',','))valor", FALSE)
                ->from('associado')
                ->join('especialidade', 'associado.id_especialidade_FK = especialidade.id_especialidade', 'left outer')
                ->where("associado.ativo", "s");
         
        $data['result'] = $this->datatables->generate();
        echo $data['result'];
    }

    public function auto_complete($query = null) {
        if (!empty($query)) {
            $retorno = $this->associado_model->busca($query);
            header('Content-type: application/json');
            echo json_encode($retorno);
        }
        else
            echo json_encode(null);
    }

    public function lista_associado() {
        $this->load->view("post");
    }

    # Singular pega apenas 1, pelo id.

    public function pega_associado($id = null) {
        $retorno = $this->associado_model->por_id($id);
        header('Content-type: application/json');
        echo json_encode($retorno);
    }

}

/* End of file associado.php */
/* Location: ./application/controllers/associado.php */