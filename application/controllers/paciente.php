<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Paciente extends CI_Controller {
    /*
     * @author Hermes Alves
     * @since 07/01/2014
     * 
     * @description Gestão direta ao paciente
     */

    public function __construct() {
        parent::__construct();
        $this->load->model("paciente_model");
        $this->load->library("suporte_library");
        ob_start(); //Evita erro do header  
    }

    public function cadastra() {

        if ($this->paciente_model->gravar($_POST))
            echo "true"; //se deu certo
        else
            echo "false"; //deu errado
    }

    public function modifica() {
        if ($this->paciente_model->atualizar($_POST))
            echo "true";
        else
            echo "false";
    }

    public function apaga($id = null) {
        if (!empty($id)) {
            if ($this->paciente_model->apagar($id))
                echo "true";
            else
                echo "false";
        }
    }

    public function cpf_valido_existe() {
        $cpf = $_GET['cpf'];
        $this->load->library("validar_library");
        if ($this->validar_library->validCPF($cpf)) {
            if ($this->paciente_model->tem_cpf($cpf))
                echo "false";
            else {
                echo "true";
            }
        } else {
            echo "false";
        }
    }

    public function cpf_valido() {
        $cpf = $_GET['cpf'];
        $this->load->library("validar_library");
        if ($this->validar_library->validCPF($cpf)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function sus_valido_existe() {
        $sus = $_GET['sus'];
        $this->load->library("validar_library");
        if ($this->validar_library->validaCNS($sus)) {
            if ($this->paciente_model->tem_sus($sus))
                echo "false";
            else {
                echo "true";
            }
        } else {
            echo "false";
        }
    }

    public function sus_valido() {
        $sus = $_GET['sus'];
        $this->load->library("validar_library");
        if ($this->validar_library->validaCNS($sus)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function titulo_eleitor_existe() {
        $titulo_eleitor = $_GET['titulo_eleitor'];
        $this->load->library("validar_library");
        if ($this->paciente_model->tem_titulo($titulo_eleitor))
            echo "false";
        else {
            echo "true";
        }
    }

    # No plural pega mais de 1

    public function pega_pacientes() {
        $this->load->library("Datatables");

        $this->datatables
                ->select("nome, cpf, titulo_eleitor, sus, DATE_FORMAT( NOW( ) , '%Y' ) - DATE_FORMAT( nascimento, '%Y' ) - ( DATE_FORMAT( NOW( ) , '00-%m-%d' ) < DATE_FORMAT( nascimento, '00-%m-%d' ) ) AS idade,
                   telefone, CONCAT(celular,' - ', operadora) as cel, estado_civil, cidade, uf", FALSE)
                ->from('paciente')
                ->where("paciente.ativo", "s");

        $data['result'] = $this->datatables->generate();
        echo $data['result'];
    }

    public function auto_complete($query = null) {
        if (!empty($query)) {
            $retorno = $this->paciente_model->busca($query);
            header('Content-type: application/json');
            echo json_encode($retorno);
        }
        else
            echo json_encode(null);
    }

    public function lista_paciente() {
        $this->load->view("post");
    }

    # Singular pega apenas 1, pelo id.
    public function pega_paciente($id = null) {
        $retorno = $this->paciente_model->por_id($id);
        header('Content-type: application/json');
        echo json_encode($retorno);
    }

    public function pega_dados() {
        header('Content-type: application/json');
        if (!empty($_POST['email_paciente'])) {
            $email = $_POST['email_paciente'];
            $retorno = $this->paciente_model->pega_dados($email);
            if (!empty($retorno)) {
                $sucesso = array("success" => true);
                $retorno = array_merge($retorno, $sucesso);
            } else {
                $retorno = array('success' => false);
            }
        } else {
            $retorno = array('success' => false);
        }

        echo json_encode($retorno);
    }

}

/* End of file paciente.php */
/* Location: ./application/controllers/paciente.php */