<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Saida_caixa extends CI_Controller {
    /*
     * @author Erick Medeiros
     * @since 14/04/2014
     * 
     * @description Gestão direta ao saida_caixa
     */

    public function __construct() {
        parent::__construct();
        $this->load->model("saida_caixa_model");
        $this->load->library("suporte_library");
        ob_start(); //Evita erro do header  
    }

    public function cadastra() {
        $_POST['id_usuario_FK'] = $this->session->userdata('id_usuario');
        if ($this->saida_caixa_model->gravar($_POST))
            echo "true"; //se deu certo
        else
            echo "false"; //deu errado
    }

    public function modifica() {
        if ($this->saida_caixa_model->atualizar($_POST))
            echo "true";
        else
            echo "false";
    }

    public function apaga($id = null) {
        if (!empty($id)) {
            if ($this->saida_caixa_model->apagar($id))
                echo "true";
            else
                echo "false";
        }
    }

    public function documento_existe() {
        $documento = $_GET['documento'];
        if ($this->saida_caixa_model->tem_documento($documento))
            echo "false";
        else {
            echo "true";
        }
    }

    public function email_existe() {
        $email = $_GET['email_saida_caixa'];
        if ($this->saida_caixa_model->tem_email($email))
            echo "false";
        else {
            echo "true";
        }
    }

    # No plural pega mais de 1

    public function pega_saida_caixas() {
        $this->load->library("Datatables");

        $this->datatables
                ->select("caixa.nome, "
                        . "CONCAT('R$ ',Replace(Replace(Replace(Format(valor, 2), '.', '|'), ',', '.'), '|', ',')) AS valor, "
                        . "cen.nome AS centro_custo,"
                        . "DATE_FORMAT( caixa.quando , '%d/%m/%Y %H:%i:%s' ) AS quando", FALSE)
                ->from('saida_caixa AS caixa')
                ->join('centro_custo AS cen', 'caixa.id_centro_custo_FK = cen.id_centro_custo', 'inner')
                ->where("caixa.ativo", "s");

        $data['result'] = $this->datatables->generate();
        echo $data['result'];
    }

    public function auto_complete($query = null) {
        if (!empty($query)) {
            $retorno = $this->saida_caixa_model->busca($query);
            header('Content-type: application/json');
            echo json_encode($retorno);
        } else
            echo json_encode(null);
    }

    public function lista_saida_caixa() {
        $this->load->view("post");
    }

    # Singular pega apenas 1, pelo id.

    public function pega_saida_caixa($id = null) {
        $retorno = $this->saida_caixa_model->por_id($id);
        header('Content-type: application/json');
        echo json_encode($retorno);
    }

    public function pega_dados() {
        header('Content-type: application/json');
        if (!empty($_POST['email_saida_caixa'])) {
            $email = $_POST['email_saida_caixa'];
            $retorno = $this->saida_caixa_model->pega_dados($email);
            if (!empty($retorno)) {
                $sucesso = array("success" => true);
                $retorno = array_merge($retorno, $sucesso);
            } else {
                $retorno = array('success' => false);
            }
        } else {
            $retorno = array('success' => false);
        }

        echo json_encode($retorno);
    }

}

/* End of file saida_caixa.php */
/* Location: ./application/controllers/saida_caixa.php */