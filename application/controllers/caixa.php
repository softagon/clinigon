<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Caixa extends CI_Controller {
    /*
     * @author Hermes Alves
     * @since 27/09/2013
     * 
     * @description Gestão direta ao caixa
     */

    public function __construct() {
        parent::__construct();
        $this->load->model("caixa_model");
        $this->load->library("suporte_library");
        ob_start(); //Evita erro do header  
    }

    public function entrada() {
        
        $data['hoje'] = $this->caixa_model->hoje();
        $data['ontem'] = $this->caixa_model->ontem();

        $this->load->view("entrada_financeira", $data);
    }

    public function lista_hoje() {
        $this->load->library("Datatables");
        $this->datatables
                ->select("a.id_atendimento, "
                        . "CONCAT('<a href=" . base_url() . "atender/finalizando/',a.id_atendimento,' target=_blank>',p.nome, '</a>') as nome,
                CONCAT(p.cpf, ' ', p.sus, ' ', p.titulo_eleitor) AS documento,                   
                CONCAT(p.cidade, '-', UF) AS lugar, 
                DATE_FORMAT( p.nascimento , '%d/%m/%Y ' ) AS nascimento, 
                ass.nome AS associado, 
                ex.nome AS exame,
                fp.nome AS formpag,    
                IF (a.id_convenio_FK = 1 , 'Particular', 'Convênio') as tipo,
                DATE_FORMAT( a.quando , '%d/%m/%Y %H:%i:%s' ) AS quando, 
                IF(a.id_convenio_FK=1 AND fp.nome != 'Grátis',CONCAT('R$ ', Replace(Replace(Replace(Format(a.total, 2), '.', '|'), ', ', '.'), '|', ', ')), 'R$ 0,00') AS valorf", FALSE)
                ->from('atendimento AS a')
                ->join('paciente AS p', 'p.id_paciente = a.id_paciente_FK', 'left outer')
                ->join('atendimento_tem_associado AS ats', 'ats.id_atendimento_FK = a.id_atendimento', 'left outer')
                ->join('associado AS ass', 'ass.id_associado = ats.id_associado_FK', 'left outer')
                ->join('atendimento_tem_exame AS ate', 'ate.id_atendimento_FK = a.id_atendimento', 'left outer')
                ->join('exame AS ex', 'ex.id_exame = ate.id_exame_FK', 'left outer')
                ->join('pagamento AS pag', 'pag.id_atendimento_FK = a.id_atendimento', 'left outer')
                ->join('forma_pagamento AS fp', 'pag.id_forma_pagamento_FK = fp.id_forma_pagamento', 'left outer')
                ->where("DATE(a.quando) = DATE(NOW())");
        $data['result'] = $this->datatables->generate();
//        echo $this->db->last_query();
        echo $data['result'];
    }

    public function despesas_diario() {
        $this->load->library("Datatables");
        $this->datatables
                ->select("caixa.nome, "
                        . "CONCAT('R$ ',Replace(Replace(Replace(Format(valor, 2), '.', '|'), ',', '.'), '|', ',')) AS valor, "
                        . "cen.nome AS centro_custo,"
                        . "DATE_FORMAT( caixa.quando , '%d/%m/%Y %H:%i:%s' ) AS quando", FALSE)
                ->from('saida_caixa AS caixa')
                ->join('centro_custo AS cen', 'caixa.id_centro_custo_FK = cen.id_centro_custo', 'inner')
                ->where("caixa.ativo", "s")
                 ->where("DATE(caixa.quando) = DATE(NOW())");;
        $data['result'] = $this->datatables->generate();
//        echo $this->db->last_query();
        echo $data['result'];
    }
    
    public function saida() {
        $data['hoje'] = $this->caixa_model->saida_hoje();
        $data['ontem'] = $this->caixa_model->saida_ontem();
        
        $this->load->view("saida_financeira", $data);
    }

    public function pega_caixa_saida() {
        $this->load->library("Datatables");

        $this->datatables
                ->select("nome, email_usuario, quando,
                    visitou, nivel", FALSE)
                ->from('usuario')
                ->join('usuario_nivel', 'usuario.id_usuario_nivel_FK = usuario_nivel.id_usuario_nivel', 'left outer')
                ->where("usuario.ativo", "s");

        $data['result'] = $this->datatables->generate();
        echo $data['result'];
    }

}

/* End of file caixa.php */
/* Location: ./application/controllers/caixa.php */