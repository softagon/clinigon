<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Especialidade extends CI_Controller {
    /*
     * @author Hermes Alves
     * @since 27/09/2013
     * 
     * @description Gestão direta ao especialidade
     */

    public function __construct() {
        parent::__construct();
        $this->load->model("especialidade_model");
        $this->load->library("suporte_library");
        ob_start(); //Evita erro do header  
    }

    public function cadastra() {

        if ($this->especialidade_model->gravar($_POST))
            echo "true"; //se deu certo
        else
            echo "false"; //deu errado
    }

    public function modifica() {
        if ($this->especialidade_model->atualizar($_POST))
            echo "true";
        else
            echo "false";
    }

    public function apaga($id = null) {
        if (!empty($id)) {
            if ($this->especialidade_model->apagar($id))
                echo "true";
            else
                echo "false";
        }
    }

    public function documento_existe() {
        $documento = $_GET['documento'];
        if ($this->especialidade_model->tem_documento($documento))
            echo "false";
        else {
            echo "true";
        }
    }

    public function email_existe() {
        $email = $_GET['email_especialidade'];
        if ($this->especialidade_model->tem_email($email))
            echo "false";
        else {
            echo "true";
        }
    }

    # No plural pega mais de 1

    public function pega_especialidades() {
        $this->load->library("Datatables");
        
         $this->datatables
                ->select("id_especialidade, especialidade", FALSE)
                ->from('especialidade')
                ->where("especialidade.ativo", "s");
         
        $data['result'] = $this->datatables->generate();
        echo $data['result'];
    }

    public function auto_complete($query = null) {
        if (!empty($query)) {
            $retorno = $this->especialidade_model->busca($query);
            header('Content-type: application/json');
            echo json_encode($retorno);
        }
        else
            echo json_encode(null);
    }

    public function lista_especialidade() {
        $this->load->view("post");
    }

    # Singular pega apenas 1, pelo id.

    public function pega_especialidade($id = null) {
        $retorno = $this->especialidade_model->por_id($id);
        header('Content-type: application/json');
        echo json_encode($retorno);
    }

    public function pega_dados() {
        header('Content-type: application/json');
        if (!empty($_POST['email_especialidade'])) {
            $email = $_POST['email_especialidade'];
            $retorno = $this->especialidade_model->pega_dados($email);
            if(!empty($retorno)) {
            $sucesso = array("success" => true);
            $retorno = array_merge($retorno, $sucesso);
            } else {
                $retorno = array('success' => false);
            }
        } else {
            $retorno = array('success' => false);
        }

        echo json_encode($retorno);
    }

}

/* End of file especialidade.php */
/* Location: ./application/controllers/especialidade.php */