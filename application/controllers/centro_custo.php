<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Centro_custo extends CI_Controller {
    /*
     * @author Hermes Alves
     * @since 27/09/2013
     * 
     * @description Gestão direta ao centro_custo
     */

    public function __construct() {
        parent::__construct();
        $this->load->model("centro_custo_model");
        $this->load->library("suporte_library");
        ob_start(); //Evita erro do header  
    }

    public function cadastra() {

        if ($this->centro_custo_model->gravar($_POST))
            echo "true"; //se deu certo
        else
            echo "false"; //deu errado
    }

    public function modifica() {
        if ($this->centro_custo_model->atualizar($_POST))
            echo "true";
        else
            echo "false";
    }

    public function apaga($id = null) {
        if (!empty($id)) {
            if ($this->centro_custo_model->apagar($id))
                echo "true";
            else
                echo "false";
        }
    }

    public function documento_existe() {
        $documento = $_GET['documento'];
        if ($this->centro_custo_model->tem_documento($documento))
            echo "false";
        else {
            echo "true";
        }
    }

    public function email_existe() {
        $email = $_GET['email_centro_custo'];
        if ($this->centro_custo_model->tem_email($email))
            echo "false";
        else {
            echo "true";
        }
    }

    # No plural pega mais de 1

    public function pega_centro_custos() {
        $this->load->library("Datatables");
        
         $this->datatables
                ->select("nome, finalidade,DATE_FORMAT( quando , '%d/%m/%Y %H:%i:%s' ) AS quando", FALSE)
                ->from('centro_custo')
                ->where("centro_custo.ativo", "s");
         
        $data['result'] = $this->datatables->generate();
        echo $data['result'];
    }

    public function auto_complete($query = null) {
        if (!empty($query)) {
            $retorno = $this->centro_custo_model->busca($query);
            header('Content-type: application/json');
            echo json_encode($retorno);
        }
        else
            echo json_encode(null);
    }

    public function lista_centro_custo() {
        $this->load->view("post");
    }

    # Singular pega apenas 1, pelo id.

    public function pega_centro_custo($id = null) {
        $retorno = $this->centro_custo_model->por_id($id);
        header('Content-type: application/json');
        echo json_encode($retorno);
    }

    public function pega_dados() {
        header('Content-type: application/json');
        if (!empty($_POST['email_centro_custo'])) {
            $email = $_POST['email_centro_custo'];
            $retorno = $this->centro_custo_model->pega_dados($email);
            if(!empty($retorno)) {
            $sucesso = array("success" => true);
            $retorno = array_merge($retorno, $sucesso);
            } else {
                $retorno = array('success' => false);
            }
        } else {
            $retorno = array('success' => false);
        }

        echo json_encode($retorno);
    }

}

/* End of file centro_custo.php */
/* Location: ./application/controllers/centro_custo.php */