<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Util extends CI_Controller {
    /*
     * @author Hermes Alves
     * @since 27/09/2013
     * 
     * @description Gestão direta ao fornecedor
     */

    public function __construct() {
        parent::__construct();
        ob_start(); //Evita erro do header  
    }

    /*
     * Retira pontos e traços do cep
     */

    public function organizacep($cep) {
        $e = explode(".", $cep);
        $cepf = $e[0] . $e[1];
        $e = explode("-", $cepf);
        $cepf = $e[0] . $e[1];

        return $cepf;
    }

    public function pegaendereco() {
        header('Content-type: application/json');
        $this->load->model("util_model");
        if (!empty($_POST['cep'])) {
            $cep = $this->organizacep($_POST['cep']);
            $retorno = $this->util_model->pega_endereco($cep);
            if (!empty($retorno)) {
                $sucesso = array("success" => true);
                $retorno = array_merge($retorno, $sucesso);
            } else {
                $retorno = array('success' => false);
            }
        } else {
            $retorno = array('success' => false);
        }

        echo json_encode($retorno);
    }

    public function pega_operadora() {
        header('Content-type: application/json');
        $celular = $_POST['celular'];
        if (!empty($celular)) {
            $this->load->library("operadora_library");
            $operadora = $this->operadora_library->descobre_operadora($celular);
            $retorno = array("operadora" => $operadora);
            if (!empty($retorno)) {
                $sucesso = array("success" => true);
                $retorno = array_merge($retorno, $sucesso);
            } else {
                $retorno = array('success' => false);
            }
        } else {
            $retorno = array('success' => false);
        }

        echo json_encode($retorno);
    }

}

/* End of file fornecedor.php */
/* Location: ./application/controllers/fornecedor.php */