<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Exame extends CI_Controller {
    /*
     * @author Hermes Alves
     * @since 27/09/2013
     * 
     * @description Gestão direta ao exame
     */

    public function __construct() {
        parent::__construct();
        $this->load->model("exame_model");
        $this->load->library("suporte_library");
        ob_start(); //Evita erro do header  
    }

    public function cadastra() {

        if ($this->exame_model->gravar($_POST))
            echo "true"; //se deu certo
        else
            echo "false"; //deu errado
    }

    public function modifica() {
        if ($this->exame_model->atualizar($_POST))
            echo "true";
        else
            echo "false";
    }

    public function apaga($id = null) {
        if (!empty($id)) {
            if ($this->exame_model->apagar($id))
                echo "true";
            else
                echo "false";
        }
    }

    public function documento_existe() {
        $documento = $_GET['documento'];
        if ($this->exame_model->tem_documento($documento))
            echo "false";
        else {
            echo "true";
        }
    }

    public function email_existe() {
        $email = $_GET['email_exame'];
        if ($this->exame_model->tem_email($email))
            echo "false";
        else {
            echo "true";
        }
    }

    # No plural pega mais de 1

    public function pega_exames() {
        $this->load->library("Datatables");

        $this->datatables
                ->select("id_exame, nome, CONCAT(comissao, '%'),
                   CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(valor, 2),'.',';'),',','.'),';',',')) AS valor, 
                   CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(valor_convenio, 2),'.',';'),',','.'),';',',')) AS valor_convenio", FALSE)
                ->from('exame')
                ->where("exame.ativo", "s");

        $data['result'] = $this->datatables->generate();
        echo $data['result'];
    }

    public function auto_complete($query = null) {
        if (!empty($query)) {
            $retorno = $this->exame_model->busca($query);
            header('Content-type: application/json');
            echo json_encode($retorno);
        } else
            echo json_encode(null);
    }

    public function lista_exame() {
        $this->load->view("post");
    }

    # Singular pega apenas 1, pelo id.

    public function pega_exame($id = null) {
        $retorno = $this->exame_model->por_id($id);
        header('Content-type: application/json');
        echo json_encode($retorno);
    }

}

/* End of file exame.php */
/* Location: ./application/controllers/exame.php */