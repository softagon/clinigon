<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dependente extends CI_Controller {
    /*
     * @author Hermes Alves
     * @since 23/01/2014
     * 
     * @description Gestão direta ao dependente
     */

    public function __construct() {
        parent::__construct();
        $this->load->model("dependente_model");
        $this->load->library("suporte_library");
        ob_start(); //Evita erro do header  
    }

    public function cadastra() {

        if ($this->dependente_model->gravar($_POST))
            echo "true"; //se deu certo
        else
            echo "false"; //deu errado
    }

    public function modifica() {
        if ($this->dependente_model->atualizar($_POST))
            echo "true";
        else
            echo "false";
    }

    public function apaga($id = null) {
        if (!empty($id)) {
            if ($this->dependente_model->apagar($id))
                echo "true";
            else
                echo "false";
        }
    }

    public function documento_existe() {
        $documento = $_GET['documento'];
        if ($this->dependente_model->tem_documento($documento))
            echo "false";
        else {
            echo "true";
        }
    }

    public function email_existe() {
        $email = $_GET['email_dependente'];
        if ($this->dependente_model->tem_email($email))
            echo "false";
        else {
            echo "true";
        }
    }

    # No plural pega mais de 1

    public function pega_dependentes() {
        $this->load->library("Datatables");

        $this->datatables
                ->select("dependente.nome, paciente.nome AS paciente,
                    DATE_FORMAT( dependente.nascimento , '%d/%m/%Y' ) AS nascimento,                    
                    CASE WHEN dependente.sexo IN ('F') THEN 'Feminino'
                    WHEN dependente.sexo IN ('M') THEN 'Masculino' END AS Continent
                    ", FALSE)
                ->from('dependente')
                ->join('paciente', 'paciente.id_paciente = dependente.id_paciente_FK', 'left outer')
                ->where("dependente.ativo", "s");
        $data['result'] = $this->datatables->generate();
        echo $data['result'];
    }

    public function auto_complete($query = null) {
        if (!empty($query)) {
            $retorno = $this->dependente_model->busca($query);
            header('Content-type: application/json');
            echo json_encode($retorno);
        }
        else
            echo json_encode(null);
    }

    public function lista_dependente() {
        $this->load->view("post");
    }

    # Singular pega apenas 1, pelo id.

    public function pega_dependente($id = null) {
        $retorno = $this->dependente_model->por_id($id);
        header('Content-type: application/json');
        echo json_encode($retorno);
    }

}

/* End of file dependente.php */
/* Location: ./application/controllers/dependente.php */