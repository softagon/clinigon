<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Atender extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("atender_model");
        $this->load->library("vendas_library");
        ob_start(); //Evita erro do header  
    }

    public function registra_associado() {
        $this->atender_model->add_associado($_POST);
    }

    public function remove_associado() {
        $total = $this->atender_model->del_associado($_POST);
        $retorno = array("id_associado" => $_POST['id_associado'], "itemCount" => $_POST['itemCount'], "total" => $total);
        header('Content-type: application/json');
        echo json_encode($retorno);
    }

    public function pega_associados_sessao() {
        $retorno = $this->atender_model->sessao_associados();
        header('Content-type: application/json');
        echo json_encode($retorno);
    }

    # CONVENIOS

    public function registra_convenio() {
        $this->atender_model->add_convenio($_POST);
    }

    public function remove_convenio() {
        $total = $this->atender_model->del_convenio($_POST);
        $retorno = array("id_convenio" => $_POST['id_convenio'], "itemCount" => $_POST['itemCount'], "total" => $total);
        header('Content-type: application/json');
        echo json_encode($retorno);
    }

    public function pega_convenio_sessao() {
        $retorno = $this->atender_model->sessao_associados();
        header('Content-type: application/json');
        echo json_encode($retorno);
    }

    #EXAMES 

    public function registra_exame() {
        $this->atender_model->add_exame($_POST);
    }

    public function remove_exame() {
        $total = $this->atender_model->del_exame($_POST);
        $retorno = array("id_exame" => $_POST['id_exame'], "itemCount" => $_POST['itemCount'], "total" => $total);
//        header('Content-type: application/json');
        echo json_encode($retorno);
    }

    public function pega_exames_sessao() {
        $retorno = $this->atender_model->sessao_exames();
        header('Content-type: application/json');
        echo json_encode($retorno);
    }

    public function finalizando($id_atendimento = null) {

        if (empty($id_atendimento))
            $id_atendimento = $this->atender_model->cria_atendimento($_POST);

//        $id_atendimento = 49;
        if (!empty($id_atendimento)) {
            $this->session->sess_destroy();
            $this->load->helper("formatar_helper");
            $this->load->model("paciente_model");
            $this->load->model("convenio_model");

            $data['exames'] = $this->atender_model->pega_exames($id_atendimento);
            $data['associados'] = $this->atender_model->pega_associados($id_atendimento);
            $data['forma_pagamento'] = $this->atender_model->pega_forma_pagamento($id_atendimento);
            $data['total'] = $this->atender_model->total_atendimento($id_atendimento);
            $data['paciente'] = $this->paciente_model->por_atendimento($id_atendimento);
            $data['data_atendimento'] = $this->atender_model->data($id_atendimento);
            $data['tipo'] = $this->atender_model->pega_tipo($id_atendimento);
            $data['convenio_nome'] = $this->convenio_model->pega_nome($id_atendimento);

            if (!empty($data['exames'])) {
                $s_exames = 0;
                foreach ($data['exames'] as $value) {
                    $s_exames += $value['valor'];
                }

                $data['s_exame'] = real_brasileiro($s_exames);
            } else
                $data['s_exame'] = null;

            if (!empty($data['associados'])) {
                $s_associados = 0;
                foreach ($data['associados'] as $value) {
                    $s_associados += $value['valor'];
                }

                $data['s_associado'] = real_brasileiro($s_associados);
            } else
                $data['s_associado'] = null;

            //Caso seja só exame ou só consulta
            if (!empty($s_associados) AND ! empty($s_exames)) {
                $desconto = ($s_associados + $s_exames) - $data['total'];
            } else {

                if (!empty($s_associados))
                    $desconto = $s_associados - $data['total'];

                if (!empty($s_exames))
                    $desconto = $s_exames - $data['total'];
            }

            if ($desconto > 0 AND ! empty($desconto))
                $data['desconto'] = real_brasileiro($desconto);
            else
                $data['desconto'] = null;


            $data['total'] = real_brasileiro($data['total']);

            $this->load->view("recibo", $data);
        } else {
            echo "Sistema se comportou de forma MUITO inesperada. Chamem a SOFTAGON.";
        }
    }

    public function reg_consulta($id_atendimento = null) {

        if (empty($id_atendimento)) {
            if ($_POST["tipo_consulta"] == "particular") {

                $this->load->helper("formatar_helper");
                $ate['id_paciente_FK'] = $_POST['id_paciente_FK'];
                $ate['total'] = dinheiro($_POST['total']);
                $_POST['valor_pago_agora'] = dinheiro($_POST['valor_pago_agora']);
                if (!empty($_POST['total_desconto'])) {
                    $desconto = dinheiro($_POST['total_desconto']);
                    $ate['desconto'] = $desconto;
                } else
                    $ate["desconto"] = $desconto = null;

                if (($ate['total'] - $desconto) == $_POST['valor_pago_agora'])
                    $ate['todo_pago'] = "s";
                else
                    $ate['todo_pago'] = "n";

                $pag['id_atendimento_FK'] = $this->atender_model->add_atendimento($ate);
                $pag['id_forma_pagamento_FK'] = $_POST['id_forma_pagamento_FK'];
                $pag['parte_paga'] = $_POST['valor_pago_agora'];

                if ($this->atender_model->add_pagamento($pag)) {
                    $ass['id_atendimento_FK'] = $pag['id_atendimento_FK'];
                    $ass['id_associado_FK'] = $_POST['id_associado_FK'];
                    $ass['valor'] = $_POST['valor_pago_agora'];
                    if (($ate['total'] - $desconto) == $_POST['valor_pago_agora'])
                        $ass['status'] = "f";
                    else
                        $ass['status'] = "p";

                    $this->atender_model->liga_associado($ass);
                }else {
                    echo "Deu erraddo";
                }
            } else {
                $ate['id_paciente_FK'] = $_POST['id_paciente_FK'];
                $ate['id_convenio_FK'] = $_POST['id_convenio_FK'];
                $ate['id_atendimento_FK'] = $this->atender_model->add_atendimento($ate);

                $ass['id_atendimento_FK'] = $ate['id_atendimento_FK'];
                $ass['id_associado_FK'] = $_POST['id_associado_FK'];
                $ass['status'] = "f";
                $this->atender_model->liga_associado($ass);
            }
            
            $id_atendimento = $pag['id_atendimento_FK'] = $ate['id_atendimento_FK'];
            if (!empty($id_atendimento)) {
                $red = current_url() . '/' . $pag['id_atendimento_FK'];
                redirect($red, 'refresh');
            } else {
                echo "Deu errado";
            }
        } else {
            $this->load->helper("formatar_helper");
            $this->load->model("paciente_model");
            $this->load->model("convenio_model");

            $data['associado'] = $this->atender_model->pega_associado($id_atendimento);
            $data['forma_pagamento'] = $this->atender_model->pega_forma_pagamento($id_atendimento);
            $data['total_atendimento'] = $this->atender_model->total_atendimento($id_atendimento);
            $data['todo_pago'] = $this->atender_model->todo_pago($id_atendimento);
            $data['paciente'] = $this->paciente_model->por_atendimento($id_atendimento);
            $data['data_atendimento'] = $this->atender_model->data($id_atendimento);
            $data['tipo'] = $this->atender_model->pega_tipo($id_atendimento);
            $data['convenio_nome'] = $this->convenio_model->pega_nome($id_atendimento);

            #O total muda, se tiver desconto
            if (!empty($data['total_atendimento']['desconto'])) {
                $total_com_desconto = $data['total_atendimento']['total'] - $data['total_atendimento']['desconto'];
                $data['total_atendimento']['totalf'] = real_brasileiro($total_com_desconto);
            }

            #Falta algo para ser pago? Preenche pendente então.
            if (!$data['todo_pago']) {
                $data['pendente'] = $data['total_atendimento']['total'] - $data['total_atendimento']['parte_paga'];
                $data['pendentef'] = real_brasileiro($data['pendente']);
            }



//            echo "<pre>";
//            echo $data['total_atendimento']['total'];
//            var_dump($data);
//            exit();

            $this->load->view("recibo_consulta", $data);
        }
    }

    public function atendimentos() {
        $this->load->library("Datatables");
        $this->db->cache_on();
        $this->datatables
                ->select("a.id_atendimento AS id, CONCAT('<a href=" . base_url() . "atender/finalizando/',a.id_atendimento,' target=_blank>',p.nome, '</a>') as nome,  p.cpf, p.sus, p.titulo_eleitor, CONCAT(p.cidade, '-', UF) AS lugar, 
                DATE_FORMAT( p.nascimento , '%d/%m/%Y ' ) AS nascimento, ass.nome AS associado, ex.nome AS exame,
                DATE_FORMAT( a.quando , '%d/%m/%Y %H:%i:%s' ) AS quando, 
                CONCAT('R$ ', REPLACE(REPLACE(REPLACE(FORMAT(a.total, 2),'.',';'),',','.'),';',',')) AS valorf", FALSE)
                ->from('atendimento AS a')
                ->join('paciente AS p', 'p.id_paciente = a.id_paciente_FK', 'left outer')
                ->join('atendimento_tem_associado AS ats', 'ats.id_atendimento_FK = a.id_atendimento', 'left outer')
                ->join('associado AS ass', 'ass.id_associado = ats.id_associado_FK', 'left outer')
                ->join('atendimento_tem_exame AS ate', 'ate.id_atendimento_FK = a.id_atendimento', 'left outer')
                ->join('exame AS ex', 'ex.id_exame = ate.id_exame_FK', 'left outer');

        $data['result'] = $this->datatables->generate();
        echo $data['result'];
    }

    public function finalizados() {
//        $this->output->cache(15);
        $this->load->view("atendimentos_finalizados");
    }

}
