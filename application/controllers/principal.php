<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Principal extends CI_Controller {

    public function __construct() {
        parent::__construct();
        ob_start(); //Evita erro do header  

        $this->cad_ok = $this->suporte_library->cad_ok();
        $this->cad_erro = $this->suporte_library->cad_erro();
    }

    public function index() {

        if (!$this->acesso->is_logged()) {
            if (empty($_POST['email_usuario'])) {
                $this->load->view('login');
            } else {
                $this->acesso->login($_POST, 1);
            }
        }
        else
            redirect("/principal/inicio/", 'refresh');
    }

    public function inicio() {

        $this->load->model("atualizacoes_model");
        $this->load->helper("formatar_helper");

        $data['atualizacoes'] = $this->atualizacoes_model->ultimas();

        if ($this->acesso->is_logged())
            $this->load->view('inicio', $data);
        else
            redirect(RED, 'refresh');
    }

    public function sair() {
        $this->acesso->logout();
    }

    # São os médicos

    public function associado() {
        $this->load->model("associado_model");

        $data['associados'] = $this->associado_model->pega_associados();
        $data['especialidade'] = $this->associado_model->pega_funcoes();
      
        $this->load->view('associado', $data);
       
        
    }

    # São os planos

    public function convenio() {
        $this->load->model("convenio_model");
        $this->load->helper("endereco_helper");

        $data['uf'] = exibe_uf();
        $data['convenios'] = $this->convenio_model->pega_convenios();
        $this->load->view('convenio', $data);
    }

    public function exame() {
        $this->load->model("exame_model");

        $data['exames'] = $this->exame_model->pega_exames();
        $this->load->view('exame', $data);
       
    }

    public function usuario() {
        $this->load->model("usuario_model");

        $data['usuarios'] = $this->usuario_model->pega_usuarios();
        $data['nivel'] = $this->usuario_model->pega_nivel();
        $this->load->view('usuario', $data);
    }

    public function especialidade() {
        $this->load->model("especialidade_model");

        $data['especialidades'] = $this->especialidade_model->pega_especialidades();
        $this->load->view('especialidade', $data);
    }

    public function paciente() {
        $this->load->model("paciente_model");
        $this->load->helper("endereco_helper");

        $data['uf'] = exibe_uf();
        $data['pacientes'] = $this->paciente_model->pega_pacientes();
        $this->load->view('paciente', $data);
       
    }

    public function dependente() {
        $this->load->model("dependente_model");

        $data['dependentes'] = $this->dependente_model->pega_dependentes();
        $data['especialidade'] = $this->dependente_model->pega_funcoes();
        $this->load->view('dependente', $data);
    }

    public function atender() {
        $this->session->sess_destroy(); //Iniciando atendimento, limpa carrinho
        $this->load->model("atender_model");
        $data['formas'] = $this->atender_model->forma_pagamento();
        $this->load->view("atender", $data);
    }
    
    public function registrar_consulta() {
        $this->session->sess_destroy(); //Iniciando atendimento, limpa carrinho
        $this->load->model("atender_model");
        $data['formas'] = $this->atender_model->forma_pagamento();
        $this->load->view("registrar_consulta", $data);
    }
    
    public function registrar_exame() {
        $this->session->sess_destroy(); //Iniciando atendimento, limpa carrinho
        $this->load->model("atender_model");
        $data['formas'] = $this->atender_model->forma_pagamento();
        $this->load->view("registrar_exame", $data);
    }
    
       
     public function centro_custo() {
        $this->load->model("centro_custo_model");

        $data['centro_custos'] = $this->centro_custo_model->pega_centro_custos();
        $this->load->view('centro_custo', $data);
    }

    public function saida_caixa() {
        $this->load->model("saida_caixa_model");
        $this->load->model("centro_custo_model");

        $data['saida_caixas'] = $this->saida_caixa_model->pega_saida_caixas();
        $data['centro_custo'] = $this->centro_custo_model->pega_todos_centro_custos();
        $this->load->view('saida_caixa', $data);
    }
    
    public function historico_medico() {
        $this->load->model("historico_medico_model");
        $data['historico_medicos'] = $this->historico_medico_model->pega_saida_caixas();
        
        $this->load->view('historico_medico', $data);
    }
    public function teste() {
//        $this->session->sess_destroy();
        $carrinho = $this->session->all_userdata();

        echo $carrinho['id_paciente'];

        $this->load->model("atender_model");
        echo $this->atender_model->soma_carrinho($carrinho);
        echo "<br/>";
        echo "<pre>";
        var_dump($carrinho);
        echo "</pre>";
    }
    
    

    function tabela() {

        $this->load->view("tabela_teste");
    }

}
