<?php

function exibe_uf() {
    $data[] = "AC";
    $data[] = "AL";
    $data[] = "AM";
    $data[] = "AP";
    $data[] = "BA";
    $data[] = "CE";
    $data[] = "DF";
    $data[] = "ES";
    $data[] = "GO";
    $data[] = "MA";
    $data[] = "MG";
    $data[] = "MS";
    $data[] = "MT";
    $data[] = "PA";
    $data[] = "PB";
    $data[] = "PE";
    $data[] = "PI";
    $data[] = "PR";
    $data[] = "RJ";
    $data[] = "RN";
    $data[] = "RO";
    $data[] = "RR";
    $data[] = "RS";
    $data[] = "SC";
    $data[] = "SE";
    $data[] = "SP";
    $data[] = "TO";
    return $data;
}

function tipo_rua() {
    $data[] = "Aeroporto";
    $data[] = "Alameda";
    $data[] = "Área";
    $data[] = "Avenida";
    $data[] = "Campo";
    $data[] = "Chácara";
    $data[] = "Colônia";
    $data[] = "Condomínio";
    $data[] = "Conjunto";
    $data[] = "Distrito";
    $data[] = "Esplanada";
    $data[] = "Estação";
    $data[] = "Estrada";
    $data[] = "Favela";
    $data[] = "Fazenda";
    $data[] = "Feira";
    $data[] = "Jardim";
    $data[] = "Ladeira";
    $data[] = "Lago";
    $data[] = "Lagoa";
    $data[] = "Largo";
    $data[] = "Loteamento";
    $data[] = "Morro";
    $data[] = "Núcleo";
    $data[] = "Parque";
    $data[] = "Passarela";
    $data[] = "Pátio";
    $data[] = "Praça";
    $data[] = "Quadra";
    $data[] = "Recanto";
    $data[] = "Residencial";
    $data[] = "Rodovia";
    $data[] = "Rua";
    $data[] = "Setor";
    $data[] = "Sítio";
    $data[] = "Travessa";
    $data[] = "Trecho";
    $data[] = "Trevo";
    $data[] = "Vale";
    $data[] = "Vereda";
    $data[] = "Via";
    $data[] = "Viaduto";
    $data[] = "Viela";
    $data[] = "Vila";

    return $data;
}

?>
