<?php

#Formato americano para salvar no banco como decimal

function dinheiro($dinheiro) {
    $a = explode("R$ ", $dinheiro);
    $dinheiro = $a[1];
    $dinheiro = str_replace('.', '', $dinheiro);
    $dinheiro = str_replace(',', '.', $dinheiro);
    return $dinheiro;
}

#Nome do produto vem com o preço, estou organizando

function retira_dinheiro_nome($nome) {
    $a = explode("R$", $nome);
    return $a[0];
}

#Formato para visualização.

function real_brasileiro($dinheiro) {
    return "R$ " . number_format($dinheiro, 2, ',', '.');
}

# Recebe timestamp assim: date("Y-m-d H:i:s");

function data_brasil($data) {
    $fdata = $data;
    $a = explode(" ", $fdata);
    $a = explode("-", $a[0]);
    $dataf = $a[2] . "/" . $a[1] . "/" . $a[0];
    return $dataf;
}

#Recebe no formato brasil dd/mm/aaaa

function data_americana($data) {
    $fdata = $data;
    $a = explode("/", $fdata);
    $dataf = $a[2] . "-" . $a[1] . "-" . $a[0];
    return $dataf;
}

function formata_celular($celular) {
    $c = explode("_", $celular);
    return $c[0];
}

function limpa_cnpj_cpf($valor) {
    $valor = trim($valor);
    $valor = str_replace(".", "", $valor);
    $valor = str_replace(",", "", $valor);
    $valor = str_replace("-", "", $valor);
    $valor = str_replace("/", "", $valor);
    return $valor;
}

function idade($nascimento) {
    $date = new DateTime($nascimento); // data de nascimento
    $interval = $date->diff(new DateTime());
    
    return $interval->format( '%Y Anos, %m Meses e %d Dias' );
}

?>
