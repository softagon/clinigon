SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `softagon_clinigon` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `softagon_clinigon` ;

-- -----------------------------------------------------
-- Table `softagon_clinigon`.`clinica`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `softagon_clinigon`.`clinica` (
  `id_clinica` INT NOT NULL AUTO_INCREMENT COMMENT 'Comissão que a clínica cobra dos associados.',
  `nome` VARCHAR(45) NULL,
  `endereco` TEXT NULL,
  `cnpj` VARCHAR(45) NULL,
  `telefone` VARCHAR(45) NULL,
  `website` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `comissao_padrao` VARCHAR(45) NULL DEFAULT '15',
  PRIMARY KEY (`id_clinica`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `softagon_clinigon`.`funcao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `softagon_clinigon`.`funcao` (
  `id_funcao` INT NOT NULL AUTO_INCREMENT COMMENT 'Função, se é médico, fisioterapeuta e etc.',
  `especialidade` VARCHAR(45) NULL,
  PRIMARY KEY (`id_funcao`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `softagon_clinigon`.`associado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `softagon_clinigon`.`associado` (
  `id_associado` INT NOT NULL AUTO_INCREMENT,
  `id_funcao_FK` INT NOT NULL,
  `id_clinica_FK` INT NOT NULL,
  `nome` VARCHAR(45) NULL,
  `documento` VARCHAR(45) NULL COMMENT 'Documento pode ser o CRM',
  `email_associado` VARCHAR(120) NULL,
  `comissao` INT NULL,
  `quando` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `telefone` VARCHAR(45) NULL,
  `celular` VARCHAR(45) NULL,
  `ativo` ENUM('s','n') NULL DEFAULT 's',
  `valor` DOUBLE(13,2) NULL,
  PRIMARY KEY (`id_associado`),
  INDEX `fk_associado_clinica1_idx` (`id_clinica_FK` ASC),
  INDEX `fk_associado_funcao1_idx` (`id_funcao_FK` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email_associado` ASC),
  CONSTRAINT `fk_associado_clinica1`
    FOREIGN KEY (`id_clinica_FK`)
    REFERENCES `softagon_clinigon`.`clinica` (`id_clinica`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_associado_funcao1`
    FOREIGN KEY (`id_funcao_FK`)
    REFERENCES `softagon_clinigon`.`funcao` (`id_funcao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `softagon_clinigon`.`usuario_nivel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `softagon_clinigon`.`usuario_nivel` (
  `id_usuario_nivel` INT NOT NULL AUTO_INCREMENT,
  `nivel` VARCHAR(45) NULL,
  PRIMARY KEY (`id_usuario_nivel`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `softagon_clinigon`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `softagon_clinigon`.`usuario` (
  `id_usuario` INT NOT NULL AUTO_INCREMENT,
  `id_clinica_FK` INT NOT NULL,
  `nome` VARCHAR(45) NULL,
  `email_usuario` VARCHAR(45) NULL,
  `senha` VARCHAR(20) NULL,
  `quando` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `visitou` DATETIME NULL,
  `id_usuario_nivel_FK` INT NOT NULL,
  `ativo` ENUM('s','n') NULL DEFAULT 's',
  PRIMARY KEY (`id_usuario`),
  INDEX `fk_usuario_clinica1_idx` (`id_clinica_FK` ASC),
  INDEX `fk_usuario_usuario_nivel1_idx` (`id_usuario_nivel_FK` ASC),
  UNIQUE INDEX `email_usuario_UNIQUE` (`email_usuario` ASC),
  CONSTRAINT `fk_usuario_clinica1`
    FOREIGN KEY (`id_clinica_FK`)
    REFERENCES `softagon_clinigon`.`clinica` (`id_clinica`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_usuario_nivel1`
    FOREIGN KEY (`id_usuario_nivel_FK`)
    REFERENCES `softagon_clinigon`.`usuario_nivel` (`id_usuario_nivel`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `softagon_clinigon`.`cid10_subcategoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `softagon_clinigon`.`cid10_subcategoria` (
  `id_cid10` INT NOT NULL AUTO_INCREMENT,
  `subcat` VARCHAR(45) NULL,
  `classif` VARCHAR(45) NULL,
  `causaobito` VARCHAR(45) NULL,
  `descricao` TEXT NULL,
  `descrabrev` VARCHAR(45) NULL,
  `refer` VARCHAR(45) NULL,
  `excluidos` VARCHAR(45) NULL,
  PRIMARY KEY (`id_cid10`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `softagon_clinigon`.`cid10_categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `softagon_clinigon`.`cid10_categoria` (
  `id_cid10_cat` INT NOT NULL AUTO_INCREMENT,
  `cat` VARCHAR(45) NULL,
  `classif` VARCHAR(45) NULL,
  `descricao` TEXT NULL,
  `descrabrev` VARCHAR(45) NULL,
  `refer` VARCHAR(45) NULL,
  `excluidos` VARCHAR(45) NULL,
  PRIMARY KEY (`id_cid10_cat`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `softagon_clinigon`.`associado_tem_usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `softagon_clinigon`.`associado_tem_usuario` (
  `id_associado_FK` INT NOT NULL,
  `id_usuario_FK` INT NOT NULL,
  PRIMARY KEY (`id_associado_FK`, `id_usuario_FK`),
  INDEX `fk_associado_has_usuario_usuario1_idx` (`id_usuario_FK` ASC),
  INDEX `fk_associado_has_usuario_associado1_idx` (`id_associado_FK` ASC),
  UNIQUE INDEX `id_associado_FK_UNIQUE` (`id_associado_FK` ASC),
  UNIQUE INDEX `id_usuario_FK_UNIQUE` (`id_usuario_FK` ASC),
  CONSTRAINT `fk_associado_has_usuario_associado1`
    FOREIGN KEY (`id_associado_FK`)
    REFERENCES `softagon_clinigon`.`associado` (`id_associado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_associado_has_usuario_usuario1`
    FOREIGN KEY (`id_usuario_FK`)
    REFERENCES `softagon_clinigon`.`usuario` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `softagon_clinigon`.`paciente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `softagon_clinigon`.`paciente` (
  `id_paciente` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NULL,
  `email_paciente` VARCHAR(45) NULL,
  `nascimento` DATE NULL,
  `sexo` ENUM('M','F') NULL,
  `cep` VARCHAR(45) NULL,
  `endereco` TEXT NULL,
  `cidade` VARCHAR(45) NULL,
  `UF` VARCHAR(45) NULL,
  `telefone` VARCHAR(45) NULL,
  `celular` VARCHAR(45) NULL,
  `estado_civil` VARCHAR(45) NULL,
  `cpf` VARCHAR(45) NULL,
  `sus` VARCHAR(45) NULL,
  `titulo_eleitor` VARCHAR(45) NULL,
  `observacoes` TEXT NULL COMMENT 'Importante observações\n',
  `ativo` ENUM('s','n') NULL DEFAULT 's',
  `quando` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_paciente`),
  UNIQUE INDEX `cpf_UNIQUE` (`cpf` ASC),
  UNIQUE INDEX `sus_UNIQUE` (`sus` ASC),
  UNIQUE INDEX `titulo_UNIQUE` (`titulo_eleitor` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `softagon_clinigon`.`associado_tem_paciente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `softagon_clinigon`.`associado_tem_paciente` (
  `associado_id_associado` INT NOT NULL,
  `paciente_id_paciente` INT NOT NULL,
  PRIMARY KEY (`associado_id_associado`, `paciente_id_paciente`),
  INDEX `fk_associado_has_paciente_paciente1_idx` (`paciente_id_paciente` ASC),
  INDEX `fk_associado_has_paciente_associado1_idx` (`associado_id_associado` ASC),
  CONSTRAINT `fk_associado_has_paciente_associado1`
    FOREIGN KEY (`associado_id_associado`)
    REFERENCES `softagon_clinigon`.`associado` (`id_associado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_associado_has_paciente_paciente1`
    FOREIGN KEY (`paciente_id_paciente`)
    REFERENCES `softagon_clinigon`.`paciente` (`id_paciente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `softagon_clinigon`.`prontuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `softagon_clinigon`.`prontuario` (
  `id_prontuario` INT NOT NULL AUTO_INCREMENT,
  `anamnese` TEXT NULL,
  `id_paciente_FK` INT NOT NULL,
  PRIMARY KEY (`id_prontuario`),
  INDEX `fk_prontuario_paciente1_idx` (`id_paciente_FK` ASC),
  CONSTRAINT `fk_prontuario_paciente1`
    FOREIGN KEY (`id_paciente_FK`)
    REFERENCES `softagon_clinigon`.`paciente` (`id_paciente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `softagon_clinigon`.`anexo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `softagon_clinigon`.`anexo` (
  `id_anexo` INT NOT NULL AUTO_INCREMENT,
  `localizacao` VARCHAR(45) NULL,
  `id_prontuario_FK` INT NOT NULL,
  PRIMARY KEY (`id_anexo`),
  INDEX `fk_anexo_prontuario1_idx` (`id_prontuario_FK` ASC),
  CONSTRAINT `fk_anexo_prontuario1`
    FOREIGN KEY (`id_prontuario_FK`)
    REFERENCES `softagon_clinigon`.`prontuario` (`id_prontuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `softagon_clinigon`.`atualizacoes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `softagon_clinigon`.`atualizacoes` (
  `id_atualizacoes` INT NOT NULL AUTO_INCREMENT,
  `descricao` TEXT NULL,
  `quando` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_atualizacoes`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `softagon_clinigon`.`clinica`
-- -----------------------------------------------------
START TRANSACTION;
USE `softagon_clinigon`;
INSERT INTO `softagon_clinigon`.`clinica` (`id_clinica`, `nome`, `endereco`, `cnpj`, `telefone`, `website`, `email`, `comissao_padrao`) VALUES (1, 'Uniclinic do Araripe', 'Rua Pedro José Rodrigues, s/n – Bairro Centro', '07.796.296/0001-48', '(87)3873-2866', 'http://www.uniclinicdoararipe.com/', 'uniclinicdoaripe@gmail.com', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `softagon_clinigon`.`usuario_nivel`
-- -----------------------------------------------------
START TRANSACTION;
USE `softagon_clinigon`;
INSERT INTO `softagon_clinigon`.`usuario_nivel` (`id_usuario_nivel`, `nivel`) VALUES (1, 'Atendente');
INSERT INTO `softagon_clinigon`.`usuario_nivel` (`id_usuario_nivel`, `nivel`) VALUES (2, 'Financeiro');
INSERT INTO `softagon_clinigon`.`usuario_nivel` (`id_usuario_nivel`, `nivel`) VALUES (3, 'Associado');

COMMIT;

